subroutine cnrat(ratp0,irds,ak,bk,ilga,leva,isec)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     selection of appropriate look-up-table for given koehler terms
  !     for droplet growth calculations.
  !
  !     history:
  !     --------
  !     * aug 11/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use cnparm
  !
  implicit none
  integer :: il
  integer :: irat
  integer :: is
  integer :: l
  !
  logical, parameter :: kio = .false. !<
  !      logical, parameter :: kio=.true.
  integer, parameter :: iof = 50 !<
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  real, intent(in), dimension(ilga,leva) :: ak !<
  real, intent(in), dimension(ilga,leva,isec) :: bk !<
  integer, intent(out), dimension(ilga,leva,isec) :: irds !<
  real, intent(out), dimension(ilga,leva,isec) :: ratp0 !<
  real, allocatable, dimension(:,:,:) :: rat0 !<
  real, allocatable, dimension(:,:,:) :: rdiff !<
  real, allocatable, dimension(:,:,:) :: rdifft !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work arrays.
  !
  allocate(rat0  (ilga,leva,isec))
  allocate(rdiff (ilga,leva,isec))
  allocate(rdifft(ilga,leva,isec))
  !
  !-----------------------------------------------------------------------
  !     * koehler parameter ratio to determine the appropriate data table.
  !     * the approach is to select the table that produces the best
  !     * agreement for the parameter ratio.
  !
  rat0 = yna
  do is = 1,isec
    do l = 1,leva
      do il = 1,ilga
        if (abs(bk(il,l,is) - yna) > ysmall) then
          rat0(il,l,is) = bk(il,l,is)/(2. * ak(il,l))
        end if
      end do
    end do
  end do
  rdiff = ylarge
  ratp0 = yna
  irds = idef
  do irat = 1,irdp
    do is = 1,isec
      do l = 1,leva
        do il = 1,ilga
          if (abs(rat0(il,l,is) - yna) > ysmall) then
            rdifft(il,l,is) = abs(rat0(il,l,is) - ratp(irat))
            if (rdifft(il,l,is) < rdiff(il,l,is) ) then
              rdiff(il,l,is) = rdifft(il,l,is)
              irds(il,l,is) = irat
              ratp0(il,l,is) = ratp(irat)
            end if
          end if
        end do
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * write out points.
  !
  if (kio) then
    do is = 1,isec
      do l = 1,leva
        do il = 1,ilga
          write(iof, &
            '(a30,i3,1x,i3,1x,i3,i3,e10.4,1x,e10.4,1x,e10.4,1x,e10.4)') &
                 'il,l,is,irds,rat0,ratp0,ak,bk = ',il,l,is, &
                 irds(il,l,is),rat0(il,l,is),ratp0(il,l,is), &
                 ak(il,l),bk(il,l,is)
        end do
      end do
    end do
  end if
  !
  !-----------------------------------------------------------------------
  !     * deallocate work arrays.
  !
  deallocate(rat0)
  deallocate(rdiff)
  deallocate(rdifft)
  !
end subroutine cnrat
