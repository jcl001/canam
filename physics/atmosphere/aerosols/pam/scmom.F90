subroutine scmom(cen0,cephi0,cepsi,cin0,ciphi0,cipsi,cemom0, &
                 cemom2,cemom3,cemom4,cemom5,cemom6,cemom7,cemom8, &
                 cimom0,cimom2,cimom3,cimom4,cimom5,cimom6,cimom7, &
                 cimom8,cephis0,cedphi0,ciphis0,cidphi0,pen0, &
                 pephi0,pepsi,pin0,piphi0,pipsi,ilga,leva)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     calculation of various moments of the dry aerosol size
  !     distribution. results will be used in subsequent calculations
  !     such as for water content. calculations are for the sub-grid
  !     sections that are used in the condensation/evaporation
  !     calculations.
  !
  !     history:
  !     --------
  !     * jun 20/2008 - k.vonsalzen   additional moments.
  !     * aug 18/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use scparm
  use sdparm
  use sdcode
  use fpdef
  !
  implicit none
  integer :: il
  integer :: is
  integer :: isc
  integer :: isi
  integer :: ism
  integer :: ist
  integer :: l
  real :: rmom
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  real, intent(in), dimension(ilga,leva,isext) :: cephis0 !<
  real, intent(in), dimension(ilga,leva,isext) :: cedphi0 !<
  real, intent(in), dimension(ilga,leva,isint) :: ciphis0 !<
  real, intent(in), dimension(ilga,leva,isint) :: cidphi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaint) :: pin0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: piphi0 !<
  real, intent(in), dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(out), dimension(ilga,leva,isext) :: cen0 !<
  real, intent(out), dimension(ilga,leva,isext) :: cephi0 !<
  real, intent(out), dimension(ilga,leva,isext) :: cepsi !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom0 !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom2 !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom3 !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom4 !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom5 !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom6 !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom7 !<
  real(r8), intent(out), dimension(ilga,leva,isext) :: cemom8 !<
  real, intent(out), dimension(ilga,leva,isint) :: cin0 !<
  real, intent(out), dimension(ilga,leva,isint) :: ciphi0 !<
  real, intent(out), dimension(ilga,leva,isint) :: cipsi !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom0 !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom2 !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom3 !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom4 !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom5 !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom6 !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom7 !<
  real(r8), intent(out), dimension(ilga,leva,isint) :: cimom8 !<
  !
  !-----------------------------------------------------------------------
  !
  !     * pla parameters for externally mixed aerosol types.
  !
  isc = 0
  do is = 1,iswext
    ism = sextf%iswet(is)%ism
    do ist = 1,isesc
      isc = isc + 1
      do l = 1,leva
        do il = 1,ilga
          cen0  (il,l,isc) = pen0  (il,l,ism)
          cephi0(il,l,isc) = pephi0(il,l,ism)
          cepsi (il,l,isc) = pepsi (il,l,ism)
        end do
      end do
    end do
  end do
  !
  !     * pla parameters for internally mixed aerosol types.
  !
  isc = 0
  do is = 1,iswint
    isi = sintf%iswet(is)%isi
    do ist = 1,isesc
      isc = isc + 1
      do l = 1,leva
        do il = 1,ilga
          cin0  (il,l,isc) = pin0  (il,l,isi)
          ciphi0(il,l,isc) = piphi0(il,l,isi)
          cipsi (il,l,isc) = pipsi (il,l,isi)
        end do
      end do
    end do
  end do
  !
  !     * third and fourth moment of the size distribution for externally
  !     * mixed types of aerosols and mass of water.
  !
  if (isext > 0) then
    cemom0 = sdint0(cephi0,cepsi,cephis0,cedphi0,ilga,leva,isext)
    rmom = 2.
    cemom2 = sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom = 3.
    cemom3 = sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom = 4.
    cemom4 = sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom = 5.
    cemom5 = sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom = 6.
    cemom6 = sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom = 7.
    cemom7 = sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    rmom = 8.
    cemom8 = sdint(cephi0,cepsi,rmom,cephis0,cedphi0,ilga,leva,isext)
    where (abs(cepsi - yna) <= ytiny)
      cemom0 = yna
      cemom2 = yna
      cemom3 = yna
      cemom4 = yna
      cemom5 = yna
      cemom6 = yna
      cemom7 = yna
      cemom8 = yna
    end where
  end if
  !
  !     * third and fourth moment of the size distribution for internally
  !     * mixed types of aerosols.
  !
  if (isint > 0) then
    cimom0 = sdint0(ciphi0,cipsi,ciphis0,cidphi0,ilga,leva,isint)
    rmom = 2.
    cimom2 = sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom = 3.
    cimom3 = sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom = 4.
    cimom4 = sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom = 5.
    cimom5 = sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom = 6.
    cimom6 = sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom = 7.
    cimom7 = sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    rmom = 8.
    cimom8 = sdint(ciphi0,cipsi,rmom,ciphis0,cidphi0,ilga,leva,isint)
    where (abs(cipsi - yna) <= ytiny)
      cimom0 = yna
      cimom2 = yna
      cimom3 = yna
      cimom4 = yna
      cimom5 = yna
      cimom6 = yna
      cimom7 = yna
      cimom8 = yna
    end where
  end if
  !
end subroutine scmom
