subroutine radpari(pinum,pimas,pin0,piphi0,pipsi,piphis0,pidphi0, &
                   piwetrc,piddn,pire,pive,piload, &
                   pifrc,fr1,fr2,ilga,leva)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     radiation parameters (load, effective radius, effective variance).
  !
  !     history:
  !     --------
  !     * nov 21/2012 - k. von salzen    remove externally mixed aerosol
  !     * jan 17/2012 - k. von salzen    based on ssrad
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  !
  implicit none
  integer, intent(in) :: ilga
  integer :: is
  integer, intent(in) :: leva
  real, intent(in),  dimension(ilga,leva,isaint) :: pinum !<
  real, intent(in),  dimension(ilga,leva,isaint) :: pimas !<
  real, intent(in),  dimension(ilga,leva,isaint) :: piwetrc !<
  real, intent(in),  dimension(ilga,leva,isaint) :: piddn !<
  real, intent(in),  dimension(ilga,leva,isaint) :: pin0 !<
  real, intent(in),  dimension(ilga,leva,isaint) :: pipsi !<
  real, intent(in),  dimension(ilga,leva,isaint) :: piphi0 !<
  real, intent(in),  dimension(ilga,leva,isaint) :: piphis0 !<
  real, intent(in),  dimension(ilga,leva,isaint) :: pidphi0 !<
  real, intent(in),  dimension(ilga,leva,isaint,kint) :: pifrc !<
  real, intent(out),  dimension(ilga,leva) :: pire !<
  real, intent(out),  dimension(ilga,leva) :: pive !<
  real, intent(out),  dimension(ilga,leva) :: piload !<
  real, intent(out),  dimension(ilga,leva) :: fr1 !<
  real, intent(out),  dimension(ilga,leva) :: fr2 !<
  real,  dimension(ilga,leva) :: tre !<
  real,  dimension(ilga,leva) :: tve !<
  real,  dimension(ilga,leva) :: tload !<
  real,  dimension(ilga,leva) :: atmp1 !<
  real,  dimension(ilga,leva) :: atmp2 !<
  real,  dimension(ilga,leva) :: atmp3 !<
  !
  !-----------------------------------------------------------------------
  !     * initializations.
  !
  if (kint > 0) then
    fr1 = 1.
    fr2 = 0.
    piload = 0.
    pire = 0.
    pive = 0.
  end if
  !
  !     * internally mixed aerosol.
  !
  if (kint > 0) then
    call effrv(pinum,pimas,pin0,piphi0,pipsi,piphis0,pidphi0, &
               piwetrc,pidryrc,piddn,tload,tre,tve,ilga,leva, &
               isaint)
    piload = tload
    pire = tre
    pive = tve
    !
    !       * mass fractions for ammonium sulphate and black carbon.
    !
    atmp1 = 0.
    atmp2 = 0.
    atmp3 = 0.
    do is = 1,isaint
      atmp1 = atmp1 + pimas(:,:,is)
    end do
    if (kintso4 > 0) then
      do is = 1,isaint
        atmp2 = atmp2 + pimas(:,:,is) * pifrc(:,:,is,kintso4)
      end do
    end if
    if (kintbc > 0) then
      do is = 1,isaint
        atmp3 = atmp3 + pimas(:,:,is) * pifrc(:,:,is,kintbc)
      end do
    end if
    where (atmp1 > ytiny)
      fr1 = max(atmp2/atmp1,0.)
      fr2 = max(atmp3/atmp1,0.)
    end where
    atmp1 = fr1 + fr2
    where (atmp1 > .9999)
      atmp2 = 1./atmp1
      fr1 = fr1 * atmp2
      fr2 = fr2 * atmp2
    end where
  end if
  !
end subroutine radpari
