subroutine gtss (dso4dt,dsoadt,peddn,pen0,penum,pephi0,pepsi, &
                 pewetrc,t,rhoa,h2so4i,soapi,sgpr,ogpr,dt, &
                 ilga,leva,cnds,cndo)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     gas-to-particle conversion for sea salt.
  !
  !     history:
  !     --------
  !     * oct  1/2017 - k.vonsalzen   new, based on gtsoa
  !
  !     ************************ index of variables **********************
  !
  !     primary input/output variables (argument list):
  !
  ! o   dso4dt   gas-phase sulphuric acid (h2so4) tendency (kg/kg/sec)
  ! o   dsoadt   gas-phase soa aerosol prrecursor (soap) tendency (kg/kg/sec)
  ! o   cndo     soa precursor gas condensation rate (kg/kg/sec)
  ! o   cnds     (nh4)2so4 condensation rate (kg/kg/sec)
  ! i   dt       time step (sec)
  ! i   h2so4i   initial h2so4 mixing ratio (kg/kg)
  ! i   ilga     number of grid points in horizontal direction
  ! i   leva     number of grid pointa in vertical direction
  ! i   ogpr     gas-phase production rate of soa precursor gases (kg/kg/sec)
  ! i   peddn    density of dry aerosol particle (kg/m3), ext. mixture
  ! i   pen0     1st pla size distribution parameter (amplitude), ext.
  !              mixture
  ! i   penum    aerosol number concentration (1/kg), ext. mixture
  ! i   pephi0   3rd pla size distribution parameter (mode size), ext.
  !              mixture
  ! i   pepsi    2nd pla size distribution parameter (width), ext. mixture
  ! i   pewetrc  total/wet paricle radius (m), ext. mixture
  ! i   rhoa     air density (kg/m3)
  ! i   sgpr     gas-phase production rate of h2so4 (kg/kg/sec)
  ! i   soapi    initial soa precursor gas mixing ratio (kg/kg)
  ! i   t        air temperature (k)
  !
  !     secondary input/output variables (via modules and common blocks):
  !
  ! i   aextf%tp(:)%dryrc(:)
  !              dry particle radius in section centre (m), ext. mixture
  ! i   aextf%tp(:)%dryr(:)%vl
  !              lower dry particle radius for each section (m), ext.
  !              mixture
  ! i   aextf%tp(:)%dryr(:)%vr
  !              upper dry particle radius for each section (m), ext.
  !              mixture
  ! i   iexss    aerosol tracer index for sea salt, ext. mixture
  ! i   isaext   number of aerosol tracers, ext. mixture
  ! i   isextss  number of sections for sea salt aerosol, ext. mixture
  ! i   kextss   species index for sea salt aerosol, ext. mixture
  ! i   pedphis  section width, ext. mixture
  ! i   pephiss  lower dry particle size (=log(r/r0)), ext. mixture
  ! i   ylarge   large number
  ! i   ysec     security parameter for adjustment of timestep
  ! i   ysmall   small number
  ! i   ytiny    smallest valid number
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use fpdef
  !
  implicit none
  real, intent(in) :: dt
  integer :: is
  real :: wrat
  !
  real, intent(out), dimension(ilga,leva) :: dso4dt !<
  real, intent(out), dimension(ilga,leva) :: dsoadt !<
  real, intent(out), dimension(ilga,leva) :: cnds !<
  real, intent(out), dimension(ilga,leva) :: cndo !<
  real, intent(in), dimension(ilga,leva) :: sgpr !<
  real, intent(in), dimension(ilga,leva) :: ogpr !<
  real, intent(in), dimension(ilga,leva) :: t !<
  real, intent(in), dimension(ilga,leva) :: rhoa !<
  real, intent(in), dimension(ilga,leva) :: h2so4i !<
  real, intent(in), dimension(ilga,leva) :: soapi !<
  !
  real, intent(in), dimension(ilga,leva,isaext) :: peddn !<
  real, intent(in), dimension(ilga,leva,isaext) :: pen0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: penum !<
  real, intent(in), dimension(ilga,leva,isaext) :: pephi0 !<
  real, intent(in), dimension(ilga,leva,isaext) :: pepsi !<
  real, intent(in), dimension(ilga,leva,isaext) :: pewetrc !<
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  real(r8), allocatable, dimension(:,:,:) :: tecndgp !<
  real(r8), allocatable, dimension(:,:,:) :: tect1 !<
  real(r8), allocatable, dimension(:,:,:) :: tect2 !<
  real, allocatable, dimension(:,:,:) :: tewetrc !<
  real, allocatable, dimension(:,:,:) :: teddn !<
  real, allocatable, dimension(:,:,:) :: tecgr1 !<
  real, allocatable, dimension(:,:,:) :: tecgr2 !<
  real, allocatable, dimension(:,:,:) :: ten0 !<
  real, allocatable, dimension(:,:,:) :: tephi0 !<
  real, allocatable, dimension(:,:,:) :: tepsi !<
  real, allocatable, dimension(:,:,:) :: tedphit !<
  real, allocatable, dimension(:,:,:) :: tephist !<
  real, allocatable, dimension(:,:,:) :: tedccs !<
  real, allocatable, dimension(:,:,:) :: tedcco !<
  real, allocatable, dimension(:,:,:) :: ten !<
  real(r8), allocatable, dimension(:,:) :: dfso4 !<
  real(r8), allocatable, dimension(:,:) :: dbl1 !<
  real(r8), allocatable, dimension(:,:) :: dbl2 !<
  real(r8), allocatable, dimension(:,:) :: acnd !<
  real, allocatable, dimension(:,:) :: plfn !<
  real, allocatable, dimension(:,:) :: cndms !<
  real, allocatable, dimension(:,:) :: cndmsx !<
  real, allocatable, dimension(:,:) :: termx !<
  real, allocatable, dimension(:,:) :: term1 !<
  real, allocatable, dimension(:,:) :: term2 !<
  real, allocatable, dimension(:,:) :: frp !<
  real, allocatable, dimension(:,:) :: plam !<
  real, allocatable, dimension(:,:) :: pertc !<
  real, allocatable, dimension(:,:) :: expt !<
  real, allocatable, dimension(:,:) :: sadts !<
  real, allocatable, dimension(:,:) :: sadto !<
  real, allocatable, dimension(:,:) :: ctmps !<
  real, allocatable, dimension(:,:) :: ctmpo !<
  real, allocatable, dimension(:,:) :: cndmo !<
  real, allocatable, dimension(:,:) :: cndmox !<
  real, allocatable, dimension(:) :: tedryr !<
  real, allocatable, dimension(:) :: tedryrc !<
  !
  !-----------------------------------------------------------------------
  !     * allocate memory for local fields.
  !
  if (isextss > 0) then
    allocate(tewetrc(ilga,leva,isextss))
    allocate(ten    (ilga,leva,isextss))
    allocate(teddn  (ilga,leva,isextss))
    allocate(ten0   (ilga,leva,isextss))
    allocate(tephi0 (ilga,leva,isextss))
    allocate(tepsi  (ilga,leva,isextss))
    allocate(tect1  (ilga,leva,isextss))
    allocate(tect2  (ilga,leva,isextss))
    allocate(tecgr1 (ilga,leva,isextss))
    allocate(tecgr2 (ilga,leva,isextss))
    allocate(tecndgp(ilga,leva,isextss))
    allocate(tedcco (ilga,leva,isextss))
    allocate(tedccs (ilga,leva,isextss))
    allocate(tedphit(ilga,leva,isextss))
    allocate(tephist(ilga,leva,isextss))
    allocate(tedryr (isextss + 1))
    allocate(tedryrc(isextss))
  end if
  allocate(ctmpo  (ilga,leva))
  allocate(ctmps  (ilga,leva))
  allocate(acnd   (ilga,leva))
  allocate(plfn   (ilga,leva))
  allocate(cndms  (ilga,leva))
  allocate(cndmsx (ilga,leva))
  allocate(cndmo  (ilga,leva))
  allocate(cndmox (ilga,leva))
  allocate(termx  (ilga,leva))
  allocate(term1  (ilga,leva))
  allocate(term2  (ilga,leva))
  allocate(dfso4  (ilga,leva))
  allocate(frp    (ilga,leva))
  allocate(plam   (ilga,leva))
  allocate(pertc  (ilga,leva))
  allocate(expt   (ilga,leva))
  allocate(sadto  (ilga,leva))
  allocate(sadts  (ilga,leva))
  allocate(dbl1   (ilga,leva))
  allocate(dbl2   (ilga,leva))
  !
  !     * temporary aerosol fields for sea salt aerosol for dry
  !     * and wet particle sizes for externally mixed aerosol type.
  !
  if (kextss > 0) then
    do is = 1,isextss
      tewetrc(:,:,is) = pewetrc(:,:,iexss(is))
      ten    (:,:,is) = penum  (:,:,iexss(is))
      teddn  (:,:,is) = peddn  (:,:,iexss(is))
      ten0   (:,:,is) = pen0   (:,:,iexss(is))
      tephi0 (:,:,is) = pephi0 (:,:,iexss(is))
      tepsi  (:,:,is) = pepsi  (:,:,iexss(is))
      tedphit(:,:,is) = pedphis(iexss(is))
      tephist(:,:,is) = pephiss(iexss(is))
      tedryrc(is) = aextf%tp(kextss)%dryrc(is)
      tedryr(is) = aextf%tp(kextss)%dryr(is)%vl
    end do
    tedryr(isextss + 1) = aextf%tp(kextss)%dryr(isextss)%vr
  end if
  !
  !-----------------------------------------------------------------------
  !     * diffusivity of h2so4, mean free path, and rate coefficients
  !     * for condensation of gases onto pre-existing aerosol
  !     * (cndgp, in 1/s).
  !
  term1 = sqrt(t)
  term1 = term1 * t
  dfso4 = 2.150e-09_r8 * term1
  dbl1 = 1.62864e-25/t
  dbl2 = sqrt(dbl1)
  frp = 3.372999311e+11_r8 * dfso4 * dbl2
  wrat = wamsul/wh2so4
  acnd = 0.
  if (isextss > 0) then
    call grwtrm(tecndgp,tecgr1,tecgr2,tect1,tect2,ten0,ten,tewetrc, &
                tedryrc,tedryr,teddn,tephi0,tepsi,tephist,tedphit, &
                frp,dfso4,wrat,rhoa,leva,ilga,isextss)
    acnd = acnd + sum(tecndgp,dim = 3)
  end if
  !
  !-----------------------------------------------------------------------
  !     * parameters for time-evolution of gas concentrations
  !     * to equilibrium solution (ctmp).
  !
  ctmps = sgpr/max(acnd,ytiny)
  ctmpo = ogpr/max(acnd,ytiny)
  plam = acnd
  dbl1 = - plam * dt
  dbl2 = exp(dbl1)
  expt = dbl2
  !
  !     * time-integrated h2so4 concentration (sadts).
  !
  pertc = h2so4i - ctmps
  term2 = pertc * (expt - 1.)
  where (plam <= max(abs(term2)/ylarge,ytiny) )
    sadts = h2so4i * dt + .5 * sgpr * dt ** 2
  else where
    plfn = term2/plam
    sadts = ctmps * dt - plfn
  end where
  sadts = max(sadts,0.)
  !
  !     * time-integrated soa concentration (sadto).
  !
  pertc = soapi - ctmpo
  term2 = pertc * (expt - 1.)
  where (plam <= max(abs(term2)/ylarge,ytiny) )
    sadto = soapi * dt + .5 * ogpr * dt ** 2
  else where
    plfn = term2/plam
    sadto = ctmpo * dt - plfn
  end where
  sadto = max(sadto,0.)
  !
  !-----------------------------------------------------------------------
  !     * change in gas concentration from condensation.
  !
  if (isextss > 0) then
    do is = 1,isextss
      tedccs(:,:,is) = - tecndgp(:,:,is) * sadts(:,:)/rhoa(:,:)
      tedcco(:,:,is) = - tecndgp(:,:,is) * sadto(:,:)/rhoa(:,:)
    end do
  end if
  !
  !     * diagnose condensation rates.
  !
  cnds = 0.
  cndms = 0.
  if (isextss > 0) then
    termx = sum(tedccs,dim = 3)
    cndms = cndms + termx
    cnds = cnds - termx/dt
  end if
  cndo = 0.
  cndmo = 0.
  if (isextss > 0) then
    termx = sum(tedcco,dim = 3)
    cndmo = cndmo + termx
    cndo = cndo - termx/dt
  end if
  !
  !     * adjust condensation rates to make sure that truncation errors
  !     * in calculations of condensation rates do not lead to any
  !     * negative gas concentrations.
  !
  cndmsx = - h2so4i - sgpr * dt
  where (cndms < cndmsx .and. cndms < min(cndmsx/ylarge, - ytiny) )
    termx = cndmsx/cndms
    cndms = cndmsx
    cnds = cnds * termx
  end where
  cndmox = - soapi - ogpr * dt
  where (cndmo < cndmox .and. cndmo < min(cndmox/ylarge, - ytiny) )
    termx = cndmox/cndmo
    cndmo = cndmox
    cndo = cndo * termx
  end where
  !
  !-----------------------------------------------------------------------
  !     * gas tendencies.
  !
  dso4dt = (sgpr * dt + cndms)/dt
  dsoadt = (ogpr * dt + cndmo)/dt
  !
  !     * deallocation.
  !
  if (isextss > 0) then
    deallocate(tewetrc)
    deallocate(ten)
    deallocate(teddn)
    deallocate(ten0)
    deallocate(tephi0)
    deallocate(tepsi)
    deallocate(tect1)
    deallocate(tect2)
    deallocate(tecgr1)
    deallocate(tecgr2)
    deallocate(tecndgp)
    deallocate(tedcco)
    deallocate(tedccs)
    deallocate(tedphit)
    deallocate(tephist)
    deallocate(tedryr)
    deallocate(tedryrc)
  end if
  deallocate(ctmpo)
  deallocate(ctmps)
  deallocate(acnd)
  deallocate(plfn)
  deallocate(cndms)
  deallocate(cndmsx)
  deallocate(cndmo)
  deallocate(cndmox)
  deallocate(termx)
  deallocate(term1)
  deallocate(term2)
  deallocate(dfso4)
  deallocate(frp)
  deallocate(plam)
  deallocate(pertc)
  deallocate(expt)
  deallocate(sadto)
  deallocate(sadts)
  deallocate(dbl1)
  deallocate(dbl2)
  !
end subroutine gtss
