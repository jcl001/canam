subroutine pla2nm (bnum,bmass,pn0,phi0,psi,phis0,dphi0,drydn, &
                   ilga,leva,isec)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     transformation of pla-parameters to number and mass.
  !
  !     history:
  !     --------
  !     * apr 07/2006 - k.vonsalzen   completely revised for use with
  !     *                             internally and externally mixed
  !     *                             types of aerosols.
  !     * dec 11/2005 - k.vonsalzen   new.
  !
  !     ************************ index of variables **********************
  !
  !     primary input/output variables (argument list):
  !
  ! o   bmass    aerosol mass concentration (kg/kg)
  ! o   bnum     aerosol number concentration (1/kg)
  ! i   dphi0    section width
  ! i   drydn    density of dry aerosol particle (kg/m3)
  ! i   ilga     number of grid points in horizontal direction
  ! i   isec     number of separate aerosol tracers
  ! i   leva     number of grid pointa in vertical direction
  ! i   phi0     3rd pla size distribution parameter (mode size)
  ! i   pn0      1st pla size distribution parameter (amplitude)
  ! i   phis0    lower dry particle size (=log(r/r0))
  ! i   psi      2nd pla size distribution parameter (width)
  !
  !     secondary input/output variables (via modules and common blocks):
  !
  ! i   ycnst    4*pi/3
  ! i   yna      default for undefined value
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use sdcode
  !
  implicit none
  real :: rmom
  !
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  real, intent(in), dimension(ilga,leva,isec) :: drydn !<
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !<
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !<
  real, intent(in), dimension(ilga,leva,isec) :: psi !<
  real, intent(in), dimension(ilga,leva,isec) :: phis0 !<
  real, intent(in), dimension(ilga,leva,isec) :: dphi0 !<
  real, intent(out), dimension(ilga,leva,isec) :: bnum !<
  real, intent(out), dimension(ilga,leva,isec) :: bmass !<
  real, allocatable, dimension(:,:,:) :: sdfn !<
  real, allocatable, dimension(:,:,:) :: sdfm !<
  !
  !-----------------------------------------------------------------------
  !     * allocate work array.
  !
  allocate(sdfn(ilga,leva,isec))
  allocate(sdfm(ilga,leva,isec))
  !
  !-----------------------------------------------------------------------
  !     * obtain number and mass concentrations from integration of
  !     * size disribution as defined by pla-parameters.
  !
  sdfn = sdint0(phi0,psi,phis0,dphi0,ilga,leva,isec)
  bnum = pn0 * sdfn
  rmom = 3.
  sdfm = sdint(phi0,psi,rmom,phis0,dphi0,ilga,leva,isec)
  bmass = pn0 * sdfm * drydn * ycnst
  !
  !-----------------------------------------------------------------------
  !     * check whether pla parameters are valid and produce invalid
  !     * results if that is not the case.
  !
  where (abs(sdfn - yna) <= ytiny .or. abs(sdfm - yna) <= ytiny)
    bnum = yna
    bmass = yna
  end where
  !
  !-----------------------------------------------------------------------
  !     * deallocate work array.
  !
  deallocate(sdfn)
  deallocate(sdfm)
  !
end subroutine pla2nm
