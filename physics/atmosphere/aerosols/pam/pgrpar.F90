subroutine pgrpar (dp0l,dp1l,dp2l,dp3l,dp4l,dp5l,dp6l, &
                   dm1l,dm2l,dm3l,dp0r,dp1r,dp2r,dp3r, &
                   dp4r,dp5r,dp6r,dm1r,dm2r,dm3r,dp0s, &
                   dp1s,dp2s,dp3s,dp4s,dp5s,dp6s,dm1s, &
                   dm2s,dm3s,i0l,i0r,cgr1,cgr2,pn0, &
                   dryr,phis,dphis,phi0,psi,ilga,leva, &
                   isec,ierr,imodc)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     parameters for particle growth from condensation of sulphuric
  !     acid.
  !
  !     history:
  !     --------
  !     * jun 12/2008 - k.vonsalzen   add dimensions to arrays for paricle
  !                                   sizes
  !     * feb 20/2007 - k.vonsalzen   modified for ext./int. mixed
  !                                   aerosol, based on pgrwth
  !     * dec 11/2005 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use sdcode
  use fpdef
  !
  implicit none
  real :: anphil
  real :: anphir
  real :: cgrt
  real :: dphi0
  integer :: il
  integer :: is
  integer :: isi
  integer :: l
  real :: rmom
  !
  integer, intent(inout) :: ierr !<
  real, intent(in), dimension(ilga,leva,isec) :: cgr1 !<
  real, intent(in), dimension(ilga,leva,isec) :: cgr2 !<
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !<
  real, intent(in), dimension(ilga,leva,isec) :: psi !<
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !<
  real, intent(in), dimension(ilga,leva,isec) :: phis !<
  real, intent(in), dimension(ilga,leva,isec) :: dphis !<
  real, intent(in), dimension(ilga,leva,isec + 1) :: dryr !<
  integer, intent(in) :: ilga !<
  integer, intent(in) :: leva !<
  integer, intent(in) :: isec !<
  integer, intent(in) :: imodc !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp0l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp1l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp2l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp3l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp4l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp5l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp6l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm1l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm2l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm3l !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp0r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp1r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp2r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp3r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp4r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp5r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp6r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm1r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm2r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm3r !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp0s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp1s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp2s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp3s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp4s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp5s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dp6s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm1s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm2s !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: dm3s !<
  integer, intent(out), dimension(ilga,leva,isec) :: i0l !<
  integer, intent(out), dimension(ilga,leva,isec) :: i0r !<
  real, dimension(ilga,leva,isec) :: dphil !<
  real, dimension(ilga,leva,isec) :: dphir !<
  real, dimension(ilga,leva,isec) :: phisr !<
  real, dimension(ilga,leva,isec) :: tmp1 !<
  real, dimension(ilga,leva,isec) :: tmp2 !<
  real, dimension(ilga,leva,isec) :: tcr1 !<
  real, dimension(ilga,leva,isec) :: tcr2 !<
  real, dimension(ilga,leva) :: term1 !<
  real, dimension(ilga,leva) :: term2 !<
  real, dimension(ilga,leva) :: term3 !<
  real, dimension(ilga,leva) :: term4 !<
  integer, parameter :: iskip = - 1 !<
  !
  !-----------------------------------------------------------------------
  !     * initialization.
  !
  dp0l = 0.
  dp1l = 0.
  dp2l = 0.
  dp3l = 0.
  dp4l = 0.
  dp5l = 0.
  dp6l = 0.
  dm1l = 0.
  dm2l = 0.
  dm3l = 0.
  dp0r = 0.
  dp1r = 0.
  dp2r = 0.
  dp3r = 0.
  dp4r = 0.
  dp5r = 0.
  dp6r = 0.
  dm1r = 0.
  dm2r = 0.
  dm3r = 0.
  dp0s = 0.
  dp1s = 0.
  dp2s = 0.
  dp3s = 0.
  dp4s = 0.
  dp5s = 0.
  dp6s = 0.
  dm1s = 0.
  dm2s = 0.
  dm3s = 0.
  !
  !-----------------------------------------------------------------------
  !     * critical growth factor for complete size sections outgrowing
  !     * the particle size distribution
  !
  do is = 1,isec
    tmp1(:,:,is) = phis(:,:,1) - (phis(:,:,is) + dphis(:,:,is))
    tmp1(:,:,is) = 0.99 * tmp1(:,:,is)
    tmp2(:,:,is) = (phis(:,:,isec) + dphis(:,:,isec)) - phis(:,:,is)
    tmp2(:,:,is) = 1.01 * tmp2(:,:,is)
  end do
  tcr1 = exp(tmp1)
  tcr2 = exp(tmp2)
  !
  !     * change in size at section boundaries due to condensation
  !     * or chemical production. different growth models are available
  !     * for the dry particle radius (r) as a function of time (t):
  !     *   imodc=1: r(t)=r(t=0)+c/r(t=0)
  !     *   imodc=2: r(t)=c*r(t=0)
  !     * with c=cgr1+cgr2*r(t=0)
  !
  if (imodc == 1) then
    do is = 1,isec
      term1(:,:) = cgr1(:,:,is) + cgr2(:,:,is) * dryr(:,:,is)
      term1(:,:) = 1. + term1(:,:)/dryr(:,:,is) ** 2
      term2(:,:) = cgr1(:,:,is) + cgr2(:,:,is) * dryr(:,:,is + 1)
      term2(:,:) = 1. + term2(:,:)/dryr(:,:,is + 1) ** 2
      term1 = min(max(term1,tcr1(:,:,is)),tcr2(:,:,is))
      term2 = min(max(term2,tcr1(:,:,is)),tcr2(:,:,is))
      term3 = log(term1)
      dphil(:,:,is) = term3(:,:)
      where (abs(cgr2(:,:,is)) > ytiny .and. term1 > ytiny)
        term4 = term2/term1
      else where
        term4 = 1.
      end where
      term3 = log(term4)
      dphir(:,:,is) = dphil(:,:,is) + term3(:,:)
    end do
  else if (imodc == 2) then
    do is = 1,isec
      term1(:,:) = cgr1(:,:,is) + cgr2(:,:,is) * dryr(:,:,is)
      term2(:,:) = cgr1(:,:,is) + cgr2(:,:,is) * dryr(:,:,is + 1)
      term1 = min(max(term1,tcr1(:,:,is)),tcr2(:,:,is))
      term2 = min(max(term2,tcr1(:,:,is)),tcr2(:,:,is))
      term3 = log(term1)
      dphil(:,:,is) = term3(:,:)
      where (abs(cgr2(:,:,is)) > ytiny .and. term1 > ytiny)
        term4 = term2/term1
      else where
        term4 = 1.
      end where
      term3 = log(term4)
      dphir(:,:,is) = dphil(:,:,is) + term3(:,:)
    end do
  else
    call xit('pgrpar', - 1)
  end if
  !
  !-----------------------------------------------------------------------
  !     * determine index of section in which the boundary of
  !     * the modified section resides after condensation.
  !
  i0l = iskip
  i0r = iskip
  do isi = 1,isec
    do l = 1,leva
      do il = 1,ilga
        anphil = phis(il,l,isi) + dphil(il,l,isi)
        anphir = phis(il,l,isi) + dphis(il,l,isi) + dphir(il,l,isi)
        if (anphil < phis(il,l,1) ) i0l(il,l,isi) = 0
        if (anphir < phis(il,l,1) ) i0r(il,l,isi) = 0
        if (anphir > phis(il,l,isec) + dphis(il,l,isec) ) &
            i0r(il,l,isi) = isec + 1
      end do
    end do
  end do
  do is = 1,isec
    do isi = 1,isec
      do l = 1,leva
        do il = 1,ilga
          anphil = phis(il,l,isi) + dphil(il,l,isi)
          anphir = phis(il,l,isi) + dphis(il,l,isi) + dphir(il,l,isi)
          if (       (anphil >= phis(il,l,is)) &
              .and. (anphil <  phis(il,l,is) + dphis(il,l,is)) ) &
              i0l(il,l,isi) = is
          if (       (anphir >  phis(il,l,is)) &
              .and. (anphir <= phis(il,l,is) + dphis(il,l,is)) ) &
              i0r(il,l,isi) = is
        end do
      end do
    end do
  end do
  !
  !     * test whether indices are within allowed range.
  !
  do is = 1,isec
    do l = 1,leva
      do il = 1,ilga
        if     (i0r(il,l,is) < i0l(il,l,is) &
            .or. i0r(il,l,is) > (i0l(il,l,is) + 1) ) then
          ierr = - 10
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * calculate the size of the particle in each section that
  !     * corresponds to the boundary of the corresponding section for
  !     * the grown paritcles at time t. rinit is the initial radius of
  !     * the particle (r(t=0)) and is calculated from the inverse
  !     * of r(t)=f(r(t=0)).
  !
  do is = 1,isec
    phisr(:,:,is) = phis(:,:,is) + dphis(:,:,is)
  end do
  term1 = phis(:,:,1)
  term4 = exp(term1)
  if (imodc == 1) then
    do isi = 1,isec
      term1 = yna
      do l = 1,leva
        do il = 1,ilga
          if (i0l(il,l,isi) >= isi) then
            is = i0l(il,l,isi)
            if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
                .and. is /= i0r(il,l,isi) ) then
              term1(il,l) = dryr(il,l,is + 1) - cgr2(il,l,isi)
            end if
          else
            is = i0r(il,l,isi)
            if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
                .and. is /= i0l(il,l,isi) ) then
              term1(il,l) = dryr(il,l,is) - cgr2(il,l,isi)
            end if
          end if
        end do
      end do
      where (abs(term1 - yna) > ytiny)
        term2 = max(term1 ** 2 - 4. * cgr1(:,:,isi),0.)
      else where
        term2 = 0.
      end where
      term3 = sqrt(term2)
      where (abs(term1 - yna) > ytiny)
        term3 = .5 * (term1 + term3)/r0
      else where
        term3 = 1.
      end where
      term3 = max(term3,term4)
      term2 = log(term3)
      where (abs(term1 - yna) > ytiny)
        phisr(:,:,isi) = term2
      end where
      do l = 1,leva
        do il = 1,ilga
          phisr(il,l,isi) = max(phis(il,l,isi),min(phisr(il,l,isi), &
                            phis(il,l,isi) + dphis(il,l,isi)))
        end do
      end do
    end do
  else
    do isi = 1,isec
      term1 = yna
      do l = 1,leva
        do il = 1,ilga
          cgrt = min(max(cgr1(il,l,isi),tcr1(il,l,isi)),tcr2(il,l,isi))
          if (i0l(il,l,isi) >= isi) then
            is = i0l(il,l,isi)
            if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
                .and. is /= i0r(il,l,isi) ) then
              if (abs(cgr2(il,l,isi)) > ytiny) then
                term1(il,l) = cgr1(il,l,isi) ** 2 &
                              + 4. * cgr2(il,l,isi) * dryr(il,l,is + 1)
              else if (cgrt > ytiny) then
                term1(il,l) = dryr(il,l,is + 1)/cgrt
              end if
            end if
          else
            is = i0r(il,l,isi)
            if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
                .and. is /= i0l(il,l,isi) ) then
              if (abs(cgr2(il,l,isi)) > ytiny) then
                term1(il,l) = cgr1(il,l,isi) &
                              + 4. * cgr2(il,l,isi) * dryr(il,l,is)
              else if (cgrt > ytiny) then
                term1(il,l) = dryr(il,l,is)/cgrt
              end if
            end if
          end if
        end do
      end do
      where (term1 - yna > ytiny .and. abs(cgr2(:,:,isi)) > ytiny)
        term2 = term1
      else where
        term2 = 0.
      end where
      term3 = sqrt(term2)
      where (abs(term1 - yna) > ytiny)
        where (abs(cgr2(:,:,isi)) > ytiny)
          term3 = .5 * ( - cgr1(:,:,isi) + term3)/(cgr2(:,:,isi) * r0)
        else where (abs(term1 - yna) > ytiny)
          term3 = term1/r0
        else where
          term3 = 1.
        end where
      else where
        term3 = 1.
      end where
      term3 = max(term3,term4)
      term2 = log(term3)
      where (abs(term1 - yna) > ytiny)
        phisr(:,:,isi) = term2
      end where
      do l = 1,leva
        do il = 1,ilga
          phisr(il,l,isi) = max(phis(il,l,isi),min(phisr(il,l,isi), &
                            phis(il,l,isi) + dphis(il,l,isi)))
        end do
      end do
    end do
  end if
  !
  !     * test whether the boundary from inverse tranformation lies
  !     * within the section.
  !
  do is = 1,isec
    do l = 1,leva
      do il = 1,ilga
        if     (phisr(il,l,is) < phis(il,l,is) &
            .or. phisr(il,l,is) > phis(il,l,is) + dphis(il,l,is) ) then
          ierr = - 11
        end if
      end do
    end do
  end do
  !
  !-----------------------------------------------------------------------
  !     * calculate moments of size distribution.
  !
  if (imodc == 1) then
    do isi = 1,isec
      do l = 1,leva
        do il = 1,ilga
          !
          !          * calculate contribution in each section from the left-hand
          !          * side part of the modified section.
          !
          is = i0l(il,l,isi)
          if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
              ) then
            !
            !            * boundaries and width of original section at t=0.
            !
            anphil = phis (il,l,isi)
            anphir = phisr(il,l,isi)
            dphi0 = anphir - anphil
            dp0l(il,l,isi) = sdintb0(phi0(il,l,isi),psi(il,l,isi), &
                             anphil,dphi0)
            rmom = 3.
            dp3l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 2.
            dp2l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 1.
            dp1l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 1.
            dm1l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 2.
            dm2l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 3.
            dm3l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
          end if
          !
          !          * calculate contribution in each section from the right-hand
          !          * side part of the modified section.
          !
          is = i0r(il,l,isi)
          if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
              .and. i0l(il,l,isi) /= is) then
            !
            !            * boundaries and width of original section at t=0.
            !
            anphil = phisr(il,l,isi)
            anphir = phis(il,l,isi) + dphis(il,l,isi)
            dphi0 = anphir - anphil
            dp0r(il,l,isi) = sdintb0(phi0(il,l,isi),psi(il,l,isi), &
                             anphil,dphi0)
            rmom = 3.
            dp3r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 2.
            dp2r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 1.
            dp1r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 1.
            dm1r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 2.
            dm2r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 3.
            dm3r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
          end if
        end do
      end do
    end do
    !
    !-----------------------------------------------------------------------
    !       * residuum due to particles growing to sizes larger than
    !       * the spectrum cutoff.
    !
    do isi = 1,isec
      do l = 1,leva
        do il = 1,ilga
          is = i0r(il,l,isi)
          if ( (is == (isec + 1) .or. is == 0) &
              .and. pn0(il,l,isi) > ytiny) then
            !
            !            * boundaries and width of original section.
            !
            anphil = phisr(il,l,isi)
            anphir = phis(il,l,isi) + dphis(il,l,isi)
            dphi0 = anphir - anphil
            dp0s(il,l,isi) = sdintb0(phi0(il,l,isi),psi(il,l,isi), &
                             anphil,dphi0)
            rmom = 3.
            dp3s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 2.
            dp2s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 1.
            dp1s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 1.
            dm1s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 2.
            dm2s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 3.
            dm3s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
          end if
          is = i0l(il,l,isi)
          if ( (is == (isec + 1) .or. is == 0) &
              .and. pn0(il,l,isi) > ytiny) then
            !
            !            * boundaries and width of original section.
            !
            anphil = phis (il,l,isi)
            anphir = phisr(il,l,isi)
            dphi0 = anphir - anphil
            dp0s(il,l,isi) = sdintb0(phi0(il,l,isi),psi(il,l,isi), &
                             anphil,dphi0)
            rmom = 3.
            dp3s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 2.
            dp2s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 1.
            dp1s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 1.
            dm1s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 2.
            dm2s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = - 3.
            dm3s(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
          end if
        end do
      end do
    end do
  else
    do isi = 1,isec
      do l = 1,leva
        do il = 1,ilga
          !
          !          * calculate contribution in each section from the left-hand
          !          * side part of the modified section.
          !
          is = i0l(il,l,isi)
          if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
              ) then
            !
            !            * boundaries and width of original section at t=0.
            !
            anphil = phis (il,l,isi)
            anphir = phisr(il,l,isi)
            dphi0 = anphir - anphil
            dp0l(il,l,isi) = sdintb0(phi0(il,l,isi),psi(il,l,isi), &
                             anphil,dphi0)
            rmom = 3.
            dp3l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 4.
            dp4l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 5.
            dp5l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 6.
            dp6l(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
          end if
          !
          !          * calculate contribution in each section from the right-hand
          !          * side part of the modified section.
          !
          is = i0r(il,l,isi)
          if (is >= 1 .and. is <= isec .and. pn0(il,l,isi) > ytiny &
              .and. i0l(il,l,isi) /= is) then
            !
            !            * boundaries and width of original section at t=0.
            !
            anphil = phisr(il,l,isi)
            anphir = phis(il,l,isi) + dphis(il,l,isi)
            dphi0 = anphir - anphil
            dp0r(il,l,isi) = sdintb0(phi0(il,l,isi),psi(il,l,isi), &
                             anphil,dphi0)
            rmom = 3.
            dp3r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 4.
            dp4r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 5.
            dp5r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
            rmom = 6.
            dp6r(il,l,isi) = sdintb(phi0(il,l,isi),psi(il,l,isi),rmom, &
                             anphil,dphi0)
          end if
        end do
      end do
    end do
    !
    !-----------------------------------------------------------------------
    !       * residuum due to particles growing to sizes outside
    !       * the spectrum cutoff.
    !
    do isi = 1,isec
      do l = 1,leva
        do il = 1,ilga
          is = i0r(il,l,isi)
          if ( (is == (isec + 1) .or. is == 0) &
              .and. pn0(il,l,isi) > ytiny) then
            !
            !            * boundaries and width of original section.
            !
            anphil = phisr(il,l,isi)
            anphir = phis(il,l,isi) + dphis(il,l,isi)
            dphi0 = anphir - anphil
            dp0s(il,l,isi) = dp0s(il,l,isi) &
                             + sdintb0(phi0(il,l,isi),psi(il,l,isi),anphil,dphi0)
            rmom = 3.
            dp3s(il,l,isi) = dp3s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
            rmom = 4.
            dp4s(il,l,isi) = dp4s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
            rmom = 5.
            dp5s(il,l,isi) = dp5s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
            rmom = 6.
            dp6s(il,l,isi) = dp6s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
          end if
          is = i0l(il,l,isi)
          if ( (is == (isec + 1) .or. is == 0) &
              .and. pn0(il,l,isi) > ytiny) then
            !
            !            * boundaries and width of original section.
            !
            anphil = phis (il,l,isi)
            anphir = phisr(il,l,isi)
            dphi0 = anphir - anphil
            dp0s(il,l,isi) = dp0s(il,l,isi) &
                             + sdintb0(phi0(il,l,isi),psi(il,l,isi),anphil,dphi0)
            rmom = 3.
            dp3s(il,l,isi) = dp3s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
            rmom = 4.
            dp4s(il,l,isi) = dp4s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
            rmom = 5.
            dp5s(il,l,isi) = dp5s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
            rmom = 6.
            dp6s(il,l,isi) = dp6s(il,l,isi) &
                             + sdintb(phi0(il,l,isi),psi(il,l,isi),rmom,anphil,dphi0)
          end if
        end do
      end do
    end do
  end if
  !
end subroutine pgrpar
