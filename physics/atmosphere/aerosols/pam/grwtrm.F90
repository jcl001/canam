subroutine grwtrm(cndgp,cgr1,cgr2,ct1,ct2,pn0,anum,wetrc, &
                  dryrc,dryr,ddn,phi0,psi,phiss,dphis, &
                  frp,dfso4,wrat,rhoa,leva,ilga,isec)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     rate coefficient for condensation of h2so4 onto pre-existing
  !     aerosol (cndgp, in 1/s).
  !
  !     history:
  !     --------
  !     * apr 17/2015 - k.vonsalzen   changes to support single precision
  !     * nov 17/2006 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use sdcode
  use fpdef
  !
  implicit none
  real :: afl
  integer :: il
  integer, intent(in) :: ilga
  integer :: is
  integer, intent(in) :: isec
  integer :: l
  integer, intent(in) :: leva
  real :: rmom1
  real :: rmom2
  !
  real(r8), intent(out), dimension(ilga,leva,isec) :: cndgp !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: ct1 !<
  real(r8), intent(out), dimension(ilga,leva,isec) :: ct2 !<
  real, intent(out), dimension(ilga,leva,isec) :: cgr1 !<
  real, intent(out), dimension(ilga,leva,isec) :: cgr2 !<
  real, intent(in), dimension(ilga,leva,isec) :: anum !<
  real, intent(in), dimension(ilga,leva,isec) :: wetrc !<
  real, intent(in), dimension(ilga,leva,isec) :: ddn !<
  real, intent(in), dimension(ilga,leva,isec) :: phi0 !<
  real, intent(in), dimension(ilga,leva,isec) :: psi !<
  real, intent(in), dimension(ilga,leva,isec) :: phiss !<
  real, intent(in), dimension(ilga,leva,isec) :: dphis !<
  real, intent(in), dimension(ilga,leva,isec) :: pn0 !<
  real(r8), intent(in), dimension(ilga,leva) :: dfso4 !<
  real, intent(in), dimension(ilga,leva) :: frp !<
  real, intent(in), dimension(ilga,leva) :: rhoa !<
  real, intent(in), dimension(isec) :: dryrc !<
  real, intent(in), dimension(isec + 1) :: dryr !<
  real, intent(in) :: wrat !<
  real(r8), dimension(ilga,leva,isec) :: dbl !<
  real(r8) :: fgr !<
  real(r8) :: wetrl !<
  real(r8) :: wetrr !<
  real(r8) :: aknl !<
  real(r8) :: aknr !<
  real(r8) :: fnl !<
  real(r8) :: fnr !<
  real(r8) :: anl !<
  real(r8) :: anr !<
  real(r8) :: dr !<
  real(r8) :: dafdr !<
  real(r8) :: cterm !<
  !
  !     * intrinsic functions.
  !
  real(r8) :: zfn !<
  real(r8) :: zan !<
  real(r8) :: ag1 !<
  real(r8) :: ag2 !<
  zfn(ag1) = (1. + ag1)/(1. + 1.71 * ag1 + 1.33 * ag1 * ag1)
  zan(ag1,ag2) = 1./(1. + 1.33 * ag1 * ag2 * (1./ace - 1.))
  !
  !-----------------------------------------------------------------------
  !
  cgr1 = 0.
  cgr2 = 0.
  ct1 = 0.
  ct2 = 0.
  do is = 1,isec
    do l = 1,leva
      do il = 1,ilga
        if (anum(il,l,is) > ytiny) then
          fgr = wetrc(il,l,is)/dryrc(is)
          !
          !         * term a*f in growth equation at lower and upper boundaries
          !         * of the section from linearization.
          !
          wetrl = fgr * dryr(is)
          aknl = frp(il,l)/wetrl
          fnl = zfn(aknl)
          anl = zan(aknl,fnl)
          wetrr = fgr * dryr(is + 1)
          aknr = frp(il,l)/wetrr
          fnr = zfn(aknr)
          anr = zan(aknr,fnr)
          afl = fnl * anl
          dr = dryr(is + 1) - dryr(is)
          dafdr = (fnr * anr - afl)/dr
          !
          !         * terms for calculation of section-mean condensation rate.
          !
          cterm = 4. * ypi * dfso4(il,l) * fgr * rhoa(il,l)
          ct1(il,l,is) = cterm * (afl - dafdr * dryr(is))
          ct2(il,l,is) = cterm * dafdr
          !
          !         * terms for calculation of growth factor (cg, m5/kg/sec).
          !
          cterm = dfso4(il,l) * fgr * wrat/ddn(il,l,is)
          cgr1(il,l,is) = cterm * (afl - dafdr * dryr(is))
          cgr2(il,l,is) = cterm * dafdr
        end if
      end do
    end do
  end do
  rmom1 = 1.
  rmom2 = 2.
  dbl = pn0
  cndgp = dbl * (ct1 * sdint(phi0,psi,rmom1,phiss,dphis,ilga,leva,isec) &
          + ct2 * sdint(phi0,psi,rmom2,phiss,dphis,ilga,leva,isec))
  cndgp = max(0._r8,cndgp)
  !
end subroutine grwtrm
