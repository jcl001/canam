!
module coadat
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     brownian coagulation coefficient (kernel) for aerosol coagulation
  !     calculations.
  !
  !     history:
  !     --------
  !     * may 7/2009 - k.vonsalzen   new.
  !
  !-----------------------------------------------------------------------
  !
  implicit none
  integer :: ida !<
  integer :: idb !<
  integer :: idc !<
  integer :: idd !<
  real, allocatable, dimension(:,:,:,:) :: cker !<
  real, allocatable, dimension(:) :: cpre !<
  real, allocatable, dimension(:) :: ctem !<
  real, allocatable, dimension(:) :: cphi !<
  !
end module coadat
