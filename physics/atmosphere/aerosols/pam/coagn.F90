subroutine coagn(dndts,kern,fmom0,fmom3,fmom0i,fmom0d,ilga,leva)
  !-----------------------------------------------------------------------
  !     purpose:
  !     --------
  !     various terms in coagulation equation for aerosol number.
  !
  !     history:
  !     --------
  !     * feb 10/2010 - k.vonsalzen   newly installed.
  !
  !-----------------------------------------------------------------------
  !
  use sdparm
  use fpdef
  !
  implicit none
  integer :: ik
  integer, intent(in) :: ilga
  integer :: ir1
  integer :: ir2
  integer :: irx
  integer :: is
  integer :: its
  integer :: itsm
  integer, intent(in) :: leva
  !
  real, intent(out), dimension(ilga,leva,isfint,isaint) :: dndts !<
  real(r8), intent(in), dimension(ilga,leva,isfint) :: fmom0 !<
  real(r8), intent(in), dimension(ilga,leva,isfint) :: fmom3 !<
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom0i !<
  real(r8), intent(in), dimension(ilga,leva,isftrim) :: fmom0d !<
  real, intent(in), dimension(ilga,leva,isftri) :: kern !<
  real(r8), dimension(ilga,leva) :: dndtt !<
  !
  !-----------------------------------------------------------------------
  !
  dndts = 0.
  !
  !-----------------------------------------------------------------------
  !     * first, third and fifth term in number tendency equation.
  !
  do is = 2,isfint
    ir1 = is - 1
    ir2 = ir1
    its = itr(ir1,ir2)
    dndtt = .5 * kern(:,:,its) * fmom0(:,:,ir1) ** 2
    irx = iro(ir1,ir2)
    dndts(:,:,is - 1,irx) = dndts(:,:,is - 1,irx) - 2. * dndtt
    dndts(:,:,is  ,irx) = dndts(:,:,is  ,irx) + dndtt
  end do
  is = isfint + 1
  ir1 = is - 1
  ir2 = ir1
  its = itr(ir1,ir2)
  dndtt = .5 * kern(:,:,its) * fmom0(:,:,ir1) ** 2
  irx = iro(ir1,ir2)
  dndts(:,:,is - 1,irx) = dndts(:,:,is - 1,irx) - 2. * dndtt
  !
  !     * second and fourth term in number tendency equation.
  !
  do is = 3,isfint
    ir1 = is - 1
    do ik = 1,is - 2
      ir2 = ik
      its = itr(ir1,ir2)
      itsm = itrm(ir1,ir2)
      dndtt = fmom0(:,:,ir2) * kern(:,:,its) * fmom0i(:,:,itsm) &
              + fmom3(:,:,ir2) * kern(:,:,its) * fmom0d(:,:,itsm)
      irx = iro(ir1,ir2)
      dndts(:,:,is - 1,irx) = dndts(:,:,is - 1,irx) - dndtt
      dndts(:,:,is  ,irx) = dndts(:,:,is  ,irx) + dndtt
    end do
  end do
  is = isfint + 1
  ir1 = is - 1
  do ik = 1,is - 2
    ir2 = ik
    its = itr(ir1,ir2)
    itsm = itrm(ir1,ir2)
    dndtt = fmom0(:,:,ir2) * kern(:,:,its) * fmom0i(:,:,itsm) &
            + fmom3(:,:,ir2) * kern(:,:,its) * fmom0d(:,:,itsm)
    irx = iro(ir1,ir2)
    dndts(:,:,is - 1,irx) = dndts(:,:,is - 1,irx) - dndtt
  end do
  !
  !     * sixth term in number tendency equation.
  !
  do is = 1,isfint
    ir1 = is
    do ik = 1,is - 1
      ir2 = ik
      its = itr(ir1,ir2)
      dndtt = - kern(:,:,its) * fmom0(:,:,ir1) * fmom0(:,:,ir2)
      irx = iro(ir1,ir2)
      dndts(:,:,ik,irx) = dndts(:,:,ik,irx) + dndtt
    end do
  end do
  !
end subroutine coagn
