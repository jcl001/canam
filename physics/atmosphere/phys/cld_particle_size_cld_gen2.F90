!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine cld_particle_size_cld_gen2(rel_sub, rei_sub, & ! output
                                      cdd, radeqvw, radeqvi, & ! input
                                      clw_sub, cic_sub, &
                                      il1, il2, ilg, lay, lev, &
                                      nx_loc, ipph_re)

  !      * feb 12/2009 - jason cole.  new version for gcm15h:
  !      *                            - hard-coded value of 75.46 for
  !      *                              liquid equivilent radius is
  !      *                              now properly decomposed into
  !      *                              pifac=62.035 and the "TUNABLE"
  !      *                              factor beta (increased to 1.3
  !      *                              to give better agreement with
  !      *                              observations of liquid equivilent
  !      *                              radius).
  !      * dec 12/2007 - jason cole.  previous version
  !      *                            cld_particle_size_cld_gen for gcm15g:
  !      *                            - initial version of cloud generator
  !      *                              for mcica.

  implicit none

  !
  ! input data
  !

  real, intent(in) , dimension(ilg,lay,nx_loc) :: clw_sub ! subgrid cloud liquid water\f$[units]\f$
  real, intent(in) , dimension(ilg,lay,nx_loc) :: cic_sub !< Subgrid cloud ice water\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: cdd !< Cloud liquid water droplet number concentration\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: radeqvw !< Liquid cloud particle size\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: radeqvi !< Ice cloud particle size\f$[units]\f$

  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay   !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev   !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: nx_loc   !< Number of cloud subcolumns in an atmospheric column \f$[unitless]\f$

  integer, intent(in) :: ipph_re !< Variable description\f$[units]\f$

  !
  ! output data
  !

  real, intent(out) , dimension(ilg,lay,nx_loc) :: rel_sub !< Variable description\f$[units]\f$
  real, intent(out) , dimension(ilg,lay,nx_loc) :: rei_sub !< Variable description\f$[units]\f$

  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  ! local data
  !

  real :: wcdw
  real :: wcdi
  real :: rei_loc
  real :: rel_loc
  real :: cic_loc
  real :: pcdnc
  real :: rog

  integer :: il
  integer :: ilay
  integer :: icol

  !
  ! parameters
  !

  real, parameter :: third = 0.3333333333333

  ! the following parameters are defined in clouds16. make sure these are
  ! consistent !

  real, parameter :: cicmin = 0.00001
  real, parameter :: clwmin = 0.00001
  real, parameter :: rieffmax = 50.0
  real, parameter :: rweffmin = 2.0
  real, parameter :: rweffmax = 30.0
  real, parameter :: pifac = 62.035
  real, parameter :: beta = 1.3
  !-------------------------------------------------------------------------
  if (ipph_re == 0) then  ! horizontally homogeneous cloud particle effective size

    ! assign the horizontally constant particle sizes to each cloudy cell
    ! in the generated subcolumns

    do icol = 1, nx_loc
      do ilay = 1, lay
        do il = il1, il2
          rel_sub(il,ilay,icol) = radeqvw(il,ilay)
          rei_sub(il,ilay,icol) = radeqvi(il,ilay)
        end do ! il
      end do ! ilay
    end do ! icol

  else if (ipph_re == 1) then ! horizontally inhomogeneous cloud particle sizes

    do icol = 1, nx_loc
      do ilay = 1, lay
        do il = il1, il2
          rel_loc = beta * pifac &
                    * (clw_sub(il,ilay,icol)/cdd(il,ilay)) ** third
          rel_sub(il,ilay,icol) = &
                                  max(min(rel_loc,rweffmax),rweffmin)
          cic_loc = max(cic_sub(il,ilay,icol),cicmin)
          rei_loc = 83.8 * cic_loc ** 0.216
          rei_sub(il,ilay,icol) = min(rei_loc,rieffmax)
        end do ! il
      end do ! ilay
    end do ! icol
  end if

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
