!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine holefil3(qrow,qaddrol,qmin,ilg,lonsl,il1,il2)
  !
  !     * jun 27/2006 - m.lazare.  new version for gcm15f:
  !     *                          - qaddrol and qmin are native real.
  !     *                          - use variable instead of constant
  !     *                            in intrinsics such as "MAX", so
  !     *                            that can compile in 32-bit mode
  !     *                            with real(8).
  !     * nov 01/04 - m.lazare.    previous version holefil2.
  !
  !     * subroutine to fill holes for a given level, along a
  !     * single longitude pass (chained latitudes are taken into
  !     * account).
  !
  !     * if there is sufficient moisture to borrow from other points
  !     * in the longitude pass, this is done to minimize global
  !     * correction subsequently done in spectral space.
  !
  implicit none
  integer :: i
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: j
  integer :: k
  integer, intent(inout) :: lonsl
  integer :: nlatj
  real :: qd1
  real :: qd2
  real :: qdef
  real :: qdif
  real :: qexc
  real :: qnew
  real :: qold
  real :: ratio
  real :: sumqdef
  real :: sumqex
  real :: zero
  !
  real, intent(inout), dimension(ilg) :: qrow   !< Physics internal field for specific humidity, including moon layer at atmospheric model top \f$[kg kg^{-1}]\f$
  real, intent(inout), dimension(ilg) :: qaddrol !< Variable description\f$[units]\f$
  real, intent(in)   :: qmin !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  data zero /0./
  !----------------------------------------------------------------
  !     * calculate number of chained latitudes.
  !
  nlatj = il2/lonsl
  !
  !     * holefilling correction for each latitude.
  !
  do j = 1,nlatj
    sumqex = 0.0
    sumqdef = 0.0
    !
    do k = 1,lonsl
      i       = (j - 1) * lonsl + k
      qd1     = qrow(i) - qmin
      qd2     = qmin - qrow(i)
      qexc    = max(qd1,zero)
      qdef    = max(qd2,zero)
      sumqex  = sumqex  + qexc
      sumqdef = sumqdef + qdef
    end do
    !
    if (sumqex == 0.) then
      do k = 1,lonsl
        i          = (j - 1) * lonsl + k
        qold       = qrow(i)
        qrow(i)    = qmin
        qaddrol(i) = qrow(i) - qold
      end do
    else if (sumqex > sumqdef) then
      ratio = max(sumqex - sumqdef,zero)/sumqex
      do k = 1,lonsl
        i          = (j - 1) * lonsl + k
        qold       = qrow(i)
        qdif       = qrow(i) - qmin
        qrow(i)    = qmin + ratio * max(qdif,zero)
        qaddrol(i) = qrow(i) - qold
      end do
    else if (sumqex <= sumqdef) then
      do k = 1,lonsl
        i          = (j - 1) * lonsl + k
        qold       = qrow(i)
        qnew       = qmin
        qrow(i)    = max(qrow(i),qnew)
        qaddrol(i) = qrow(i) - qold
      end do
    end if
  end do
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
