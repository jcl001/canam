!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine prep_cld_gen2(sigma_qcw, rlc_cf, rlc_cw, & ! output
                         anu, & ! input
                         il1, il2, ilg, lay)
  !
  !      * dec 12/2007 - jason cole.   new version for gcm15g:
  !      *                             - handle 3 different overlap
  !      *                               methods (defined using ioverlap).
  !      *                             - remove code related to setting
  !      *                               lpph and imaxran since now using
  !      *                               ipph and ioverlap.
  !   * j. cole. oct. 4, 2006  stripped down version for gcm15e.2 and gcm15.
  !   * j. cole, k. von salzen. june 5, 2006. modify subroutine to use new method to compute
  !                                           nu in gcm.  it is computed in cond4 and so is
  !                                           simply passed into this subroutine.
  !   * j. cole. jan 16, 2006. subroutine to set the various parameters needed
  !                            in the stochastic cloud generator.  also set
  !                            various fields diagnosed from cloud properties
  !                            such as cloud amount, cloud water content

  implicit none

  !
  ! input data
  !

  real, intent(in) , dimension(ilg,lay) :: anu !< SQUARE OF MEAN OVER STANDARD DEVIATION OF TAU (OR CLOUD WATER)\f$[units]\f$

  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay   !< Number of vertical layers \f$[unitless]\f$

  !
  ! output data
  !
  real, intent(out) , dimension(ilg,lay) :: sigma_qcw !< NORMALIZED STANDARD DEVIATION OF CLOUD CONDENSATE\f$[units]\f$
  real, intent(out) , dimension(ilg,lay) :: rlc_cf !< CLOUD FRACTION DECORRELATION LENGTHS IN (KM)\f$[units]\f$
  real, intent(out) , dimension(ilg,lay) :: rlc_cw !< CLOUD CONDENSATE DECORRELATION LENGTHS IN (KM)\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !
  ! local data
  !

  integer :: il
  integer :: kk

  ! zero out the output arrays
  do kk = 1, lay
    do il = il1, il2
      sigma_qcw(il,kk) = 0.0
      rlc_cf(il,kk)    = 0.0
      rlc_cw(il,kk)    = 0.0
    end do ! il
  end do ! kk


  ! for now, set the decorrelation lengths to be reasonable estimates
  ! update when have some clue about how to do this based on large-scale
  ! variables

  do kk = 1, lay
    do il = il1, il2
      rlc_cf(il,kk) = 2.0 ! in kilometers
      rlc_cw(il,kk) = 1.0 ! in kilometers
    end do
  end do

  ! compute the normalized standard deviation of cloud condensate
  ! using anu from cldifm3

  do kk = 1, lay
    do il = il1, il2
      if (anu(il,kk) <= 0.0) then
        sigma_qcw(il,kk) = 0.0
      else
        sigma_qcw(il,kk) = 1.0/sqrt(anu(il,kk))
      end if
    end do ! il
  end do ! kk

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
