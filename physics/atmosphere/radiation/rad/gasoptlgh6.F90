!> \file
!>\brief Compute large gas optical thicknesses at thermal wavelengths
!!
!! @author Jiangnan Li
!
subroutine gasoptlgh6(taug, gwgh, dp, ib, ig, o3, q, co2, ch4, &
                      an2o, inpt, mcont, dir, dip, dt, lev1, gh, &
                      il1, il2, ilg, lay)
  !
  !     * apr 22/2010 - j.li.     new version for gcm15i:
  !     *                         - add one extra term to bandl4gh for
  !     *                           greater accuracy.
  !     * feb 09,2009 - j.li.     previous version gasoptlgh5 for gcm15h:
  !     *                         - 3d ghg implemented, thus no need
  !     *                           for "trace" common block or
  !     *                           temporary work arrays to hold
  !     *                           mixing ratios of ghg depending on
  !     *                           a passed, specified option.
  !     *                         - calls tline{1,2,3}z instead of
  !     *                           tline{1,2,3}y.
  !     * apr 18,2008 - m.lazare/ previous version gasoptlgh4 for gcm15g:
  !     *               j.li.     - cosmetic change to use scalar variable
  !     *                           "initaug" (=2) in calls to tline1y
  !     *                           instead of the actual number itself.
  !     *                           similar cosmetic change to use ntl(=28)
  !     *                           scalar variable instead of the actual
  !     *                           number itself in calls to all "tline_"
  !     *                           routines.
  !     *                         - calls tline{1,2,3}y instead of
  !     *                           tline{1,2,3}x.
  !     * may 05/2006 - m.lazare. previous version gasoptlgh3 for gcm15e/f:
  !     *                         - calls new versions of:
  !     *                           tline1x,tline2x,tline3y,tcontl1,
  !     *                           tconthl1.
  !     * dec 07/2004 - j.li. previous version gasoptlgh2 for gcm15c/gcm15d:
  !     *                     bugfix to reverse cl4n2ogh and cl4ch4gh
  !     *                     in bandl4gh common block to be consistent
  !     *                     with data defined in ckdlw2.
  !     * apr 25/2003 - j.li. previous version gasoptlgh for gcm15b.
  !----------------------------------------------------------------------c
  !     the same as gasoptl but for intervals close to 1 in the          c
  !     accumulated probability space                                    c
  !     tline, etc., deal with line absorption and tcontl and tconthl    c
  !     deal with water vapor continuum                                  c
  !                                                                      c
  !     taug: gaseous optical depth                                      c
  !     dp:   air mass path for a model layer (exlained in raddriv).     c
  !     o3:   o3 mass mixing ratio                                       c
  !     q:    water vapor mass mixing ratio                              c
  !     an2o: n2o, also co2, ch4, f11, f12, f113, f114 are mass mixing   c
  !           ratios                                                     c
  !     dip:  interpretation factor for pressure between two             c
  !           neighboring standard input data pressure levels            c
  !     dt:   layer temperature - 250 k                                  c
  !     inpt: number of the level for the standard input data pressures  c
  !----------------------------------------------------------------------c
  implicit none
  real, dimension(5,28,5) :: cl1co2gh
  real, dimension(5,3) :: cl2cfgh
  real, dimension(5,3) :: cl2csgh
  real, dimension(5,28) :: cl2h2ogh
  real, dimension(5,4) :: cl3cfgh
  real, dimension(5,4) :: cl3csgh
  real, dimension(5,28,3) :: cl3h2ogh
  real, dimension(5,28,5) :: cl4ch4gh
  real, dimension(5,28,5) :: cl4h2ogh
  real, dimension(5,28,5) :: cl4n2ogh
  real, dimension(5,4,2) :: cl5cfgh
  real, dimension(5,4,2) :: cl5csgh
  real, dimension(5,28,4) :: cl5h2ogh
  real, dimension(5,28,4) :: cl5o3gh
  real, dimension(5,28,7) :: cl7co2gh
  real, dimension(5,28,4) :: cl7h2ogh
  real, dimension(2) :: cl7o3gh
  real, dimension(5,28,3) :: cl8h2ogh
  real, dimension(5,28,6) :: cl9h2ogh
  real, dimension(5) :: gw1gh
  real, dimension(1) :: gw2gh
  real, dimension(3) :: gw3gh
  real, dimension(5) :: gw4gh
  real, dimension(4) :: gw5gh
  real, dimension(7) :: gw7gh
  real, dimension(3) :: gw8gh
  real, dimension(6) :: gw9gh
  real, intent(inout) :: gwgh
  integer :: i
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: initaug
  integer :: k
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer :: lc
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer, intent(in) :: mcont
  integer :: ntl
  !
  real, intent(inout), dimension(ilg,lay) :: taug !< Gas optical depth \f$[1]\f$
  !
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path of a layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg,lay) :: q !< H2O mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: co2 !< CO2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ch4 !< CH4 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: an2o !< N2O mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: dir !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,lay) :: dip !< Interpretation between two neighboring standard input pressure levels \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dt !< layer temperature - 250 K \f$[K]\f$
  integer, intent(in), dimension(ilg,lay) :: inpt !< level number of the standard input pressures \f$[0]\f$
  logical, intent(in) :: gh !< If is true, use large gaseous optical thickness group for calculations \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  common /bandl1gh/  gw1gh,  cl1co2gh
  common /bandl2gh/  gw2gh,  cl2h2ogh, cl2csgh, cl2cfgh
  common /bandl3gh/  gw3gh,  cl3h2ogh, cl3csgh, cl3cfgh
  common /bandl4gh/  gw4gh,  cl4h2ogh, cl4n2ogh, cl4ch4gh
  common /bandl5gh/  gw5gh,  cl5h2ogh, cl5o3gh, cl5csgh, cl5cfgh
  !     (no band 6 for gh)
  common /bandl7gh/  gw7gh,  cl7h2ogh, cl7co2gh, cl7o3gh
  common /bandl8gh/  gw8gh,  cl8h2ogh
  common /bandl9gh/  gw9gh,  cl9h2ogh
  !
  !     * number of vertical levels in absorber pressure-based coefficient
  !     * array.
  !
  data ntl /28/
  !
  !     * initaug is a switch used in tline1y (as "iplus") which
  !     * initializes taug to zero if its value is two. this is what
  !     * we require throughout this routine.
  !
  data initaug /2/
  !=======================================================================
  if (ib == 1) then
    !
    !----------------------------------------------------------------------c
    !     band (2500 - 2200 cm-1), nongray gaseous absorption of co2.     c
    !----------------------------------------------------------------------c
    !
    call tline1z(taug, cl1co2gh(1,1,ig), co2, dp, dip, dt, inpt, &
                 lev1, gh, ntl, initaug, il1, il2, ilg, lay)
    !
    gwgh =  gw1gh(ig)
    !
  else if (ib == 2) then
    !
    !----------------------------------------------------------------------c
    !     band (2200 - 1900 cm-1), nongray gaseous absorption of h2o      c
    !----------------------------------------------------------------------c
    !
    call tline1z(taug, cl2h2ogh, q, dp, dip, dt, inpt, &
                 lev1, gh, ntl, initaug, il1, il2, ilg, lay)
    !
    lc =  3
    call tcontl1(taug, cl2csgh, cl2cfgh, q, dp, dip, dt, lc, inpt, &
                 mcont, gh, il1, il2, ilg, lay)
    !
    gwgh =  gw2gh(ig)
    !
  else if (ib == 3) then
    !
    !----------------------------------------------------------------------c
    !     band (1900 - 1400 cm-1), nongray gaseous absorption of h2o.     c
    !----------------------------------------------------------------------c
    !
    call tline1z(taug, cl3h2ogh(1,1,ig), q, dp, dip, dt, inpt, &
                 lev1, gh, ntl, initaug, il1, il2, ilg, lay)
    !
    if (ig == 1) then
      lc =  4
      call tcontl1(taug, cl3csgh, cl3cfgh, q, dp, dip, dt, lc, inpt, &
                   mcont, gh, il1, il2, ilg, lay)
      !
    end if
    !
    gwgh =  gw3gh(ig)
    !
  else if (ib == 4) then
    !
    !----------------------------------------------------------------------c
    !     band3 (1100 - 1400 cm-1), overlapping absorption of h2o, n2o,   c
    !     and ch4. direct mapping method for h2o and ch4 and n2o           c
    !----------------------------------------------------------------------c
    !
    call tline3z(taug, cl4h2ogh(1,1,ig), cl4ch4gh(1,1,ig), &
                 cl4n2ogh(1,1,ig), q, ch4, an2o, dp, dip, dt, inpt, &
                 lev1, gh, ntl, il1, il2, ilg, lay)
    !
    gwgh =  gw4gh(ig)
    !
  else if (ib == 5) then
    !
    !----------------------------------------------------------------------c
    !     band5 (980 - 1100 cm-1), overlapping absorption of h2o and o3   c
    !     direct mapping method                                            c
    !----------------------------------------------------------------------c
    !
    call tline2z(taug, cl5h2ogh(1,1,ig), cl5o3gh(1,1,ig), q, o3, &
                 dp, dip, dt, inpt, lev1, gh, ntl, &
                 il1, il2, ilg, lay)
    !
    if (ig <= 2) then
      lc =  4
      call tcontl1(taug, cl5csgh(1,1,ig), cl5cfgh(1,1,ig), q, dp, dip, &
                   dt, lc, inpt, mcont, gh, il1, il2, ilg, lay)
    end if
    !
    gwgh =  gw5gh(ig)
    !
    !----------------------------------------------------------------------c
    !     band (800 - 980 cm-1), no gh                                    c
    !----------------------------------------------------------------------c
    !
  else if (ib == 7) then
    !
    !----------------------------------------------------------------------c
    !     band6 (540 - 800 cm-1), overlapping absorption of h2o and co2   c
    !     direct mapping method. for ig > 4, the contribution by h2o is    c
    !     very small.                                                      c
    !----------------------------------------------------------------------c
    !
    if (ig <= 4) then
      call tline2z(taug, cl7h2ogh(1,1,ig), cl7co2gh(1,1,ig), q, co2, &
                   dp, dip, dt, inpt, lev1, gh, ntl, &
                   il1, il2, ilg, lay)

      !----------------------------------------------------------------------c
      !     simply add the o3 effect                                         c
      !----------------------------------------------------------------------c
      !
      if (ig <= 2) then
        do k = 1, lay
          do i = il1, il2
            taug(i,k) =  taug(i,k) + cl7o3gh(ig) * o3(i,k) * dp(i,k)
          end do
        end do ! loop 700
      end if
    else
      !
      call tline1z(taug, cl7co2gh(1,1,ig), co2, dp, dip, dt, inpt, &
                   lev1, gh, ntl, initaug, il1, il2, ilg, lay)
    end if
    !
    gwgh =  gw7gh(ig)
    !
  else if (ib == 8) then
    !
    !----------------------------------------------------------------------c
    !     band (340 - 540 cm-1), nongray gaseous absorption of h2o.       c
    !----------------------------------------------------------------------c
    !
    call tline1z(taug, cl8h2ogh(1,1,ig), q, dp, dip, dt, inpt, &
                 lev1, gh, ntl, initaug, il1, il2, ilg, lay)
    !
    gwgh =  gw8gh(ig)
    !
  else if (ib == 9) then
    !
    !----------------------------------------------------------------------c
    !     band (0 - 340 cm-1), nongray gaseous absorption of h2o.         c
    !----------------------------------------------------------------------c
    !
    call tline1z(taug, cl9h2ogh(1,1,ig), q, dp, dip, dt, inpt, &
                 lev1, gh, ntl, initaug, il1, il2, ilg, lay)
    !
    gwgh =  gw9gh(ig)
    !
  end if
  !
  return
end
!> \file
!> Compute gas optical thickness for correlated \f$k\f$-distribution quadrature points
!! close to 1 in the accumulated probability space, where the gaseous absorption
!! coeffcient is several order larger than the \f$k\f$ intervals not
!! 1 in the accumulated probability space. For such large absorption,
!! the radiative transfer can be much simplified.      
!! using tlinE, etc. to deal with line absorption and tcontl to
!! deal with water vapor continuum  