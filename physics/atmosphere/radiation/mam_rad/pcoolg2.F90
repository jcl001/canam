!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine pcoolg2(h,t,co2,o2,sn2,o,am,zco2o,flux, &
                         su,lambda,h2,ilg,il1,il2)

  !****************************************************************************
  !*                                                                          *
  !*                       subroutine  pcoolg2                                *
  !*                                                                          *
  !****************************************************************************
  !
  !  modified from pcoolg to be used with ckd scheme:
  !    (1) 9.6 um o3 band is taken out (results from the ckd scheme are used)
  !                                   july, 2004. v. i. fomichev
  !
  !  to calculate the heating rate in both, 15 um co2 and 9.6 um o3 bands.
  !  nlte conditions are taken into account for co2 band. irregular vertical
  !  grid, to account for heat exchange in the "LTE-layer" (x=2-12.5), is used.
  !  the subroutine can be used for an arbitrary co2 volume mixing ratio
  !  with the parameterization coefficients calculated by the cco2gr.
  !
  !                                    november, 1996.  v.i. fomichev

  ! called by mamrad2
  ! calls nothing

  implicit none
  real :: a10
  real :: aku
  real :: al
  real :: amat
  real :: ao3
  real :: bmat
  real :: boltz
  real :: const
  real :: constb
  real :: d
  real :: d1
  real :: d2
  real :: hh
  integer :: i
  integer :: ig
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer :: im
  integer :: is
  integer :: j
  integer :: js
  real :: tt
  real :: x
  real :: xh
  real :: y
  real :: z
  real, intent(in) :: zco2o
  real :: zn2
  real :: zo2

  ! input:
  ! t(longitude,67)  - temperature (k) at x(=0-16.5, 0.25) levels
  ! CO2,O2,N2,O(17) - volume mixing ratios (vmr's) for corresponding gases
  !                   at x = 12.5 - 16.5, with a step of 0.25
  ! am(17) - air molecular weight at x = 12.5 - 16.5, 0.25
  ! zco2o    - collisional deactivation rate constant (in cm3/sec)
  ! ilg = il1, il2 - longitude indexes
  ! parameterization coeficients for co2 (calculated by cco2gr) come from
  ! the co2cfg and piro3 common blocks.
  ! the following data are defined in the block data dpmco2:
  ! irregular vertical grid (ig(9)) comes from the pirmgr common block
  ! pressure scale height arrays (0(2)-16.5,0.25) - pirgrd common block
  ! some constants for cooling rate calculation - pircons common block

  ! output:
  !  h(ilg,59) - heating rates in erg/g/sec at x = 2-16.5, 0.25
  !  flux(ilg) - upward flux at x=16.5 to calculate the heating rate
  !              above this level.
  !
  ! note: 1) actually, any arbitrary vertical grid could be utilized above
  !          from x=13.75. to do so, the coefficients for the reccurence
  !          formula must be determined at a proper grid (using cco2gr) and all
  !          input information should be given at this grid too.
  !       2) as of now, zco2o is recommended to vary in a range of (1.5-6)e-12
  !          with a mean value of 3e-12 cm3/sec, without t-dependence
  !       3) to get the heating rate values in k/sec, the heating rates
  !          calculated here should be divided by cp - specific heat at constant
  !          pressure. where cp = r/am * {7/2*sum[rv,i - for 2-atom gases] +
  !          5/2*SUM[Rv,i - for 1-atom gases]}, R - gas constant, Rv - vmr's.

  real, intent(inout), dimension(ilg,59) :: h !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg) :: flux !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,67) :: t !< Variable description\f$[units]\f$
  real, intent(in), dimension(17) :: co2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(17) :: o2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(17) :: sn2 !< Variable description\f$[units]\f$
  real, intent(in), dimension(17) :: o !< Variable description\f$[units]\f$
  real, intent(in), dimension(17) :: am !< Variable description\f$[units]\f$

  common /co2cfg/ amat(43,9),bmat(43,9),al(17)
  common /pirmgr/ ig(9)
  common /piro3/  ao3(35,9)
  common /pirgrd/ x(67),xh(59)
  common /pircons/ const, constb, boltz, a10, aku

  ! internal arrays:

  ! su(ilg,67)  - exponential part of the planck function for co2 band
  !               at x(67) grid
  ! lambda(ilg,17) - quantum survival probability at the x grid for levels
  !                  above x=12.5 (indexes 51-67)

  real, intent(inout), dimension(ilg,67) :: su !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,17) :: lambda !< Variable description\f$[units]\f$

  ! auxiliary arrays
  !
  real, intent(inout), dimension(ilg) :: h2 !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !

  ! to determine su(ilg,67)

  do i = 1,67
    do il = il1, il2
      su(il,i)=exp(aku/t(il,i))
    end do ! loop 4
  end do ! loop 3

  ! to determine lambda

  do i=1,17
    do il = il1, il2

      ! ----- co2-o2 and co2-n2 v-t constants:
      tt=t(il,i+50)
      y=tt**(-1./3.)
      zn2=5.5e-17*sqrt(tt)+6.7e-10*exp(-83.8*y)
      zo2=1.e-15*exp(23.37-230.9*y+564.*y*y)

      ! ----- air number density:
      d=1.e6*exp(-x(i+50))/(tt*boltz)

      ! ----- collisional deactivation rate:
      z=(sn2(i)*zn2+o2(i)*zo2+o(i)*zco2o)*d

      lambda(il,i) = a10/(a10+z)
    end do ! loop 6
  end do ! loop 5

  ! cooling rate in the 15 um co2 band (x=2-12.5, matrix approach is used)

  do i=1,5
    is = i+8
    do il = il1, il2
      h(il,i)=(amat(i,1)+bmat(i,1)*su(il,is))*su(il,1)
    end do ! loop 8
    do j=3,9
      js=is+ig(j)
      do il = il1, il2
        h(il,i)=h(il,i)+(amat(i,j)+bmat(i,j)*su(il,is))*su(il,js)
      end do ! loop 10
    end do ! loop 9
  end do ! loop 7

  do i=6,18
    is = i+8
    do il = il1, il2
      h(il,i)=(amat(i,1)+bmat(i,1)*su(il,is))*su(il,1)
    end do ! loop 13
    do j=2,9
      js=is+ig(j)
      do il = il1, il2
        h(il,i)=h(il,i)+(amat(i,j)+bmat(i,j)*su(il,is))*su(il,js)
      end do ! loop 15
    end do ! loop 14
  end do ! loop 12

  do i=19,43
    is = i+8
    do il = il1, il2
      h(il,i)=0.
    end do ! loop 18
    do j=1,9
      js=is+ig(j)
      do il = il1, il2
        h(il,i)=h(il,i)+(amat(i,j)+bmat(i,j)*su(il,is))*su(il,js)
      end do ! loop 20
    end do ! loop 19
  end do ! loop 17

  ! calculate the heating rate for x=12.75-16.5 (the reccurence formula is used)

  ! --- to form the boundary condition at x=12.5
  do il = il1, il2
    h2(il)=h(il,43)/(co2(1)*(1.-lambda(il,1))*constb)
  end do ! loop 26

  ! --- the reccurence formula
  do i=2,17
    im=i-1
    d1=-.25*(al(i)+3.*al(im))
    d2=.25*(3.*al(i)+al(im))
    do il = il1, il2
      hh=((1.-lambda(il,im)*(1.+d1))*h2(il)-d1*su(il,im+50)- &
                      d2*su(il,i+50))/(1.-lambda(il,i)*(1.-d2))
      h2(il)=hh
      h(il,i+42)=hh*co2(i)*(1.-lambda(il,i))/am(i)*const
    end do ! loop 28
  end do ! loop 27

  ! to determine flux
  ! cooling rate above x=16.5 is suggested to be calculated by equation
  !           h(i) = const/am(i)*co2(i)*(1.-lambda(i))*(flux-su(i))
  ! no any parameterization coefficients are needed and an arbitrary hight grid
  ! can be used above x=16.5 level

  do il = il1, il2
    flux(il) = h2(il) + su(il,67)
  end do ! loop 29

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
