!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine cco2gr(rco2)

  !****************************************************************************
  !*                                                                          *
  !*                       subroutine cco2gr                                  *
  !*                                                                          *
  !****************************************************************************
  !
  !  to calculate parameterization coeficients for 15 um co2 band cooling
  !  rate calculation for arbitrary co2 volume mixing ratio (with height
  !  variation as of the basic co2 profile for 360 ppm) in a range of 150-720ppm.
  !  coefficients are calculated for atmosphere layer from the pressure scale
  !   height x=2 up to x=16.5. irregular vertical grid is used to acconut for
  !   internal heat exchange within "LTE layer" (x=0-12.5)
  !  ***** important !! !! ! *****
  !  from x=2 up to x=13.75 coeficients can be calculated only for a vertical
  !  grid with a step of 0.25. eventually, any vertical step could be used for
  !  atmospheric layer atmosphere the level of x=13.75. however, to do it,
  !  some modification is needed. no coeficients are needed to calculate
  !  cooling rates above x=16.5
  !
  !                                    may, 1996.  v.i. fomichev.

  ! called by mamrad
  ! calls a18lin and a18int


  ! input:
  !  rco2 - co2 volume mixing ration in the troposphere
  !  initial data from block data pco2o3 coming through common blocks

  ! output: parameterization coefficients for both, the matrix parameterization
  !         (is used between x=2 and 12.5) and reccurence formula.
  !         passing to other subroutines through common block co2cfg.
  !  amat,bmat(43,9) - coefficients for the matrix parameterization
  !  al(17) - coeficients for the reccurence formula. note that starting up
  !           from x=13.75, these coefficients are identical to escape functions
  !           which could be calculated at any arbitrary vertical grid.
  !           so that starting up from this level any arbitrary vertical grid
  !           could be used.

  implicit none
  real :: a
  real :: a18lin
  real :: a150
  real :: a360
  real :: a540
  real :: a720
  real :: al
  real :: alo
  real :: amat
  real :: b150
  real :: b360
  real :: b540
  real :: b720
  real :: bmat
  real :: co2cl
  real :: co2o
  real :: co2vp
  real :: cor
  real :: cor150
  real :: cor360
  real :: cor540
  real :: cor720
  integer :: i
  integer :: ig
  integer :: isgn
  integer :: j
  real, intent(in) :: rco2
  real :: uco2
  real :: uco2co
  real :: uco2ro

  common /co2cfg/ amat(43,9),bmat(43,9),al(17)

  ! data from subroutine pco2o3

  common /pirco2/ co2vp(67), co2cl(67)
  common /pirlte/ a150(43,9),b150(43,9), a360(43,9),b360(43,9), &
                 a540(43,9),b540(43,9), a720(43,9),b720(43,9), &
                 co2o(4)
  common /pirmgr/ ig(9)
  common /pirne/ uco2ro(51),alo(51), &
                cor150(6),cor360(6),cor540(6),cor720(6),uco2co(6)
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  ! auxiliary arrays

  real :: uref(4) !<
  real :: co2int(4) !<

  ! interplolate coefficients for the matrix paramerization:

  do i = 1,43
    do j = 1,9
      if ((i<=5).and.(j==2)) cycle
      isgn = int(sign(1.,a150(i,j))+sign(1.,a360(i,j))+ &
             sign(1.,a540(i,j))+sign(1.,a720(i,j)))
      co2int(1)=a150(i,j)/co2o(1)
      co2int(2)=a360(i,j)/co2o(2)
      co2int(3)=a540(i,j)/co2o(3)
      co2int(4)=a720(i,j)/co2o(4)
      if (isgn==-4) then
        co2int(1) = log(-co2int(1))
        co2int(2) = log(-co2int(2))
        co2int(3) = log(-co2int(3))
        co2int(4) = log(-co2int(4))
        a = -exp(a18lin(rco2,co2o,co2int,1,4))
      else if (isgn==4) then
        co2int(1) = log(co2int(1))
        co2int(2) = log(co2int(2))
        co2int(3) = log(co2int(3))
        co2int(4) = log(co2int(4))
        a = exp(a18lin(rco2,co2o,co2int,1,4))
      else
        call a18int(co2o,co2int,rco2,a,4,1)
      end if
      amat(i,j)=a*rco2

      isgn = int(sign(1.,b150(i,j))+sign(1.,b360(i,j))+ &
             sign(1.,b540(i,j))+sign(1.,b720(i,j)))
      co2int(1)=b150(i,j)/co2o(1)
      co2int(2)=b360(i,j)/co2o(2)
      co2int(3)=b540(i,j)/co2o(3)
      co2int(4)=b720(i,j)/co2o(4)
      if (isgn==-4) then
        co2int(1) = log(-co2int(1))
        co2int(2) = log(-co2int(2))
        co2int(3) = log(-co2int(3))
        co2int(4) = log(-co2int(4))
        a = -exp(a18lin(rco2,co2o,co2int,1,4))
      else if (isgn==4) then
        co2int(1) = log(co2int(1))
        co2int(2) = log(co2int(2))
        co2int(3) = log(co2int(3))
        co2int(4) = log(co2int(4))
        a = exp(a18lin(rco2,co2o,co2int,1,4))
      else
        call a18int(co2o,co2int,rco2,a,4,1)
      end if
      bmat(i,j)=a*rco2
    end do
  end do ! loop 1

  ! calculate coeeficients for the reccurence formula:
  ! between x=12.5 and 13.75 these coefficients (al) are calculated using
  ! correction to escape function. starting up from x=14.00 parameterization
  ! coeficients equal escape function.

  do i=1,6
    uco2 = uco2co(i)*rco2/3.6e-4
    a = a18lin(uco2,uco2ro,alo,1,51)
    co2int(1)=cor150(i)
    co2int(2)=cor360(i)
    co2int(3)=cor540(i)
    co2int(4)=cor720(i)
    uref(1) =uco2co(i)*150./360.
    uref(2) =uco2co(i)
    uref(3) =uco2co(i)*540./360.
    uref(4) =uco2co(i)*720./360.
    cor = a18lin(uco2,uref,co2int,1,4)
    al(i)=exp(cor+a)
  end do ! loop 2

  do i=7,17
    uco2 = co2cl(i+50)*rco2/3.6e-4
    a = a18lin(uco2,uco2ro,alo,1,51)
    al(i)=exp(a)
  end do ! loop 3

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
