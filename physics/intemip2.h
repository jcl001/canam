!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * nov 19, 2012 - k.vonsalzen.  new version for gcm17+:
!     *                              clean-up and merging with pla code.
!     * apr 27, 2010 - k.vonsalzen.  previous version intemi up to gcm16.
!     * interpolate tracer emission.
!---------------------------------------------------------------------
!     * interpolate basic tracer emission arrays.
!
call intcac(eostrow,eostrol, &
                  il1,il2,ilg,ilev,delt,gmt,iday,mdayt)
!
#if defined transient_aerosol_emissions
      call intprf(fbbcrow,fbbcrol,fairrow,fairrol, &
             ilg,il1,il2,levwf,levair,delt,gmt,iday,mdayt)
#endif
!---------------------------------------------------------------------
#if defined emists
#ifndef pla
!     * initialize 2d tracer emission arrays.
!
      x2demisa(il1:il2,:)=0.
      x2demiss(il1:il2,:)=0.
      x2demisk(il1:il2,:)=0.
      x2demisf(il1:il2,:)=0.
!
#endif
#endif
#if defined emists
!     * interpolate emissions.
!
      call inttemi2(sairrow,sairrol,ssfcrow,ssfcrol,sbiorow, &
               sbiorol,sshirow,sshirol,sstkrow,sstkrol, &
               sfirrow,sfirrol,oairrow,oairrol,osfcrow, &
               osfcrol,obiorow,obiorol,oshirow,oshirol, &
               ostkrow,ostkrol,ofirrow,ofirrol,bairrow, &
               bairrol,bsfcrow,bsfcrol,bbiorow,bbiorol, &
               bshirow,bshirol,bstkrow,bstkrol,bfirrow, &
               bfirrol,ilg,il1,il2,delt,gmt,iday,mdayt)
#endif
#if defined emists
#ifndef pla
!
!---------------------------------------------------------------------
!     * fill 2d tracer emisssion arrays with interpolated results.
!
      x2demisa(il1:il2,iso2)=sairrow(il1:il2)
      x2demiss(il1:il2,iso2)=ssfcrow(il1:il2)
      x2demisk(il1:il2,iso2)=sstkrow(il1:il2)
      x2demisf(il1:il2,iso2)=sfirrow(il1:il2)
!
      x2demisa(il1:il2,ioco)=.5*oairrow(il1:il2)
      x2demiss(il1:il2,ioco)=.5*osfcrow(il1:il2)
      x2demisk(il1:il2,ioco)=.5*ostkrow(il1:il2)
      x2demisf(il1:il2,ioco)=.5*ofirrow(il1:il2)
      x2demisa(il1:il2,iocy)=.5*oairrow(il1:il2)
      x2demiss(il1:il2,iocy)=.5*osfcrow(il1:il2)
      x2demisk(il1:il2,iocy)=.5*ostkrow(il1:il2)
      x2demisf(il1:il2,iocy)=.5*ofirrow(il1:il2)
!
      x2demisa(il1:il2,ibco)=.8*bairrow(il1:il2)
      x2demiss(il1:il2,ibco)=.8*bsfcrow(il1:il2)
      x2demisk(il1:il2,ibco)=.8*bstkrow(il1:il2)
      x2demisf(il1:il2,ibco)=.8*bfirrow(il1:il2)
      x2demisa(il1:il2,ibcy)=.2*bairrow(il1:il2)
      x2demiss(il1:il2,ibcy)=.2*bsfcrow(il1:il2)
      x2demisk(il1:il2,ibcy)=.2*bstkrow(il1:il2)
      x2demisf(il1:il2,ibcy)=.2*bfirrow(il1:il2)
#endif
#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
