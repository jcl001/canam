!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine tsolvl(tlak,t0,lkiceh,klak,glak,q0sat, &
                  kstar,lstar,qsens,qevap,evap,alvs,alir, &
                  qswin,qlwin,csz,ta,qa,va,pres,rhoair,cdh, &
                  gzerol,qtransl,htcl,fls,fice,albw,albi, &
                  cq1a,cq1b,cq2a,cq2b,cq3a,cq3b, &
                  cq1bi,cq2bi,cq3bi,g0, &
                  nlakmax,ilg,il1,il2)
  !======================================================================
  !     * may 30/18 - m.lazare/   - limit kstar to be positive definite
  !     *             m.mackay.     to avoid infrequent crashes.
  !     * DEC 19/16 - M.LAZARE/   - NSTEP REMOVED (WASN'T USED).
  !     *             d.verseghy. - few bugfixes.
  !     *                         - {albw,albi} now defined in classl
  !     *                           and passed in, for consistency.
  !     * apr 11/16 - m.mackay.   thermal expansion of ice bug corrected
  !     *                         ice draft,freeboard now included
  !     *                         sw attenuation through leads included
  !     * sep 02/15 - d.verseghy. add effects of snow coverage on surface
  !     *                         fluxes; additional diagnostic variables
  !     *                         cosmetic changes to code and naming.
  !     * sep  2/11 - m.mackay.    ice computed in skin layer
  !     * sep 28/07 - m.mackay.    sfc energy balance now computed over
  !     *                         skin of finite width
  !     * mar 15/07 - m.mackay.   computes lake surface energy balance
  !     *
  !
  implicit none
  !
  ! ----* global lake variables *---------------------------------------
  !
  integer, intent(in) :: nlakmax
  real, intent(inout),dimension(ilg) :: t0 !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: lkiceh !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: klak !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: glak !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: q0sat !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg,nlakmax) :: tlak !< Variable description\f$[units]\f$
  !
  ! ----* output fields *------------------------------------------------
  !
  real, intent(inout),dimension(ilg) :: kstar !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: lstar !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: qsens !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: qevap !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: evap !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: alvs !< Variable description\f$[units]\f$
  real, intent(inout),dimension(ilg) :: alir !< Variable description\f$[units]\f$
  !
  ! ----* input fields *------------------------------------------------
  !
  real, intent(in),dimension(ilg) :: qswin !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: qlwin !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: csz !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: ta !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: qa !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: va !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: pres !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: rhoair !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cdh !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: gzerol !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: qtransl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: htcl !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: fls !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: fice !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: albw !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: albi !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq1a !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq1b !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq2a !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq2b !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq3a !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq3b !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq1bi !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq2bi !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: cq3bi !< Variable description\f$[units]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  !
  ! ----* internal arrays *----------------------------------------------
  !
  real, intent(inout),dimension(ilg) :: g0 !< Variable description\f$[units]\f$
  real,dimension(ilg) :: xice !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! ----* common blocks *------------------------------------------------
  !
  real :: delt
  real :: tfrez
  real :: rgas
  real :: rgasv
  real :: grav
  real :: sbc
  real :: vkc
  real :: ct
  real :: vmin
  real :: tcw
  real :: tcice
  real :: tcsand
  real :: tcclay
  real :: tcom
  real :: tcdrys
  real :: rhosol
  real :: rhoom
  real :: hcpw
  real :: hcpice
  real :: hcpsol
  real :: hcpom
  real :: hcpsnd
  real :: hcpcly
  real :: sphw
  real :: sphice
  real :: sphveg
  real :: sphair
  real :: rhow
  real :: rhoice
  real :: tcglac
  real :: clhmlt
  real :: clhvap
  real :: cgrav
  real :: ckarm
  real :: cpd
  real :: as
  real :: asx
  real :: ci
  real :: bs
  real :: beta
  real :: factn
  real :: hmin
  real :: tkecn
  real :: tkecf
  real :: tkece
  real :: tkecs
  real :: hdpthmin
  real :: tkemin
  real :: delmax
  real :: delmin
  real :: emsw
  real :: delzlk
  real :: delskin
  real :: dhmax
  real :: tkecl
  real :: dumax
  common /class1/ delt,tfrez
  common /class2/ rgas,rgasv,grav,sbc,vkc,ct,vmin
  common /class3/ tcw,tcice,tcsand,tcclay,tcom,tcdrys, &
                 rhosol,rhoom
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  common /lakecon/ tkecn,tkecf,tkece,tkecs,hdpthmin, &
                  tkemin,delmax,delmin,emsw,delzlk,delskin,dhmax, &
                  tkecl,dumax
  !
  ! ----* local variables *--------------------------------------------
  !
  integer :: i
  integer :: j
  integer :: itmax
  real :: dz
  real :: ca
  real :: cb
  real :: e0sat
  real :: ds
  real :: albtot
  real :: t0old
  real :: newice
  real :: eskin
  real :: ecool
  real :: eavail
  real :: atten1
  real :: atten2
  real :: atten3
  real :: eheat
  real :: tc
  real :: cphch
  real :: rhoiw
  real :: icetop
  real :: icebot
  !
  ! ----* local parameter definitions *-------------------------------
  !
  dz = delzlk
  ds = delskin
  rhoiw = rhoice/rhow
  !----------------------------------------------------------------------
  do i = il1,il2

    !======================================================================
    ! compute surface energy balance for current timestep
    !======================================================================
    ! compute sw fluxes --------------------------------------------------
    !   kstar is net sw at surface of skin
    !   klak is penetrating sw at base of skin
    !
    xice(i) = max(fice(i) - fls(i),0.0)
    if (fice(i) > (fls(i) + 0.001)) then
      albtot = (xice(i) * albi(i) + (1. - fice(i)) * albw(i))/(1.0 - fls(i))
    else
      albtot = albw(i)
    end if

    kstar(i) = (1. - fls(i)) * (1. - albtot) * qswin(i) + fls(i) * qtransl(i)
    kstar(i) = max(kstar(i),0.0)
    alvs(i) = albtot
    alir(i) = albtot

    !--- attenuation through ice (now includes attenuation through leads)
    !---
    icebot = rhoiw * lkiceh(i)                ! ice draft
    icetop = lkiceh(i) - icebot               ! ice freeboard
    if (lkiceh(i) <= 0.0) then         ! no ice
      atten1 = cq1b(i) * ds
      atten2 = cq2b(i) * ds
      atten3 = cq3b(i) * ds
    else
      if (icebot > ds) then        ! z inside ice
        atten1 = fice(i) * (cq1bi(i) * (ds + icetop)) + &
                 (1. - fice(i)) * cq1b(i) * ds
        atten2 = fice(i) * (cq2bi(i) * (ds + icetop)) + &
                 (1. - fice(i)) * cq2b(i) * ds
        atten3 = fice(i) * (cq3bi(i) * (ds + icetop)) + &
                 (1. - fice(i)) * cq3b(i) * ds
      else
        atten1 = fice(i) * (cq1bi(i) * lkiceh(i) + cq1b(i) * (ds - icebot)) &
                 + (1. - fice(i)) * cq1b(i) * ds
        atten2 = fice(i) * (cq2bi(i) * lkiceh(i) + cq2b(i) * (ds - icebot)) &
                 + (1. - fice(i)) * cq2b(i) * ds
        atten3 = fice(i) * (cq3bi(i) * lkiceh(i) + cq3b(i) * (ds - icebot)) &
                 + (1. - fice(i)) * cq3b(i) * ds
      end if
    end if
    klak(i) = kstar(i) * (cq1a(i) * exp( - atten1) + &
              cq2a(i) * exp( - atten2) + &
              cq3a(i) * exp( - atten3) )
    !
    ! compute turbulent fluxes -------------------------------------------
    !     * calculation of e0sat consistent with classi
    !     * but constants differ from rogers &yau
    !     * rogers and yau values
    !         ca=17.67
    !         cb=29.65
    !
    if (t0(i) >= tfrez) then
      ca = 17.269
      cb = 35.86
    else
      ca = 21.874
      cb = 7.66
    end if

    if (fice(i) > (fls(i) + 0.001)) then
      cphch = (xice(i) * (clhmlt + clhvap) + (1. - fice(i)) * clhvap)/ &
              (1.0 - fls(i))
    else
      cphch = clhvap
    end if
    qsens(i) = (1. - fls(i)) * rhoair(i) * sphair * cdh(i) * va(i) * (t0(i) - ta(i))
    e0sat = 611.0 * exp(ca * (t0(i) - tfrez)/(t0(i) - cb))
    q0sat(i) = 0.622 * e0sat/(pres(i) - 0.378 * e0sat)
    evap(i) = (1. - fls(i)) * rhoair(i) * cdh(i) * va(i) * (q0sat(i) - qa(i))
    ! mdm    qevap(i)=cphch*evap(i)
    qevap(i) = (1. - fls(i)) * rhoair(i) * cphch * cdh(i) * va(i) * &
               (q0sat(i) - qa(i))
    !
    ! compute net lw and net sfc energy -------------------------------
    ! glak is thermal flux at base of skin.  thermal conductivity based
    ! on weighted average for water and ice if ice present in layer
    !
    lstar(i) = (1. - fls(i)) * emsw * (qlwin(i) - sbc * t0(i) * t0(i) * t0(i) * t0(i))
    g0(i) = kstar(i) - klak(i) + lstar(i) - qsens(i) - qevap(i) + htcl(i) + &
            fls(i) * gzerol(i)
    if (icebot >= ds) then
      glak(i) = ( - 2.0 * tcice/(dz + ds)) * (tlak(i,1) - t0(i))
    else if (icebot < ds .and. lkiceh(i) > 0.0) then
      tc = (icebot * tcice + (ds - icebot) * tcw)/ds
      ! mdm      tc=20.0          ! mdm test
      glak(i) = ( - 2.0 * tc/(dz + ds)) * (tlak(i,1) - t0(i))
    else
      glak(i) = ( - 2.0 * tcw/(dz + ds)) * (tlak(i,1) - t0(i))
    end if
    !-----net energy flux into skin (w/m2)
    eskin = g0(i) - glak(i)
    !
    ! step forward skin temp t0
    !
    t0old = t0(i)
    if (lkiceh(i) <= 0.0) then
      t0(i) = t0(i) + (delt/(ds * hcpw)) * eskin
    else if (lkiceh(i) > 0.0 .and. icebot <= ds) then
      t0(i) = t0(i) + (delt/((lkiceh(i) * hcpice) + &
              (ds - icebot) * hcpw)) * eskin
    else
      t0(i) = t0(i) + (delt/((ds + icetop) * hcpice)) * eskin
    end if
    !
    ! ice growth or decay
    !
    if (eskin < 0.0 .and. icebot < ds) then
      !-----net energy flux used to lower t0 to tfrez
      if (t0old > tfrez) then
        ecool = (ds - icebot) * hcpw * (t0old - tfrez)/delt
      else
        ecool = 0.0
      end if
      !-----remaining energy flux (if any) used to freeze ice
      eavail = eskin + ecool
      if (eavail < 0.0) then
        newice = - (delt/(rhoice * clhmlt)) * eavail
        lkiceh(i) = lkiceh(i) + newice
        icebot = rhoiw * lkiceh(i)
        t0(i) = tfrez
        !-----limit ice growth to the current layer
        if (icebot > ds) then
          eheat = (rhoice * clhmlt * (icebot - ds))/delt
          t0(i) = tfrez - (eheat * delt)/(ds * hcpice)
          lkiceh(i) = ds/rhoiw
        end if
      end if
    end if

    if (eskin > 0.0 .and. lkiceh(i) > 0.0) then
      !-----net energy flux used first to raise t to zero
      if (icebot <= ds) then
        eheat = lkiceh(i) * hcpice * (tfrez - t0old)/delt
      else
        eheat = (ds + icetop) * hcpice * (tfrez - t0old)/delt
      end if

      !-----net energy flux used to melt ice
      if (eskin > eheat) then
        newice = - (delt/(rhoice * clhmlt)) * (eskin - eheat)
        lkiceh(i) = lkiceh(i) + newice
        t0(i) = tfrez
        !-----limit ice melt to the current layer
        if (lkiceh(i) < 0.0) then
          eheat = - (rhoice * clhmlt * lkiceh(i))/delt
          t0(i) = tfrez + (eheat * delt)/(ds * hcpw)
          lkiceh(i) = 0.0
        end if
      end if
    end if
    ! -----------------
  end do ! loop 100

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
