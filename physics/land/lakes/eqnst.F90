!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine eqnst(expw,rho,tcel,h)
  !======================================================================
  !     * aug  2/16 - m.mackay.    equation of state moved to subroutine
  !
  implicit none
  !
  ! ----* input fields *------------------------------------------------
  !
  real, intent(inout) :: expw !< Variable description\f$[units]\f$
  real, intent(inout) :: rho !< Variable description\f$[units]\f$
  real, intent(in) :: tcel !< Variable description\f$[units]\f$
  real, intent(in) :: h !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: grav
  real :: c0
  real :: alpha
  real :: p
  real :: beta
  real :: rho0
  real :: t0
  real :: t
  real :: z
  real :: xi
  real :: kappa
  real :: s
  !
  ! salinity and pressure effects not included
  !
  z = 0.0
  s = 0.0
  ! mdm  s=0.3
  ! mdm  s=0.014
  ! mdm  s=0.03
  ! mdm  z=min(10.0,h)
  !-------------------------------------------------------------------------
  grav = 9.80616
  c0 = 4.9388e-5
  alpha = 3.3039e-7
  beta = 8.2545e-6
  xi = 8.0477e-1
  kappa = 0.2151
  ! mdm  t0=3.9816
  ! mdm  t0=3.9839
  t0 = 3.98275
  rho0 = 999.975 + xi * s
  p = rho0 * grav * z * 1.0e-5
  t = tcel - t0 - kappa * s
  !======================================================================
  ! farmer and carmack, 1981
  !
  rho = rho0 * (1. + p * (c0 - alpha * t) - beta * t * t)
  expw = (2. * rho0 * beta * t + rho0 * p * alpha)/rho

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
