subroutine classi(vpd,tadp,padry,rhoair,rhosni, &
                        rpcp,trpcp,spcp,tspcp, &
                        ta,qa,pcpr,rrate,srate,pressg, &
                        ipcp,nl,il1,il2)
  !
  !     * nov 17/11 - m.lazare.   remove calculation of pcpr
  !     *                         for ipcp=4 (redundant since
  !     *                         pcpr must be passed in for
  !     *                         if condition on line 100).
  !     * nov 22/06 - p.bartlett. calculate pcpr if ipcp=4.
  !     * jun 06/06 - v.fortin.   add option for passing in
  !     *                         rainfall and snowfall rates
  !     *                         calculated by atmospheric model.
  !     * nov 03/04 - d.verseghy. add "IMPLICIT NONE" command
  !     *                         move cloud fraction calculation
  !     *                         back into driver.
  !     * sep 04/03 - d.verseghy. new lower limit on precipitation
  !     *                         rate.
  !     * aug 09/02 - d.verseghy. move calculation of some
  !     *                         atmospheric variables here
  !     *                         prior to gathering.
  !     * jul 26/02 - r.brown/s.fassnacht/d.verseghy. provide
  !     *                         alternate methods of estimating
  !     *                         rainfall/snowfall partitioning.
  !     * jun 27/02 - d.verseghy. estimate fractional cloud cover
  !     *                         and rainfall/snowfall rates
  !     *                         if necessary.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ipcp !<
  integer, intent(in) :: nl !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: i !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: vpd   (nl) !<
  real, intent(inout) :: tadp  (nl) !<
  real, intent(inout) :: padry (nl) !<
  real, intent(inout) :: rhoair(nl) !<
  real, intent(inout) :: rhosni(nl) !<
  real, intent(inout) :: rpcp  (nl) !<
  real, intent(inout) :: trpcp (nl) !<
  real, intent(inout) :: spcp  (nl) !<
  real, intent(inout) :: tspcp (nl) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: ta    (nl) !<
  real, intent(in) :: qa    (nl) !<
  real, intent(in) :: pressg(nl)   !< Surface pressure \f$[Pa]\f$
  real, intent(in) :: pcpr  (nl) !<
  real, intent(in) :: rrate (nl) !<
  real, intent(in) :: srate (nl) !<
  !
  !     * work arrays.
  !
  real, dimension(nl) :: phase !<
  !
  !     * temporary variables.
  !
  real :: ea !<
  real :: ca !<
  real :: cb !<
  real :: easat !<
  real :: const !<
  !
  !     * common block parameters.
  !
  real :: delt !<
  real :: tfrez !<
  real :: rgas !<
  real :: rgasv !<
  real :: grav !<
  real :: sbc !<
  real :: vkc !<
  real :: ct !<
  real :: vmin !<
  real :: hcpw !<
  real :: hcpice !<
  real :: hcpsol !<
  real :: hcpom !<
  real :: hcpsnd !<
  real :: hcpcly !<
  real :: sphw !<
  real :: sphice !<
  real :: sphveg !<
  real :: sphair !<
  real :: rhow !<
  real :: rhoice !<
  real :: tcglac !<
  real :: clhmlt !<
  real :: clhvap !<

  common /class1/ delt,tfrez
  common /class2/ rgas,rgasv,grav,sbc,vkc,ct,vmin
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !----------------------------------------------------------------
  !
  !     * calculation of atmospheric input variables.
  !
  do i=il1,il2
    ea=qa(i)*pressg(i)/(0.622+0.378*qa(i))
    if (ta(i)>=tfrez) then
      ca=17.269
      cb=35.86
    else
      ca=21.874
      cb=7.66
    end if
    easat=611.0*exp(ca*(ta(i)-tfrez)/(ta(i)-cb))
    vpd(i)=max(0.0,(easat-ea)/100.0)
    padry(i)=pressg(i)-ea
    rhoair(i)=padry(i)/(rgas*ta(i))+ea/(rgasv*ta(i))
    const=log(ea/611.0)
    tadp(i)=(cb*const-ca*tfrez)/(const-ca)
    !
    !     * density of fresh snow.
    !
    if (ta(i)<=tfrez) then
      rhosni(i)=67.92+51.25*exp((ta(i)-tfrez)/2.59)
    else
      rhosni(i)=min((119.17+20.0*(ta(i)-tfrez)),200.0)
    end if
    !
    !     * precipitation partitioning between rain and snow.
    !
    rpcp (i)=0.0
    trpcp(i)=0.0
    spcp (i)=0.0
    tspcp(i)=0.0
    if (pcpr(i)>1.0e-8) then
      if (ipcp==1) then
        if (ta(i)>tfrez) then
          rpcp (i)=pcpr(i)/rhow
          trpcp(i)=max((ta(i)-tfrez),0.0)
        else
          spcp (i)=pcpr(i)/rhosni(i)
          tspcp(i)=min((ta(i)-tfrez),0.0)
        end if
      else if (ipcp==2) then
        if (ta(i)<=tfrez) then
          phase(i)=1.0
        else if (ta(i)>=(tfrez+2.0)) then
          phase(i)=0.0
        else
          phase(i)=1.0-0.5*(ta(i)-tfrez)
        end if
        rpcp(i)=(1.0-phase(i))*pcpr(i)/rhow
        if (rpcp(i)>0.0) trpcp(i)=max((ta(i)-tfrez),0.0)
        spcp(i)=phase(i)*pcpr(i)/rhosni(i)
        if (spcp(i)>0.0) tspcp(i)=min((ta(i)-tfrez),0.0)
      else if (ipcp==3) then
        if (ta(i)<=tfrez) then
          phase(i)=1.0
        else if (ta(i)>=(tfrez+6.0)) then
          phase(i)=0.0
        else
          phase(i)=(0.0202*(ta(i)-tfrez)**6-0.3660* &
                     (ta(i)-tfrez)**5+2.0399*(ta(i)-tfrez)**4- &
                     1.5089*(ta(i)-tfrez)**3-15.038* &
                     (ta(i)-tfrez)**2+4.6664*(ta(i)-tfrez)+100.0)/ &
                     100.0
          phase(i)=max(0.0,min(1.0,phase(i)))
        end if
        rpcp(i)=(1.0-phase(i))*pcpr(i)/rhow
        if (rpcp(i)>0.0) trpcp(i)=max((ta(i)-tfrez),0.0)
        spcp(i)=phase(i)*pcpr(i)/rhosni(i)
        if (spcp(i)>0.0) tspcp(i)=min((ta(i)-tfrez),0.0)
      else if (ipcp==4) then
        rpcp(i)=rrate(i)/rhow
        if (rpcp(i)>0.0) trpcp(i)=max((ta(i)-tfrez),0.0)
        spcp(i)=srate(i)/rhosni(i)
        if (spcp(i)>0.0) tspcp(i)=min((ta(i)-tfrez),0.0)
      end if
    end if
  end do ! loop 100
  !
  return
end
