subroutine canvap(evap,subl,raican,snocan,tcan,thliq,tbar,zsnow, &
                        wlost,chcap,qfcf,qfcl,qfn,qfc,htcc,htcs,htc, &
                        fi,cmass,tsnow,hcpsno,rhosno,froot,thpor, &
                        thlmin,delzw,evlost,rlost,iroot, &
                        ig,ilg,il1,il2,jl,n)
  !
  !     * sep 15/05 - d.verseghy. remove hard coding of ig=3.
  !     * sep 13/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jun 20/02 - d.verseghy. tidy up subroutine call; shortened
  !     *                         class4 common block.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         modifications to allow for variable soil
  !     *                         permeable depth.
  !     * dec 30/96 - d.verseghy. class - version 2.6.
  !     *                         bugfixes in calculation of qfn and
  !     *                         qfc.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * aug 24/95 - d.verseghy. class - version 2.4.
  !     *                         rationalize calculation of wlost
  !     *                         refine calculation of qfcl.
  !     * dec 22/94 - d.verseghy. class - version 2.3.
  !     *                         additional diagnostic calculations -
  !     *                         htcc and htc.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !                                        new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. calculate actual evaporation,
  !     *                         sublimation and transpiration from
  !     *                         vegetation canopy.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ig !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer :: i !<
  integer :: j !<
  integer, intent(in) :: n !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: thliq (ilg,ig) !<
  real, intent(in) :: tbar  (ilg,ig) !<
  real, intent(inout) :: qfc   (ilg,ig) !<
  real, intent(inout) :: htc   (ilg,ig) !<
  !
  real, intent(inout) :: evap  (ilg) !<
  real, intent(inout) :: subl  (ilg) !<
  real, intent(inout) :: raican(ilg) !<
  real, intent(inout) :: snocan(ilg) !<
  real, intent(in) :: tcan  (ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: wlost (ilg) !<
  real, intent(inout) :: chcap (ilg) !<
  real, intent(inout) :: qfcf  (ilg) !<
  real, intent(inout) :: qfcl  (ilg) !<
  real, intent(inout) :: qfn   (ilg) !<
  real, intent(inout) :: htcc  (ilg) !<
  real, intent(inout) :: htcs  (ilg) !<

  !
  !     * input arrays.
  !
  real, intent(in) :: froot (ilg,ig) !<
  real, intent(in) :: thpor(ilg,ig) !<
  real, intent(in) :: thlmin(ilg,ig) !<
  real, intent(in) :: delzw (ilg,ig) !<
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: cmass (ilg) !<
  real, intent(in) :: tsnow (ilg) !<
  real, intent(in) :: hcpsno(ilg) !<
  real, intent(in) :: rhosno(ilg) !<
  !
  !     * work arrays.
  !
  real, intent(inout) :: evlost(ilg) !<
  real, intent(inout) :: rlost (ilg) !<
  !
  integer, intent(inout), dimension(ilg) :: iroot !<
  !
  !     * temporary variables.
  !
  real :: slost !<
  real :: thtran !<
  real :: thllim !<
  !
  !     * common block parameters.
  !
  real :: delt !<
  real :: tfrez !<
  real :: hcpw !<
  real :: hcpice !<
  real :: hcpsol !<
  real :: hcpom !<
  real :: hcpsnd !<
  real :: hcpcly !<
  real :: sphw !<
  real :: sphice !<
  real :: sphveg !<
  real :: sphair !<
  real :: rhow !<
  real :: rhoice !<
  real :: tcglac !<
  real :: clhmlt !<
  real :: clhvap !<
  !
  common /class1/ delt,tfrez
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !-----------------------------------------------------------------------
  !     * initialize arrays.
  !     * (the work array "IROOT" indicates points where transpiration
  !     * can occur.)
  !
  do i=il1,il2
    if (fi(i)>0.) then
      rlost (i)=0.0
      evlost(i)=0.0
      iroot (i)=0
      htcc  (i)=htcc(i)-fi(i)*tcan(i)*chcap(i)/delt
      htcs(i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                 zsnow(i)/delt
    end if
  end do ! loop 50
  !
  do j=1,ig
    do i=il1,il2
      if (fi(i)>0.) then
        htc (i,j)=htc(i,j)-fi(i)*(tbar(i,j)+tfrez)*thliq(i,j)* &
             hcpw*delzw(i,j)/delt
        if (froot(i,j)>1.0e-5) iroot(i)=1
      end if
    end do
  end do ! loop 100
  !
  !     * sublimation case.  if snow on canopy is insufficient to supply
  !     * demand, residual is taken first from snow underlying canopy and
  !     * then from liquid water on canopy.
  !
  do i=il1,il2
    if (fi(i)>0. .and. subl(i)>0.) then
      slost=subl(i)*delt*rhow
      if (slost<=snocan(i)) then
        snocan(i)=snocan(i)-slost
        subl(i)=0.0
      else
        slost=slost-snocan(i)
        qfcf(i)=qfcf(i)-fi(i)*slost/delt
        snocan(i)=0.0
        if (slost<=zsnow(i)*rhosno(i)) then
          zsnow(i)=zsnow(i)-slost/rhosno(i)
          subl(i)=0.0
          qfn(i)=qfn(i)+fi(i)*slost/delt
        else
          slost=slost-zsnow(i)*rhosno(i)
          qfn(i)=qfn(i)+fi(i)*zsnow(i)*rhosno(i)/delt
          zsnow(i)=0.0
          wlost(i)=wlost(i)-slost*clhmlt/clhvap
          evap(i)=evap(i)+slost*(clhmlt+clhvap)/ &
                         (clhvap*delt*rhow)
          qfcl(i)=qfcl(i)+fi(i)*slost*(clhmlt+clhvap)/ &
                         (clhvap*delt)
        end if
      end if
    end if
  end do ! loop 200
  !
  !     * evaporation.  if water on canopy is insufficient to supply
  !     * demand, assign residual to transpiration.
  !
  do i=il1,il2
    if (fi(i)>0. .and. evap(i)>0.) then
      rlost(i)=evap(i)*rhow*delt
      if (rlost(i)<=raican(i)) then
        raican(i)=raican(i)-rlost(i)
        evap  (i)=0.
        rlost (i)=0.
      else
        rlost(i)=rlost(i)-raican(i)
        qfcl(i)=qfcl(i)-fi(i)*rlost(i)/delt
        if (iroot(i)==0) evlost(i)=rlost(i)
        evap  (i)=0.
        raican(i)=0.
      end if
    end if
  end do ! loop 300
  !
  !     * transpiration.
  !
  do j=1,ig
    do i=il1,il2
      if (fi(i)>0. .and. iroot(i)>0) then
        if (delzw(i,j)>0.0) then
          thtran=rlost(i)*froot(i,j)/(rhow*delzw(i,j))
        else
          thtran=0.0
        end if
        if (thpor(i,j)<thlmin(i,j)) then
          thllim=thpor(i,j)
        else
          thllim=thlmin(i,j)
        end if
        if (thtran<=(thliq(i,j)-thllim)) then
          qfc  (i,j)=qfc(i,j)+fi(i)*rlost(i)*froot(i,j)/delt
          thliq(i,j)=thliq(i,j)-thtran
        else
          qfc  (i,j)=qfc(i,j)+fi(i)*(thliq(i,j)-thllim)*rhow* &
                        delzw(i,j)/delt
          evlost (i)=evlost(i)+(thtran+thllim-thliq(i,j))*rhow* &
                        delzw(i,j)
          thliq(i,j)=thllim
        end if
      end if
    end do
  end do ! loop 400
  !
  !     * cleanup.
  !
  do i=il1,il2
    if (fi(i)>0.) then
      chcap(i)=raican(i)*sphw+snocan(i)*sphice+cmass(i)*sphveg
      wlost(i)=wlost(i)+evlost(i)
      htcc  (i)=htcc(i)+fi(i)*tcan(i)*chcap(i)/delt
      htcs(i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                 zsnow(i)/delt
    end if
  end do ! loop 500
  !
  do j=1,ig
    do i=il1,il2
      if (fi(i)>0.) then
        htc (i,j)=htc(i,j)+fi(i)*(tbar(i,j)+tfrez)*thliq(i,j)* &
             hcpw*delzw(i,j)/delt
      end if
    end do
  end do ! loop 550
  !
  return
end
