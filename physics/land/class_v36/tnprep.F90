subroutine tnprep(a1,a2,b1,b2,c2,gdenom,gcoeff, &
                        gconst,cphchg,iwater, &
                        tbar,tctop,tcbot, &
                        fi,zpond,tbar1p,delz,tcsnow,zsnow, &
                        isand,ilg,il1,il2,jl,ig)
  !
  !     * mar 03/08 - d.verseghy. assign tctop3 and tcbot3 on the basis
  !     *                         of subarea values from tprep; replace
  !     *                         three-level temperature and thermal
  !     *                         conductivity vectors with standard
  !     *                         values.
  !     * aug 16/06 - d.verseghy. remove tstart.
  !     * may 24/05 - d.verseghy. limit delz3 to <= 4.1 m.
  !     * oct 04/05 - d.verseghy. use three-layer tbar,tctop,tcbot.
  !     * nov 04/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * aug 06/02 - d.verseghy. shortened class3 common block,
  !     * jun 17/02 - d.verseghy. use new lumped soil and ponded
  !     *                         water temperature for first layer
  !     *                         shortened common block.
  !     * mar 28/02 - d.verseghy. change pond threshold value for
  !     *                         calculation of "IWATER".
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         incorporate explicitly calculated
  !     *                         thermal conductivities at tops and
  !     *                         bottoms of soil layers.
  !     * sep 27/96 - d.verseghy. class - version 2.5.
  !     *                         surface treated as water only if
  !     *                         zpond > 1 mm.
  !     * aug 18/95 - d.verseghy. class - version 2.4.
  !     *                         revisions to allow for inhomogeneity
  !     *                         between soil layers and fractional
  !     *                         organic matter content.
  !     * nov 28/94 - m. lazare.  class - version 2.3.
  !     *                         tcsatw,tcsati declared real(16).
  !     * apr 10/92 - m. lazare.  class - version 2.1.
  !     *                         divide previous subroutine "T3LAYR"
  !     *                         into "TNPREP" and "TNPOST" and
  !     *                         vectorize.
  !     * apr 11/89 - d.verseghy. calculate coefficients for ground heat
  !     *                         flux, expressed as a linear function
  !     *                         of surface temperature.  coefficients
  !     *                         are calculated from layer temperatures,
  !     *                         thicknesses and thermal conductivities,
  !     *                         assuming a quadratic variation of
  !     *                         temperature with depth within each
  !     *                         soil layer. set the surface latent
  !     *                         heat of vaporization of water and
  !     *                         the starting temperature for the
  !     *                         iteration in "TSOLVC"/"TSOLVE".
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: ig !<
  integer :: i !<
  integer :: j !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: a1    (ilg) !<
  real, intent(inout) :: a2    (ilg) !<
  real, intent(inout) :: b1    (ilg) !<
  real, intent(inout) :: b2    (ilg) !<
  real, intent(inout) :: c2    (ilg) !<
  real, intent(inout) :: gdenom(ilg) !<
  real, intent(inout) :: gcoeff(ilg) !<
  real, intent(inout) :: gconst(ilg) !<
  real, intent(inout) :: cphchg(ilg) !<
  !
  integer, intent(inout), dimension(ilg) :: iwater !<
  !
  !     * input arrays.
  !
  real, intent(in) :: tbar  (ilg,ig) !<
  real, intent(in) :: tctop (ilg,ig) !<
  real, intent(in) :: tcbot (ilg,ig) !<
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: zpond (ilg) !<
  real, intent(in) :: tbar1p(ilg) !<
  real, intent(in) :: tcsnow(ilg) !<
  real, intent(in) :: zsnow (ilg) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  real, intent(in), dimension(ig) :: delz !<
  !
  !     * temporary variables.
  !
  real :: delz1 !<
  real :: a3 !<
  real :: b3 !<
  real :: c3 !<
  real :: tczero !<
  !
  !     * common block parameters.
  !
  real :: tcw !<
  real :: tcice !<
  real :: tcsand !<
  real :: tcclay !<
  real :: tcom !<
  real :: tcdrys !<
  real :: rhosol !<
  real :: rhoom !<
  real :: hcpw !<
  real :: hcpice !<
  real :: hcpsol !<
  real :: hcpom !<
  real :: hcpsnd !<
  real :: hcpcly !<
  real :: sphw !<
  real :: sphice !<
  real :: sphveg !<
  real :: sphair !<
  real :: rhow !<
  real :: rhoice !<
  real :: tcglac !<
  real :: clhmlt !<
  real :: clhvap !<
  !
  common /class3/ tcw,tcice,tcsand,tcclay,tcom,tcdrys, &
                 rhosol,rhoom
  common /class4/ hcpw,hcpice,hcpsol,hcpom,hcpsnd,hcpcly, &
                 sphw,sphice,sphveg,sphair,rhow,rhoice, &
                 tcglac,clhmlt,clhvap
  !-----------------------------------------------------------------------
  !     * initialization of arrays.
  !
  do i=il1,il2
    if (fi(i)>0.) then
      delz1=delz(1)+zpond(i)
      if (zpond(i)>0.5e-3) then
        iwater(i)=1
      else
        if (isand(i,1)>-4) then
          iwater(i)=0
        else
          iwater(i)=2
        end if
      end if
      !
      if (iwater(i)==2) then
        cphchg(i)=clhvap+clhmlt
      else
        cphchg(i)=clhvap
      end if
      !
      if (zsnow(i)>0.0) then
        tczero=1.0/(0.5/tcsnow(i)+0.5/tctop(i,1))
      else
        tczero=tctop(i,1)
      end if
      a1(i)=delz1/(3.0*tczero)
      a2(i)=delz1/(2.0*tczero)
      a3=a2(i)
      b1(i)=delz1/(6.0*tcbot(i,1))
      b2(i)=delz1/(2.0*tcbot(i,1))+delz(2)/(3.0*tctop(i,2))
      b3=delz1/(2.0*tcbot(i,1))+delz(2)/(2.0*tctop(i,2))
      c2(i)=delz(2)/(6.0*tcbot(i,2))
      c3=delz(2)/(2.0*tcbot(i,2))+delz(3)/(3.0*tctop(i,3))
      gdenom(i)=a1(i)*(b2(i)*c3-b3*c2(i))-b1(i)*(a2(i)*c3- &
                   a3*c2(i))
      gcoeff(i)=(b2(i)*c3-b3*c2(i)-b1(i)*(c3-c2(i)))/gdenom(i)
      gconst(i)=(-tbar1p(i)*(b2(i)*c3-b3*c2(i))+ &
                     tbar(i,2)*b1(i)*c3- &
                     tbar(i,3)*b1(i)*c2(i))/gdenom(i)
    end if
  end do ! loop 100
  !
  return
end
