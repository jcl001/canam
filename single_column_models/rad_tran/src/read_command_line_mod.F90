!>\file
!>\brief Routines to read in the command line arguments
!!
!! @author Jason Cole
!

module read_command_line_mod
  implicit none
  
  public  :: read_command_line

contains

  subroutine read_command_line(runinfo_filename)
  
    use parameters_mod, only : maxlen_char, &
                               num_args,    &
                               exit_code_read_command
    
   
    implicit none
    
    ! output 
    character(len=maxlen_char), intent(out) :: runinfo_filename !< Name of file with the information about the run/calculation
    
    ! local
    integer :: n_args
    
    ! figure out the number of command line argument(s)
    
    n_args = command_argument_count()
    
    ! read the command line argument
    if (n_args /= num_args) then
      write(*,*) 'Expected 1 argument on command line.  exiting.'
      stop exit_code_read_command
    else 
      call get_command_argument(1,runinfo_filename)
    end if
    
    return
    
  end subroutine read_command_line
  
end module read_command_line_mod
    
  
