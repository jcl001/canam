!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine vfft3(a, work,trigs,ifax,inc,jump,n,lot,isign)
  !
  !     * aug 01/03 - m.lazare. - generalized using "MAXLOT" passed in
  !     *                         common block, instead of hard-coded
  !     *                         value of 256 only valid for the nec.
  !     *                         this is consistent with what is done in
  !     *                         the routines called by this subroutine, as
  !     *                         well as the subroutines which call this
  !     *                         routine.
  !     *                       - calls new qpassf3/rpassf3 instead of
  !     *                         qpassf2/rpassf2, for consistent use of
  !     *                         "MAXLOT".
  !     *                       - eliminate computed "GOTO" at end.
  !     * nov 01/92 - j.stacey. previous version vfft2.
  !
  !     * multiple fast real :: periodic transform.
  !     * real :: transform of length n performed by removing redundant
  !     * operations from complex :: transform of length n
  !     *
  !     * a is the array containing input & output data
  !     * work is an area of size (n+1)*min(lot,maxlot)
  !     * trigs is a previously prepared list of trig function values
  !     * ifax is a previously prepared list of factors of n
  !     * INC IS THE INCREMENT WITHIN EACH DATA 'VECTOR'
  !     *     (e.g. inc=1 for consecutively stored data)
  !     * jump is the increment between the start of each data vector
  !     * n is the length of the data vectors
  !     * lot is the number of data vectors
  !     * isign = +1 for transform from spectral to gridpoint
  !     *       = -1 for transform from gridpoint to spectral
  !     *
  !     * ordering of coefficients:
  !     *     a(0),b(0),a(1),b(1),a(2),b(2),...,a(n/2),b(n/2)
  !     *     where b(0)=b(n/2)=0; (n+2) locations required
  !     *
  !     * ordering of data:
  !     *     x(0),x(1),x(2),...,x(n-1), 0 , 0 ; (n+2) locations required
  !     *
  !     * vectorization is achieved on cray by doing the teansforms
  !     * in parallel
  !     *
  !     * n must be composed of factors 2,3 & 5 but does not have to be ev
  !     *
  !     * definition of transforms:
  !     * -------------------------
  !     *
  !     * isign=+1: x(j)=sum(k=0,...,n-1)(c(k)*exp(2*i*j*k*pi/n))
  !     *     where c(k)=a(k)+i*b(k) and c(n-k)=a(k)-i*b(k)
  !     *
  !     * isign=-1: a(k)=(1/n)*sum(j=0,...,n-1)(x(j)*cos(2*j*k*pi/n))
  !     *           b(k)=-(1/n)*sum(j=0,...,n-1)(x(j)*sin(2*j*k*pi/n))
  !
  implicit none
  integer :: i
  integer :: ia
  integer :: ibase
  integer :: ierr
  integer :: ifac
  integer :: igo
  integer :: ii
  integer, intent(in) :: inc
  integer, intent(in) :: isign
  integer :: istart
  integer :: ix
  integer :: iz
  integer :: j
  integer :: jbase
  integer :: jj
  integer, intent(in) :: jump
  integer :: k
  integer :: la
  integer, intent(in) :: lot
  integer :: maxlot
  integer, intent(in) :: n
  integer :: nb
  integer :: nblox
  integer :: nfax
  integer :: nvex
  integer :: nx

  real, intent(inout), dimension(n) :: a !< Variable description\f$[units]\f$
  real, intent(in), dimension(n) :: work !< Variable description\f$[units]\f$
  real, intent(in), dimension(n) :: trigs !< Variable description\f$[units]\f$
  integer, intent(in) :: ifax(*) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  common /itrans/ maxlot
  !===================================================================
  nfax=ifax(1)
  nx=n+1
  if (mod(n,2)==1) nx=n
  nblox = 1 + (lot - 1)/maxlot
  nvex  = lot - (nblox - 1)*maxlot
  if (isign/=-1) then
    !--------------------------------------------------------------------
    !     * isign=+1, spectral to gridpoint transform
    !       -----------------------------------------
    istart=1
    do nb=1,nblox
      ia=istart
      i=istart
      do j=1,nvex
        a(i+inc)=0.5*a(i)
        i=i+jump
      end do ! loop 110
      if (mod(n,2)/=1) then
        i=istart+n*inc
        do j=1,nvex
          a(i)=0.5*a(i)
          i=i+jump
        end do ! loop 120
      end if
      ia=istart+inc
      la=1
      igo=+1
      !
      do k=1,nfax
        ifac=ifax(k+1)
        ierr=-1
        if (igo==-1) then
          call rpassf3(work(1),work(la+1),a(ia),a(ia+ifac*la*inc),trigs, &
                      1,inc,nx,jump,nvex,n,ifac,la,ierr)
        else
          call rpassf3(a(ia),a(ia+la*inc),work(1),work(ifac*la+1),trigs, &
                      inc,1,jump,nx,nvex,n,ifac,la,ierr)
        end if
        if (ierr/=0) go to 500
        la=ifac*la
        igo=-igo
        ia=istart
      end do ! loop 160
      !     if necessary, copy results back to a
      !     ------------------------------------
      if (mod(nfax,2)/=0) then
        ibase=1
        jbase=ia
        do jj=1,nvex
          i=ibase
          j=jbase
          do ii=1,n
            a(j)=work(i)
            i=i+1
            j=j+inc
          end do ! loop 170
          ibase=ibase+nx
          jbase=jbase+jump
        end do ! loop 180
      end if
      !
      !     * fill in zeros at end
      !       --------------------
      ix=istart+n*inc
      do j=1,nvex
        a(ix)=0.0
        a(ix+inc)=0.0
        ix=ix+jump
      end do ! loop 210
      !
      istart=istart+nvex*jump
      nvex=maxlot
    end do ! loop 220
    return
    !----------------------------------------------------------------------
    !     * isign=-1, gridpoint to spectral transform
    !       -----------------------------------------
  end if
  istart=1
  do nb=1,nblox
    ia=istart
    la=n
    igo=+1
    !
    do k=1,nfax
      ifac=ifax(nfax+2-k)
      la=la/ifac
      ierr=-1
      if (igo==-1) then
        call qpassf3(work(1),work(ifac*la+1),a(ia),a(ia+la*inc),trigs, &
                     1,inc,nx,jump,nvex,n,ifac,la,ierr)
      else
        call qpassf3(a(ia),a(ia+ifac*la*inc),work(1),work(la+1),trigs, &
                     inc,1,jump,nx,nvex,n,ifac,la,ierr)
      end if
      if (ierr/=0) go to 500
      igo=-igo
      ia=istart+inc
    end do ! loop 340
    !
    !     if necessary, copy results back to a
    !     ------------------------------------
    if (mod(nfax,2)/=0) then
      ibase=1
      jbase=ia
      do jj=1,nvex
        i=ibase
        j=jbase
        do ii=1,n
          a(j)=work(i)
          i=i+1
          j=j+inc
        end do ! loop 350
        ibase=ibase+nx
        jbase=jbase+jump
      end do ! loop 360
    end if
    !
    !     shift a(0) & fill in zero imag parts
    !     ------------------------------------
    ix=istart
    do j=1,nvex
      a(ix)=a(ix+inc)
      a(ix+inc)=0.0
      ix=ix+jump
    end do ! loop 380
    if (mod(n,2)/=1) then
      iz=istart+(n+1)*inc
      do j=1,nvex
        a(iz)=0.0
        iz=iz+jump
      end do ! loop 390
    end if
    !
    istart=istart+nvex*jump
    nvex=maxlot
  end do ! loop 410
  return
  !-------------------------------------------------------------------
  !     * error messages
  !       --------------
500 continue
  if (ierr==1) then
    write(6,520) nvex
    520   format('0VECTOR LENGTH =',i4,', GREATER THAN MAXLOT')
  else if (ierr==2) then
    write(6,540) ifac
    540   format('0FACTOR =',i3,', NOT CATERED FOR')
  else if (ierr==3) then
    write(6,560) ifac
    560   format('0FACTOR =',i3,', ONLY CATERED FOR IF LA*IFAC=N')
  end if
  call xit('VFFT3',-1)

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
