#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!
!     * mar 16/2019 - j. cole     - add  3-hour 2d fields (cf3hr,e3hr)
!     * nov 29/2018 - j. cole     - add/adjust tendencies
!     * nov 3/2018  - j. cole       add radiative flux profiles
!     *                             remove asnf,csnf,alnf,clnf
!     * nov 02/2018 - m.lazare.   - added {gsnopak,fnlapak).
!     *                           - added 3hr save fields.
!     * oct 12/2018 - m.lazare.     added {tbndpak,tbndpal}.
!     * aug 01/2018 - m.lazare.     remove {obeg,obwg,res}.
!     * jul 30/2018 - m.lazare.   - unused fmirow removed.
!     * mar 09/2018 - m.lazare.   - beg  and bwg  changed from pak/row to
!     *                             pal/rol.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     *                           - moved accumulated fields from init12 to zeroacc4.
!     * aug 10/2017 - m.lazare.    {phlpak,phspak,phdpak) are not
!     *                            accumulated for now, so zeroing
!     *                            removed.
!     *                            zero out cslm lake accumulated fields.
!     * aug 07/2017 - m.lazare.    initial git verison.
!     * feb 12/2015 - m.lazare/    new version for gcm18:
!     *               k.vonsalzen. - remove initialization of sfad and
!     *                              some other pla fields (not accumulated).
!     *                            - {ga,gflx,pet,ue} initialization
!     *                              to zero added.
!     *                            - all accumulated fields not under
!     *                              control of cpp directives (and not
!     *                              passed to/from coupler) now initialized here.
!     * jul 10/2013 - m.lazare/    previous version zeroacc3 for gcm17:
!     *               k.vonsalzen. - extra diagnostic microphysics
!     *                              fields zeroed out:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - many new aerosol diagnostic fields
!     *                              are zeroed out for pam,
!     *                              including those for cpp options:
!     *                              "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option
!     *                              since they are now instantaneious:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     * may 08, 2012 - m.lazare. previous version zeroacc2 for gcm16:
!     *                          - add {olr,olrc,fsr,fsrc,flam,fsam,fsan,flan}
!     *                            and "XTRADUST"/"AODPTH" diagnostic
!     *                            fields.
!     * apr 30, 2010 - m.lazare. previous version zeroacc for gcm15i.
!
!     * zero out acccumulated fields (no longer read/written in restart).
!     * this is called in section 0 of the model.
!=======================================================================
!     * general diagnostic fields not under control of cpp directives
!     * and not used in coupler.
!
call pkzeros2(pchfpak,ijpak,   1)
call pkzeros2(plhfpak,ijpak,   1)
call pkzeros2(pshfpak,ijpak,   1)
call pkzeros2(drpak,ijpak,   1)
call pkzeros2(fsopak,ijpak,   1)
call pkzeros2(fsgpak,ijpak,   1)
call pkzeros2(fsdpak,ijpak,   1)
call pkzeros2(fsvpak,ijpak,   1)
call pkzeros2(fsspak,ijpak,   1)
call pkzeros2(fsdcpak,ijpak,   1)
call pkzeros2(fsscpak,ijpak,   1)
call pkzeros2(fdlcpak,ijpak,   1)
call pkzeros2(olrpak,ijpak,   1)
call pkzeros2(olrcpak,ijpak,   1)
call pkzeros2(flampak,ijpak,   1)
call pkzeros2(flanpak,ijpak,   1)
call pkzeros2(fdlpak,ijpak,   1)
call pkzeros2(flgpak,ijpak,   1)
call pkzeros2(fslopak,ijpak,   1)
call pkzeros2(cldtpak,ijpak,   1)
call pkzeros2(cldopak,ijpak,   1)
call pkzeros2(cldapak,ijpak,   1)
call pkzeros2(fsrpak,ijpak,   1)
call pkzeros2(fsrcpak,ijpak,   1)
call pkzeros2(fsampak,ijpak,   1)
call pkzeros2(fsanpak,ijpak,   1)
call pkzeros2(fsgcpak,ijpak,   1)
call pkzeros2(flgcpak,ijpak,   1)
call pkzeros2(pwatpak,ijpak,   1)
call pkzeros2(smltpak,ijpak,   1)
call pkzeros2(fnpak,ijpak,   1)
call pkzeros2(clwtpak,ijpak,   1)
call pkzeros2(cictpak,ijpak,   1)
call pkzeros2(hflpak,ijpak,   1)
call pkzeros2(hfspak,ijpak,   1)
call pkzeros2(qfspak,ijpak,   1)
call pkzeros2(pcppak,ijpak,   1)
call pkzeros2(pcpcpak,ijpak,   1)
call pkzeros2(pcpnpak,ijpak,   1)
call pkzeros2(pcpspak,ijpak,   1)
call pkzeros2(pivfpak,ijpak,   1)
call pkzeros2(pivlpak,ijpak,   1)
call pkzeros2(pinpak,ijpak,   1)
call pkzeros2(pigpak,ijpak,   1)
call pkzeros2(ufspak,ijpak,   1)
call pkzeros2(vfspak,ijpak,   1)
call pkzeros2(begpal,ijpak,   1)
call pkzeros2(bwgpal,ijpak,   1)
call pkzeros2(rainspal,ijpak,  1)
gtpax = 0.
call pkzeros2(snowspal,ijpak,  1)
call pkzeros2(slimpal,ijpak,  1)
call pkzeros2(slimplw,ijpak,  1)
call pkzeros2(slimpsh,ijpak,  1)
call pkzeros2(slimplh,ijpak,  1)
call pkzeros2(srhnpak,ijpak,   1)
call pkzeros2(srhxpak,ijpak,   1)
call pkzeros2(swapal,ijpak,   1)
call pkzeros2(swxpak,ijpak,   1)
call pkzeros2(swxupak,ijpak,   1)
call pkzeros2(swxvpak,ijpak,   1)
call pkzeros2(tfxpak,ijpak,   1)
call pkzeros2(qfxpak,ijpak,   1)
call pkzeros2(qfslpal,ijpak,   1)
call pkzeros2(qfsopal,ijpak,   1)
call pkzeros2(rofpal, ijpak,   1)
call pkzeros2(rofopal,ijpak,   1)
call pkzeros2(ufspal,ijpak,   1)
call pkzeros2(vfspal,ijpak,   1)
call pkzeros2(ufsipal,ijpak,   1)
call pkzeros2(vfsipal,ijpak,   1)
call pkzeros2(ufsopal,ijpak,   1)
call pkzeros2(vfsopal,ijpak,   1)
call pkzeros2(begipal,ijpak,   1)
call pkzeros2(begkpal,ijpak,   1)
call pkzeros2(beglpal,ijpak,   1)
call pkzeros2(begopal,ijpak,   1)
call pkzeros2(bwgipal,ijpak,   1)
call pkzeros2(bwgkpal,ijpak,   1)
call pkzeros2(bwglpal,ijpak,   1)
call pkzeros2(bwgopal,ijpak,   1)
call pkzeros2(ofsgpal,ijpak,   1)
call pkzeros2(fsgipal,ijpak,   1)
call pkzeros2(fsgopal,ijpak,   1)
call pkzeros2(pmslpal,ijpak,   1)
call pkzeros2(hseapal,ijpak,   1)
call pkzeros2(stpak,ijpak,   1)
call pkzeros2(sqpak,ijpak,   1)
call pkzeros2(supak,ijpak,   1)
call pkzeros2(svpak,ijpak,   1)
call pkzeros2(srhpak,ijpak,   1)
call pkzeros2(swapak,ijpak,   1)
call pkzeros2(fsgvpak,ijpak,   1)
call pkzeros2(fsggpak,ijpak,   1)
call pkzeros2(flgvpak,ijpak,   1)
call pkzeros2(flggpak,ijpak,   1)
call pkzeros2(hfsvpak,ijpak,   1)
call pkzeros2(hfsgpak,ijpak,   1)
call pkzeros2(hfsnpak,ijpak,   1)
call pkzeros2(hmfvpak,ijpak,   1)
call pkzeros2(hmfnpak,ijpak,   1)
call pkzeros2(hfcgpak,ijpak,ignd)
call pkzeros2(hfcvpak,ijpak,   1)
call pkzeros2(hfcnpak,ijpak,   1)
call pkzeros2(qfgpak,ijpak,   1)
call pkzeros2(qfnpak,ijpak,   1)
call pkzeros2(qfvgpak,ijpak,ignd)
call pkzeros2(qfvlpak,ijpak,   1)
call pkzeros2(qfvfpak,ijpak,   1)
call pkzeros2(fsgnpak,ijpak,   1)
call pkzeros2(flgnpak,ijpak,   1)
call pkzeros2(rofpak,ijpak,   1)
call pkzeros2(rofvpak,ijpak,   1)
call pkzeros2(rofnpak,ijpak,   1)
call pkzeros2(rofopak,ijpak,   1)
call pkzeros2(rovgpak,ijpak,   1)
call pkzeros2(wtrvpak,ijpak,   1)
call pkzeros2(wtrnpak,ijpak,   1)
call pkzeros2(wtrgpak,ijpak,   1)
call pkzeros2(hflvpak,ijpak,   1)
call pkzeros2(hflnpak,ijpak,   1)
call pkzeros2(hflgpak,ijpak,   1)
call pkzeros2(znpak,ijpak,   1)
call pkzeros2(psapak,ijpak,   1)
call pkzeros2(fvnpak,ijpak,   1)
call pkzeros2(fvgpak,ijpak,   1)
call pkzeros2(skygpak,ijpak,   1)
call pkzeros2(skynpak,ijpak,   1)
call pkzeros2(rofbpak,ijpak,   1)
call pkzeros2(rofspak,ijpak,   1)
call pkzeros2(hmfgpak,ijpak,ignd)
call pkzeros2(altipak,ijpak,ilev)
call pkzeros2(tbndpak,ijpak,ilev)
call pkzeros2(tbndpal,ijpak,ilev)
call pkzeros2(ea55pak,ijpak,ilev)
call pkzeros2(ea55pal,ijpak,ilev)
call pkzeros2(gsnopak,ijpak,ilev)
call pkzeros2(fnlapak,ijpak,ilev)
call pkzeros2(rkmpak,ijpak,ilev)
call pkzeros2(rkhpak,ijpak,ilev)
call pkzeros2(rkqpak,ijpak,ilev)
!
call pkzeros2(cldt3hpak,ijpak,   1)
call pkzeros2(hfl3hpak,ijpak,   1)
call pkzeros2(hfs3hpak,ijpak,   1)
call pkzeros2(rof3hpal,ijpak,   1)
call pkzeros2(pcp3hpak,ijpak,   1)
call pkzeros2(pcpc3hpak,ijpak,   1)
call pkzeros2(pcpl3hpak,ijpak,   1)
call pkzeros2(pcps3hpak,ijpak,   1)
call pkzeros2(pcpn3hpak,ijpak,   1)
call pkzeros2(fdl3hpak,ijpak,   1)
call pkzeros2(fdlc3hpak,ijpak,   1)
call pkzeros2(flg3hpak,ijpak,   1)
call pkzeros2(fss3hpak,ijpak,   1)
call pkzeros2(fssc3hpak,ijpak,   1)
call pkzeros2(fsd3hpak,ijpak,   1)
call pkzeros2(fsg3hpak,ijpak,   1)
call pkzeros2(fsgc3hpak,ijpak,   1)

call pkzeros2(olr3hpak,ijpak,    1)
call pkzeros2(fsr3hpak,ijpak,    1)
call pkzeros2(olrc3hpak,ijpak,   1)
call pkzeros2(fsrc3hpak,ijpak,   1)
call pkzeros2(fso3hpak,ijpak,    1)
call pkzeros2(pwat3hpak,ijpak,   1)
call pkzeros2(clwt3hpak,ijpak,   1)
call pkzeros2(cict3hpak,ijpak,   1)
call pkzeros2(pmsl3hpak,ijpak,   1)
call pkzeros2(st3hpak,ijpak,     1)
call pkzeros2(su3hpak,ijpak,     1)
call pkzeros2(sv3hpak,ijpak,     1)

!
#if defined (cslm)
      flglpak = 0.
      fnlpak = 0.
      fsglpak = 0.
      hfclpak = 0.
      hfllpak = 0.
      hfslpak = 0.
      hmflpak = 0.
      pilpak = 0.
      qflpak = 0.
#endif
!
do n = 1,ntrac
  call pkzeros2(xfspak(1,n)  ,ijpak,   1)
  call pkzeros2(xsrfpak(1,n)  ,ijpak,   1)
  call pkzeros2(xtvipak(1,n)  ,ijpak,   1)
end do
!
#if defined (explvol)
      if (ivtau == 2) then
         call pkzeros2(w055_vtau_sa_pak, ijpak,   1)
         call pkzeros2(w110_vtau_sa_pak, ijpak,   1)
         call pkzeros2(w055_ext_gcm_sa_pak, ijpak, ilev)
         call pkzeros2(w110_ext_gcm_sa_pak, ijpak, ilev)
      end if
#endif
#if defined (tprhs)
      call pkzeros2(ttppak,ijpak,ilev)
#endif
#if defined (qprhs)
      call pkzeros2(qtppak,ijpak,ilev)
#endif
#if defined (qconsav)
      call pkzeros2(qaddpak,ijpak,   2)
      call pkzeros2(qtphpak,ijpak,   2)
      call pkzeros2(qtpfpak,ijpak,ilev)
#endif
#if defined (uprhs)
      call pkzeros2(utppak,ijpak,ilev)
#endif
#if defined (vprhs)
      call pkzeros2(vtppak,ijpak,ilev)
#endif
#if defined (tprhsc)
      call pkzeros2(ttpvpak,ijpak,ilev)
      call pkzeros2(ttpmpak,ijpak,ilev)
      call pkzeros2(ttpcpak,ijpak,ilev)
      call pkzeros2(ttpppak,ijpak,ilev)
      call pkzeros2(ttpkpak,ijpak,ilev)
      call pkzeros2(ttpnpak,ijpak,ilev)
#endif
#if (defined (tprhsc) || defined (radforce))
      call pkzeros2(ttpspak,ijpak,ilev)
      call pkzeros2(ttplpak,ijpak,ilev)
      call pkzeros2(ttscpak,ijpak,ilev)
      call pkzeros2(ttlcpak,ijpak,ilev)
#endif
#if defined (qprhsc)
      call pkzeros2(qtpvpak,ijpak,ilev)
      call pkzeros2(qtpmpak,ijpak,ilev)
      call pkzeros2(qtpcpak,ijpak,ilev)
      call pkzeros2(qtpppak,ijpak,ilev)
#endif
#if defined (uprhsc)
      call pkzeros2(utpvpak,ijpak,ilev)
      call pkzeros2(utpgpak,ijpak,ilev)
      call pkzeros2(utpcpak,ijpak,ilev)
      call pkzeros2(utpspak,ijpak,ilev)
      call pkzeros2(utpnpak,ijpak,ilev)
#endif
#if defined (vprhsc)
      call pkzeros2(vtpvpak,ijpak,ilev)
      call pkzeros2(vtpgpak,ijpak,ilev)
      call pkzeros2(vtpcpak,ijpak,ilev)
      call pkzeros2(vtpspak,ijpak,ilev)
      call pkzeros2(vtpnpak,ijpak,ilev)
#endif

do n = 1,ntrac
#if defined (xprhs)
         call pkzeros2(xtppak(1,1,n),ijpak,ilev)
#endif
#if defined (xconsav)
         call pkzeros2(xaddpak(1,1,n),ijpak,2)
         call pkzeros2(xtphpak(1,1,n),ijpak,2)
         call pkzeros2(xtpfpak(1,1,n),ijpak,ilev)
#endif
#if defined (xprhsc)
         call pkzeros2(xtpvpak(1,1,n),ijpak,ilev)
         call pkzeros2(xtpmpak(1,1,n),ijpak,ilev)
         call pkzeros2(xtpcpak(1,1,n),ijpak,ilev)
         call pkzeros2(xtpppak(1,1,n),ijpak,ilev)
#endif
#if defined (x01)
         call pkzeros2(x01pak(1,1,n),ijpak,ilev)
#endif
#if defined (x02)
         call pkzeros2(x02pak(1,1,n),ijpak,ilev)
#endif
#if defined (x03)
         call pkzeros2(x03pak(1,1,n),ijpak,ilev)
#endif
#if defined (x04)
         call pkzeros2(x04pak(1,1,n),ijpak,ilev)
#endif
#if defined (x05)
         call pkzeros2(x05pak(1,1,n),ijpak,ilev)
#endif
#if defined (x06)
         call pkzeros2(x06pak(1,1,n),ijpak,ilev)
#endif
#if defined (x07)
         call pkzeros2(x07pak(1,1,n),ijpak,ilev)
#endif
#if defined (x08)
         call pkzeros2(x08pak(1,1,n),ijpak,ilev)
#endif
#if defined (x09)
         call pkzeros2(x09pak(1,1,n),ijpak,ilev)
#endif
end do

#if defined (xtraconv)
      call pkzeros2(capepak,ijpak,   1)
      call pkzeros2(cinhpak,ijpak,   1)
      call pkzeros2(bcdpak,ijpak,   1)
      call pkzeros2(bcspak,ijpak,   1)
      call pkzeros2(tcdpak,ijpak,   1)
      call pkzeros2(tcspak,ijpak,   1)
      call pkzeros2(cdcbpak,ijpak,   1)
      call pkzeros2(cscbpak,ijpak,   1)
      call pkzeros2(dmcpak,ijpak,ilev)
      call pkzeros2(smcpak,ijpak,ilev)
      call pkzeros2(dmcdpak,ijpak,ilev)
      call pkzeros2(dmcupak,ijpak,ilev)
#endif

#if defined (xtrachem)
      call pkzeros2(dd4pak,ijpak,   1)
      call pkzeros2(dox4pak,ijpak,   1)
      call pkzeros2(doxdpak,ijpak,   1)
      call pkzeros2(esdpak ,ijpak,   1)
      call pkzeros2(esfspak,ijpak,   1)
      call pkzeros2(eaispak,ijpak,   1)
      call pkzeros2(estspak,ijpak,   1)
      call pkzeros2(efispak,ijpak,   1)
      call pkzeros2(esfbpak,ijpak,   1)
      call pkzeros2(eaibpak,ijpak,   1)
      call pkzeros2(estbpak,ijpak,   1)
      call pkzeros2(efibpak,ijpak,   1)
      call pkzeros2(esfopak,ijpak,   1)
      call pkzeros2(eaiopak,ijpak,   1)
      call pkzeros2(estopak,ijpak,   1)
      call pkzeros2(efiopak,ijpak,   1)
      call pkzeros2(edslpak,ijpak,   1)
      call pkzeros2(edsopak,ijpak,   1)
      call pkzeros2(esvcpak,ijpak,   1)
      call pkzeros2(esvepak,ijpak,   1)
      call pkzeros2(noxdpak,ijpak,   1)
      call pkzeros2(wdd4pak,ijpak,   1)
      call pkzeros2(wdl4pak,ijpak,   1)
      call pkzeros2(wds4pak,ijpak,   1)
#ifndef pla
      call pkzeros2(slo3pak,ijpak,   1)
      call pkzeros2(slhppak,ijpak,   1)
      call pkzeros2(sso3pak,ijpak,   1)
      call pkzeros2(sshppak,ijpak,   1)
      call pkzeros2(wds6pak,ijpak,   1)
      call pkzeros2(sdo3pak,ijpak,   1)
      call pkzeros2(sdhppak,ijpak,   1)
      call pkzeros2(dd6pak,ijpak,   1)
      call pkzeros2(wdl6pak,ijpak,   1)
      call pkzeros2(wdd6pak,ijpak,   1)
      call pkzeros2(dddpak ,ijpak,   1)
      call pkzeros2(ddbpak ,ijpak,   1)
      call pkzeros2(ddopak ,ijpak,   1)
      call pkzeros2(ddspak ,ijpak,   1)
      call pkzeros2(wdldpak,ijpak,   1)
      call pkzeros2(wdlbpak,ijpak,   1)
      call pkzeros2(wdlopak,ijpak,   1)
      call pkzeros2(wdlspak,ijpak,   1)
      call pkzeros2(wdddpak,ijpak,   1)
      call pkzeros2(wddbpak,ijpak,   1)
      call pkzeros2(wddopak,ijpak,   1)
      call pkzeros2(wddspak,ijpak,   1)
      call pkzeros2(wdsdpak,ijpak,   1)
      call pkzeros2(wdsbpak,ijpak,   1)
      call pkzeros2(wdsopak,ijpak,   1)
      call pkzeros2(wdsspak,ijpak,   1)
#endif
#endif

#if defined (xtrals)
      call pkzeros2(aggpak, ijpak,ilev)
      call pkzeros2(autpak, ijpak,ilev)
      call pkzeros2(cndpak, ijpak,ilev)
      call pkzeros2(deppak, ijpak,ilev)
      call pkzeros2(evppak, ijpak,ilev)
      call pkzeros2(frhpak, ijpak,ilev)
      call pkzeros2(frkpak, ijpak,ilev)
      call pkzeros2(frspak, ijpak,ilev)
      call pkzeros2(mltipak,ijpak,ilev)
      call pkzeros2(mltspak,ijpak,ilev)
      call pkzeros2(raclpak,ijpak,ilev)
      call pkzeros2(rainpak,ijpak,ilev)
      call pkzeros2(sacipak,ijpak,ilev)
      call pkzeros2(saclpak,ijpak,ilev)
      call pkzeros2(snowpak,ijpak,ilev)
      call pkzeros2(subpak, ijpak,ilev)
      call pkzeros2(sedipak,ijpak, ilev)
      call pkzeros2(rliqpak,ijpak, ilev)
      call pkzeros2(ricepak,ijpak, ilev)
      call pkzeros2(cliqpak,ijpak, ilev)
      call pkzeros2(cicepak,ijpak, ilev)
      call pkzeros2(rlncpak,ijpak, ilev)
      call pkzeros2(rliqpal,ijpak, ilev)
      call pkzeros2(ricepal,ijpak, ilev)
      call pkzeros2(cliqpal,ijpak, ilev)
      call pkzeros2(cicepal,ijpak, ilev)
      call pkzeros2(rlncpal,ijpak, ilev)
!
      call pkzeros2(vaggpak,ijpak,   1)
      call pkzeros2(vautpak,ijpak,   1)
      call pkzeros2(vcndpak,ijpak,   1)
      call pkzeros2(vdeppak,ijpak,   1)
      call pkzeros2(vevppak,ijpak,   1)
      call pkzeros2(vfrhpak,ijpak,   1)
      call pkzeros2(vfrkpak,ijpak,   1)
      call pkzeros2(vfrspak,ijpak,   1)
      call pkzeros2(vmlipak,ijpak,   1)
      call pkzeros2(vmlspak,ijpak,   1)
      call pkzeros2(vrclpak,ijpak,   1)
      call pkzeros2(vscipak,ijpak,   1)
      call pkzeros2(vsclpak,ijpak,   1)
      call pkzeros2(vsubpak,ijpak,   1)
      call pkzeros2(vsedipak,ijpak,  1)
      call pkzeros2(reliqpak,ijpak,  1)
      call pkzeros2(reicepak,ijpak,  1)
      call pkzeros2(cldliqpak,ijpak, 1)
      call pkzeros2(cldicepak,ijpak, 1)
      call pkzeros2(ctlncpak,ijpak,  1)
      call pkzeros2(cllncpak,ijpak,  1)
      call pkzeros2(reliqpal,ijpak,  1)
      call pkzeros2(reicepal,ijpak,  1)
      call pkzeros2(cldliqpal,ijpak, 1)
      call pkzeros2(cldicepal,ijpak, 1)
      call pkzeros2(ctlncpal,ijpak,  1)
      call pkzeros2(cllncpal,ijpak,  1)
#endif

#if defined (xtradust)
      call pkzeros2(duwdpak,ijpak,   1)
      call pkzeros2(dustpak,ijpak,   1)
      call pkzeros2(duthpak,ijpak,   1)
      call pkzeros2(usmkpak,ijpak,   1)
      call pkzeros2(fallpak,ijpak,   1)
      call pkzeros2(fa10pak,ijpak,   1)
      call pkzeros2(fa2pak,ijpak,   1)
      call pkzeros2(fa1pak,ijpak,   1)
      call pkzeros2(gustpak,ijpak,   1)
      call pkzeros2(zspdpak,ijpak,   1)
      call pkzeros2(vgfrpak,ijpak,   1)
      call pkzeros2(smfrpak,ijpak,   1)
#endif

#if defined (aodpth)
      call pkzeros2(exb1pak,ijpak,   1)
      call pkzeros2(exb2pak,ijpak,   1)
      call pkzeros2(exb3pak,ijpak,   1)
      call pkzeros2(exb4pak,ijpak,   1)
      call pkzeros2(exb5pak,ijpak,   1)
      call pkzeros2(exbtpak,ijpak,   1)
      call pkzeros2(odb1pak,ijpak,   1)
      call pkzeros2(odb2pak,ijpak,   1)
      call pkzeros2(odb3pak,ijpak,   1)
      call pkzeros2(odb4pak,ijpak,   1)
      call pkzeros2(odb5pak,ijpak,   1)
      call pkzeros2(odbtpak,ijpak,   1)
      call pkzeros2(odbvpak,ijpak,   1)
      call pkzeros2(ofb1pak,ijpak,   1)
      call pkzeros2(ofb2pak,ijpak,   1)
      call pkzeros2(ofb3pak,ijpak,   1)
      call pkzeros2(ofb4pak,ijpak,   1)
      call pkzeros2(ofb5pak,ijpak,   1)
      call pkzeros2(ofbtpak,ijpak,   1)
      call pkzeros2(abb1pak,ijpak,   1)
      call pkzeros2(abb2pak,ijpak,   1)
      call pkzeros2(abb3pak,ijpak,   1)
      call pkzeros2(abb4pak,ijpak,   1)
      call pkzeros2(abb5pak,ijpak,   1)
      call pkzeros2(abbtpak,ijpak,   1)
!
      call pkzeros2(exb1pal,ijpak,   1)
      call pkzeros2(exb2pal,ijpak,   1)
      call pkzeros2(exb3pal,ijpak,   1)
      call pkzeros2(exb4pal,ijpak,   1)
      call pkzeros2(exb5pal,ijpak,   1)
      call pkzeros2(exbtpal,ijpak,   1)
      call pkzeros2(odb1pal,ijpak,   1)
      call pkzeros2(odb2pal,ijpak,   1)
      call pkzeros2(odb3pal,ijpak,   1)
      call pkzeros2(odb4pal,ijpak,   1)
      call pkzeros2(odb5pal,ijpak,   1)
      call pkzeros2(odbtpal,ijpak,   1)
      call pkzeros2(odbvpal,ijpak,   1)
      call pkzeros2(ofb1pal,ijpak,   1)
      call pkzeros2(ofb2pal,ijpak,   1)
      call pkzeros2(ofb3pal,ijpak,   1)
      call pkzeros2(ofb4pal,ijpak,   1)
      call pkzeros2(ofb5pal,ijpak,   1)
      call pkzeros2(ofbtpal,ijpak,   1)
      call pkzeros2(abb1pal,ijpak,   1)
      call pkzeros2(abb2pal,ijpak,   1)
      call pkzeros2(abb3pal,ijpak,   1)
      call pkzeros2(abb4pal,ijpak,   1)
      call pkzeros2(abb5pal,ijpak,   1)
      call pkzeros2(abbtpal,ijpak,   1)
!
      call pkzeros2(exs1pak,ijpak,   1)
      call pkzeros2(exs2pak,ijpak,   1)
      call pkzeros2(exs3pak,ijpak,   1)
      call pkzeros2(exs4pak,ijpak,   1)
      call pkzeros2(exs5pak,ijpak,   1)
      call pkzeros2(exstpak,ijpak,   1)
      call pkzeros2(ods1pak,ijpak,   1)
      call pkzeros2(ods2pak,ijpak,   1)
      call pkzeros2(ods3pak,ijpak,   1)
      call pkzeros2(ods4pak,ijpak,   1)
      call pkzeros2(ods5pak,ijpak,   1)
      call pkzeros2(odstpak,ijpak,   1)
      call pkzeros2(odsvpak,ijpak,   1)
      call pkzeros2(ofs1pak,ijpak,   1)
      call pkzeros2(ofs2pak,ijpak,   1)
      call pkzeros2(ofs3pak,ijpak,   1)
      call pkzeros2(ofs4pak,ijpak,   1)
      call pkzeros2(ofs5pak,ijpak,   1)
      call pkzeros2(ofstpak,ijpak,   1)
      call pkzeros2(abs1pak,ijpak,   1)
      call pkzeros2(abs2pak,ijpak,   1)
      call pkzeros2(abs3pak,ijpak,   1)
      call pkzeros2(abs4pak,ijpak,   1)
      call pkzeros2(abs5pak,ijpak,   1)
      call pkzeros2(abstpak,ijpak,   1)
!
      call pkzeros2(exs1pal,ijpak,   1)
      call pkzeros2(exs2pal,ijpak,   1)
      call pkzeros2(exs3pal,ijpak,   1)
      call pkzeros2(exs4pal,ijpak,   1)
      call pkzeros2(exs5pal,ijpak,   1)
      call pkzeros2(exstpal,ijpak,   1)
      call pkzeros2(ods1pal,ijpak,   1)
      call pkzeros2(ods2pal,ijpak,   1)
      call pkzeros2(ods3pal,ijpak,   1)
      call pkzeros2(ods4pal,ijpak,   1)
      call pkzeros2(ods5pal,ijpak,   1)
      call pkzeros2(odstpal,ijpak,   1)
      call pkzeros2(odsvpal,ijpak,   1)
      call pkzeros2(ofs1pal,ijpak,   1)
      call pkzeros2(ofs2pal,ijpak,   1)
      call pkzeros2(ofs3pal,ijpak,   1)
      call pkzeros2(ofs4pal,ijpak,   1)
      call pkzeros2(ofs5pal,ijpak,   1)
      call pkzeros2(ofstpal,ijpak,   1)
      call pkzeros2(abs1pal,ijpak,   1)
      call pkzeros2(abs2pal,ijpak,   1)
      call pkzeros2(abs3pal,ijpak,   1)
      call pkzeros2(abs4pal,ijpak,   1)
      call pkzeros2(abs5pal,ijpak,   1)
      call pkzeros2(abstpal,ijpak,   1)
#endif

#if (defined(pla) && defined(pam))
#if defined (xtrapla1)
      call pkzeros2(sncnpak,ijpak,ilev)
      call pkzeros2(ssunpak,ijpak,ilev)
      call pkzeros2(scndpak,ijpak,ilev)
      call pkzeros2(sgsppak,ijpak,ilev)
      call pkzeros2(ccnpak ,ijpak,ilev)
      call pkzeros2(cc02pak,ijpak,ilev)
      call pkzeros2(cc04pak,ijpak,ilev)
      call pkzeros2(cc08pak,ijpak,ilev)
      call pkzeros2(cc16pak,ijpak,ilev)
      call pkzeros2(ccnepak,ijpak,ilev)
      call pkzeros2(acaspak,ijpak,ilev)
      call pkzeros2(acoapak,ijpak,ilev)
      call pkzeros2(acbcpak,ijpak,ilev)
      call pkzeros2(acsspak,ijpak,ilev)
      call pkzeros2(acmdpak,ijpak,ilev)
      call pkzeros2(ntpak,ijpak,ilev)
      call pkzeros2(n20pak,ijpak,ilev)
      call pkzeros2(n50pak,ijpak,ilev)
      call pkzeros2(n100pak,ijpak,ilev)
      call pkzeros2(n200pak,ijpak,ilev)
      call pkzeros2(wtpak,ijpak,ilev)
      call pkzeros2(w20pak,ijpak,ilev)
      call pkzeros2(w50pak,ijpak,ilev)
      call pkzeros2(w100pak,ijpak,ilev)
      call pkzeros2(w200pak,ijpak,ilev)
      call pkzeros2(rcripak,ijpak,ilev)
      call pkzeros2(supspak,ijpak,ilev)
      call pkzeros2(henrpak,ijpak,ilev)
      call pkzeros2(o3frpak,ijpak,ilev)
      call pkzeros2(h2o2frpak,ijpak,ilev)
      call pkzeros2(wparpak,ijpak,ilev)
      call pkzeros2(vncnpak,ijpak,   1)
      call pkzeros2(vasnpak,ijpak,   1)
      call pkzeros2(vscdpak,ijpak,   1)
      call pkzeros2(vgsppak,ijpak,   1)
      call pkzeros2(voaepak,ijpak,   1)
      call pkzeros2(vbcepak,ijpak,   1)
      call pkzeros2(vasepak,ijpak,   1)
      call pkzeros2(vmdepak,ijpak,   1)
      call pkzeros2(vssepak,ijpak,   1)
      call pkzeros2(voawpak,ijpak,   1)
      call pkzeros2(vbcwpak,ijpak,   1)
      call pkzeros2(vaswpak,ijpak,   1)
      call pkzeros2(vmdwpak,ijpak,   1)
      call pkzeros2(vsswpak,ijpak,   1)
      call pkzeros2(voadpak,ijpak,   1)
      call pkzeros2(vbcdpak,ijpak,   1)
      call pkzeros2(vasdpak,ijpak,   1)
      call pkzeros2(vmddpak,ijpak,   1)
      call pkzeros2(vssdpak,ijpak,   1)
      call pkzeros2(voagpak,ijpak,   1)
      call pkzeros2(vbcgpak,ijpak,   1)
      call pkzeros2(vasgpak,ijpak,   1)
      call pkzeros2(vmdgpak,ijpak,   1)
      call pkzeros2(vssgpak,ijpak,   1)
      call pkzeros2(voacpak,ijpak,   1)
      call pkzeros2(vbccpak,ijpak,   1)
      call pkzeros2(vascpak,ijpak,   1)
      call pkzeros2(vmdcpak,ijpak,   1)
      call pkzeros2(vsscpak,ijpak,   1)
      call pkzeros2(vasipak,ijpak,   1)
      call pkzeros2(vas1pak,ijpak,   1)
      call pkzeros2(vas2pak,ijpak,   1)
      call pkzeros2(vas3pak,ijpak,   1)
      call pkzeros2(vccnpak,ijpak,   1)
      call pkzeros2(vcnepak,ijpak,   1)
#endif
#if defined (xtrapla2)
      call pkzeros2(cornpak,ijpak,ilev)
      call pkzeros2(cormpak,ijpak,ilev)
      call pkzeros2(rsn1pak,ijpak,ilev)
      call pkzeros2(rsm1pak,ijpak,ilev)
      call pkzeros2(rsn2pak,ijpak,ilev)
      call pkzeros2(rsm2pak,ijpak,ilev)
      call pkzeros2(rsn3pak,ijpak,ilev)
      call pkzeros2(rsm3pak,ijpak,ilev)
      do isf = 1,isdiag
        call pkzeros2(defxpak(1,isf),ijpak,   1)
        call pkzeros2(defnpak(1,isf),ijpak,   1)
      end do
      call pkzeros2(vrn1pak,ijpak,   1)
      call pkzeros2(vrm1pak,ijpak,   1)
      call pkzeros2(vrn2pak,ijpak,   1)
      call pkzeros2(vrm2pak,ijpak,   1)
      call pkzeros2(vrn3pak,ijpak,   1)
      call pkzeros2(vrm3pak,ijpak,   1)
      call pkzeros2(defapak,ijpak,   1)
      call pkzeros2(defcpak,ijpak,   1)
      do isf = 1,isdust
        call pkzeros2(dmacpak(1,isf),ijpak,   1)
        call pkzeros2(dmcopak(1,isf),ijpak,   1)
        call pkzeros2(dnacpak(1,isf),ijpak,   1)
        call pkzeros2(dncopak(1,isf),ijpak,   1)
      end do
      do isf = 1,isdiag
        call pkzeros2(defxpak(1,isf),ijpak,   1)
        call pkzeros2(defnpak(1,isf),ijpak,   1)
      end do
      do isf = 1,isdnum
        call pkzeros2(sdnupak(1,1,isf),ijpak,ilev)
        call pkzeros2(sdmapak(1,1,isf),ijpak,ilev)
        call pkzeros2(sdacpak(1,1,isf),ijpak,ilev)
        call pkzeros2(sdcopak(1,1,isf),ijpak,ilev)
        call pkzeros2(sssnpak(1,1,isf),ijpak,ilev)
        call pkzeros2(smdnpak(1,1,isf),ijpak,ilev)
        call pkzeros2(sianpak(1,1,isf),ijpak,ilev)
        call pkzeros2(sssmpak(1,1,isf),ijpak,ilev)
        call pkzeros2(smdmpak(1,1,isf),ijpak,ilev)
        call pkzeros2(sewmpak(1,1,isf),ijpak,ilev)
        call pkzeros2(ssumpak(1,1,isf),ijpak,ilev)
        call pkzeros2(socmpak(1,1,isf),ijpak,ilev)
        call pkzeros2(sbcmpak(1,1,isf),ijpak,ilev)
        call pkzeros2(siwmpak(1,1,isf),ijpak,ilev)
      end do
      call pkzeros2(tnsspak,ijpak,   1)
      call pkzeros2(tnmdpak,ijpak,   1)
      call pkzeros2(tniapak,ijpak,   1)
      call pkzeros2(tmsspak,ijpak,   1)
      call pkzeros2(tmmdpak,ijpak,   1)
      call pkzeros2(tmocpak,ijpak,   1)
      call pkzeros2(tmbcpak,ijpak,   1)
      call pkzeros2(tmsppak,ijpak,   1)
      do n = 1,isdnum
        call pkzeros2(snsspak(1,n),ijpak,   1)
        call pkzeros2(snmdpak(1,n),ijpak,   1)
        call pkzeros2(sniapak(1,n),ijpak,   1)
        call pkzeros2(smsspak(1,n),ijpak,   1)
        call pkzeros2(smmdpak(1,n),ijpak,   1)
        call pkzeros2(smocpak(1,n),ijpak,   1)
        call pkzeros2(smbcpak(1,n),ijpak,   1)
        call pkzeros2(smsppak(1,n),ijpak,   1)
        call pkzeros2(siwhpak(1,n),ijpak,   1)
        call pkzeros2(sewhpak(1,n),ijpak,   1)
      end do
      call pkzeros2(pm25pak,ijpak,ilev)
      call pkzeros2(pm10pak,ijpak,ilev)
      call pkzeros2(dm25pak,ijpak,ilev)
      call pkzeros2(dm10pak,ijpak,ilev)
#endif
#endif

#if defined (rad_flux_profs)
      call pkzeros2(fsaupak,ijpak,ilev + 2)
      call pkzeros2(fsadpak,ijpak,ilev + 2)
      call pkzeros2(flaupak,ijpak,ilev + 2)
      call pkzeros2(fladpak,ijpak,ilev + 2)
      call pkzeros2(fscupak,ijpak,ilev + 2)
      call pkzeros2(fscdpak,ijpak,ilev + 2)
      call pkzeros2(flcupak,ijpak,ilev + 2)
      call pkzeros2(flcdpak,ijpak,ilev + 2)
#endif
call pkzeros2(fsaupal,ijpak,ilev + 2)
call pkzeros2(fsadpal,ijpak,ilev + 2)
call pkzeros2(flaupal,ijpak,ilev + 2)
call pkzeros2(fladpal,ijpak,ilev + 2)
call pkzeros2(fscupal,ijpak,ilev + 2)
call pkzeros2(fscdpal,ijpak,ilev + 2)
call pkzeros2(flcupal,ijpak,ilev + 2)
call pkzeros2(flcdpal,ijpak,ilev + 2)

#if defined (radforce)
      do nrf = 1,nrfp
         call pkzeros2(fsaupak_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fsadpak_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(flaupak_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fladpak_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fscupak_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fscdpak_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(flcupak_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(flcdpak_r(1,1,nrf),ijpak,ilev + 2)

         call pkzeros2(fsaupal_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fsadpal_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(flaupal_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fladpal_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fscupal_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(fscdpal_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(flcupal_r(1,1,nrf),ijpak,ilev + 2)
         call pkzeros2(flcdpal_r(1,1,nrf),ijpak,ilev + 2)

!
         call pkzeros2(rdtpak_r(1,1,nrf),ijpak,ilev)
         call pkzeros2(rdtpal_r(1,1,nrf),ijpak,ilev)
         call pkzeros2(rdtpam_r(1,1,nrf),ijpak,ilev)
         call pkzeros2(hrspak_r(1,1,nrf),ijpak,ilev)
         call pkzeros2(hrlpak_r(1,1,nrf),ijpak,ilev)
         call pkzeros2(hrscpak_r(1,1,nrf),ijpak,ilev)
         call pkzeros2(hrlcpak_r(1,1,nrf),ijpak,ilev)

         call pkzeros2(csbpat_r(1,1,nrf),ijpak,im)
         call pkzeros2(clbpat_r(1,1,nrf),ijpak,im)
         call pkzeros2(fsgpat_r(1,1,nrf),ijpak,im)
         call pkzeros2(flgpat_r(1,1,nrf),ijpak,im)

       end do ! nrfp

       call pkzeros2(csbpal_r,ijpak,nrfp)
       call pkzeros2(fsgpal_r,ijpak,nrfp)
       call pkzeros2(fsspal_r,ijpak,nrfp)
       call pkzeros2(fsscpal_r,ijpak,nrfp)
       call pkzeros2(clbpal_r,ijpak,nrfp)
       call pkzeros2(flgpal_r,ijpak,nrfp)
       call pkzeros2(fdlpal_r,ijpak,nrfp)
       call pkzeros2(fdlcpal_r,ijpak,nrfp)
       call pkzeros2(fsrpal_r,ijpak,nrfp)
       call pkzeros2(fsrcpal_r,ijpak,nrfp)
       call pkzeros2(fsopal_r,ijpak,nrfp)
       call pkzeros2(olrpal_r,ijpak,nrfp)
       call pkzeros2(olrcpal_r,ijpak,nrfp)
       call pkzeros2(fslopal_r,ijpak,nrfp)

#endif

#if defined (use_cosp)
! cosp input for cloudsat simulator
      call pkzeros2(rmixpak,ijpak,ilev)
      call pkzeros2(smixpak,ijpak,ilev)
      call pkzeros2(rrefpak,ijpak,ilev)
      call pkzeros2(srefpak,ijpak,ilev)
#endif
!
gapak = 0.
gflxpak = 0.
petpak = 0.
uepak = 0.
!
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
