!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

module vtau_defs

  !--- jason cole  ...21 jan 2018 -> added vtaufile_type
  !--- jason cole  ...22 dec 2016 -> initial version

  !-------------------------------------------------------------
  !--- set up namelists for stratospheric aerosol run-time options
  !-------------------------------------------------------------

  !--- flag to indicate how values are to be interpolated
  !--- this corresponds to parmsub parameter "vtau_scn_mode"
  !--- which has the following meaning:
  !---         =   -1     continuously update stratospheric aerosols form data in the file "vtau"
  !---         =  yyyymm  use a constant value from yyyy/mm in the file "vtau"
  !---         = -yyyymm  use an annual cycle from year abs(yyyymm)/100 in the file "vtau"
  implicit none
  integer :: vtau_scn_mode=-1 !<

  ! the vtaufile_type parameter defines the "type" of the stratospheric aerosol forcing file.
  ! vtaufile_type = 1 is a dataset that only contains optical thickness at 550 nm
  !   - for example, sato_volcanic_4_tau_t63_1850_2300_monthly
  ! vtaufile_type = 2 is a dataset that contains optical properties in the cccma radiation bands
  ! with data on pressure levels
  !  - for example, cmip_canesm_radiation_185001-201412_v1
  ! note: the value of vtaufile_type and vtaufile in the basefile must be consistent or the run will crash

  integer :: vtaufile_type=2 !<

  ! the "vtau_offset_year" offset year for the file with the stratospheric aerosols.
  ! vtau_offset_year  =   0 (default) read the forcing data for the same year as the model year
  !                   =  +n read the forcing data for the year ($year + n)
  !                   =  -n read the forcing data for the year ($year - n)

  integer :: vtau_offset_year=0 !<

  !--- namelist used to read in parmsub parameters
  !--- this namelist is read in init_vtau_scenario
  namelist /vtau_config/ vtau_scn_mode, vtaufile_type, &
      vtau_offset_year

end module vtau_defs
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
