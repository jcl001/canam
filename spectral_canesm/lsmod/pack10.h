#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * apr 20/2019 - s.kharin    - fix bug ioffz->ioff in *_ext_gcm_sa_* fields
!     * mar 16/2019 - j. cole     - add  3-hour 2d fields (cf3hr,e3hr)
!     * nov 29/2018 - j. cole     - add/adjust tendencies
!     * nov 02/2018 - j. cole     - add radiative flux profiles
!     * nov 03/2018 - m.lazare.   - added gsno and fnla.
!     *                           - added 3hr save fields.
!     * oct 01/2018 - s.kharin.   - add xtvipas/xtviros for sampled burdens.
!     * aug 14/2018 - m.lazare.     remove maskpak,gtpal.
!     * aug 14/2018 - m.lazare.     remove {gtm,sicp,sicm,sicnp,sicm}.
!     * aug 01/2018 - m.lazare.     remove {obeg,obwg,res,gc}.
!     * jul 30/2018 - m.lazare.   - unused fmirow removed.
!     *                           - removed non-transient aerosol emission cpp blocks.
!     * mar 09/2018 - m.lazare.   - beg  and bwg  changed from pak/row to
!     *                             pal/rol.
!     * feb 27/2018 - m.lazare.   - qfsl and begl changed from pak/row to
!     *                             pal/rol.
!     *                           - added {begk,bwgk,bwgl,qfso}.
!     * feb 06/2018 - m.lazare.    - add {ftox,ftoy} and {tdox,tdoy}.
!     * nov 01/2017 - j. cole.     - update for cmip6 stratospheric aerosols
!     * aug 09/2017 - m.lazare.    - add fnpat/fnrot.
!     *                            - flak->{flkr,flku}.
!     *                            - licn->gicn.
!     *                            - add lakes arrays.
!     * aug 07/2017 - m.lazare.    - initial git verison.
!     * mar 26/2015 - m.lazare.    final version for gcm18:
!     *                            - unused {cstrol,cltrol} removed.
!     *                            - add (for nemo/cice support):
!     *                              hsearol,rainsrol,snowsrol
!     *                              obegrol,obwgrol,begorol,bwgorol,
!     *                              begirol,hflirol,
!     *                              hsearol,rainsrol,snowsrol.
!     *                            - remove: ftilpak/ftilrow.
!     *                            - add (for harmonization of field
!     *                              capacity and wilting point between
!     *                              ctem and class):
!     *                              thlwpat/thlwrot.
!     *                            - add (for soil colour index look-up
!     *                              for land surface albedo):
!     *                              algdvpat/algdvrot, algdnpat/algdnrot,
!     *                              algwvpat/algwvrot, algwnpat/algwnrot,
!     *                              socipat/socirot.
!     *                            - add (for fractional land/water/ice):
!     *                              salbpat/salbrot, csalpat/csalrot,
!     *                              emispak/emisrow, emispat/emisrot,
!     *                              wrkapal/wrkarol, wrkbpal/wrkbrol,
!     *                              snopako/snorowo.
!     *                            - add (for pla):
!     *                              psvvpak/psvvrow, pdevpak/pdevrow,
!     *                              pdivpak/pdivrow, prevpak/prevrow,
!     *                              privpak/privrow.
!     *                            - bugfixes for isccp fields and additions
!     *                              for calipso and modis.
!     * feb 04/2014 - m.lazare.    interim version for gcm18:
!     *                            - flakpak,flndpak,gtpat,licnpak added.
!     *                            - duplicate calls for {edso,edso,esvc,esve}
!     *                              removed.
!     * nov 19/2013 - m.lazare.    cosmetic: remove {gflx,ga,hbl,ilmo,pet,ue,
!     *                                      wtab,rofs,rofb) "PAT"/"ROT" arrays.
!     * jul 10/2013 - m.lazare/    previous version pack9 for gcm17:
!     *               k.vonsalzen/ - fsf added as prognostic field
!     *               j.cole/        rather than residual.
!     *                            - extra diagnostic microphysics
!     *                              fields added:
!     *                              {sedi,rliq,rice,rlnc,cliq,cice,
!     *                               vsedi,reliq,reice,cldliq,cldice,ctlnc,cllnc}.
!     *                            - new emission fields:
!     *                              {sbio,sshi,obio,oshi,bbio,bshi} and
!     *                              required altitude field alti.
!     *                            - many new aerosol diagnostic fields
!     *                              for pam, including those for cpp options:
!     *                              "pfrc", "xtrapla1" and "xtrapla2".
!     *                            - the following fields were removed
!     *                              from the "aodpth" cpp option:
!     *                              {sab1,sab2,sab3,sab4,sab5,sabt} and
!     *                              {sas1,sas2,sas3,sas4,sas5,sast},
!     *                              (both pak and pal).
!     *                            - due to the implementation of the mosaic
!     *                              for class_v3.6, prognostic fields
!     *                              were changed from pak/row to pat/rot,
!     *                              with an extra "IM" dimension (for
!     *                              the number of mosaic tiles).
!     *                            - the instantaneous band-mean values for
!     *                              solar fields are now prognostic
!     *                              as well: {fsdb,fsfb,csdb,csfb,fssb,fsscb}.
!     *                            - removed "histemi" fields.
!     *                            - "xtraconv" cpp option moved to before
!     *                              "xtrachem", to be consistent with others.
!     * nb: the following are intermediate revisions to frozen gcm16 code
!     *     (upwardly compatible) to support the new class version in development:
!     * oct 18/2011 - m.lazare.    - add thr,thm,bi,psis,grks,thra,hcps,tcs,thfc,
!     *                              psiw,algd,algw,zbtw,isnd,igdr but only for
!     *                              kount>kstart.
!     * oct 07/2011 - m.lazare.    - add gflx,ga,hbl,pet,ilmo,rofb,rofs,ue,wtab.
!     * jul 13/2011 - e.chan.      - add fare, thlq, thic. change selected
!     *                            - pak variables to packed tile versions
!     *                              (pat). add selected pat variables in
!     *                              addition to their pak counterparts.
!     *                            - add tpnd, zpnd, tav, qav, wsno, tsfs.
!     * may 03/2012 - m.lazare/    previous version pack8 for gcm16:
!     *               k.vonsalzen/ - modify fields to support a newer
!     *               j.cole/        version of cosp which includes
!     *               y.peng.        the modis, misr and ceres and
!     *                              save the appropriate output.
!     *                            - remove {fsa,fla,fstc,fltc} and
!     *                              replace by {fsr,olr,fsrc,olrc}.
!     *                            - new conditional diagnostic output
!     *                              under control of "XTRADUST" and
!     *                              "AODPTH".
!     *                            - additional mam radiation output
!     *                              fields {fsan,flan}.
!     *                            - additional correlated-k output
!     *                              fields: "FLG", "FDLC" and "FLAM".
!     *                            - "FLGROL_R" is actual calculated
!     *                              radiative forcing term instead
!     *                              of "FDLROL_R-SIGMA*T**4".
!     *                            - include "ISEED" for random number
!     *                              generator seed now calculated
!     *                              in model driver instead of physics.
!     * may 02/2010 - m.lazare/    previous version pack7i for gcm15i:
!     *               k.vonsalzen/ - add fsopal/fsorol ("FSOL").
!     *               j.cole.      - add fields {rmix,smix,rref,sref}
!     *                              for cosp input, many fields for
!     *                              cosp output (with different options)
!     *                              and remove previous direct isccp
!     *                              fields.
!     *                            - add new diagnostic fields:
!     *                              swa (pak/row and pal/rol),swx,swxu,
!     *                              swxv,srh,srhn,srhx,fsdc,fssc,fdlc
!     *                              and remove: swmx.
!     *                            - for convection, add: dmcd,dmcu and
!     *                              remove: acmt,dcmt,scmt,pcps,clds,
!     *                              lhrd,lhrs,shrd,shrs.
!     *                            - add fields for ctem under control
!     *                              of new cpp directive: "COUPLER_CTEM".
!     *                            - add new fields for coupler: ofsg,
!     *                              phis,pmsl,xsrf.
!     *                            - previous update direcive "HISTEMI"
!     *                              converted to cpp:
!     *                              "TRANSIENT_AEROSOL_EMISSIONS" with
!     *                              further choice of cpp directives
!     *                              "HISTEMI" for historical or
!     *                              "EMISTS" for future emissions.
!     *                              the "HISTEMI" fields are the
!     *                              same as previously, except that
!     *                              ??? has been removed. for "EMISTS",
!     *                              the following are added (both pak/row
!     *                              and pal/rol since interpolated):
!     *                              sair,ssfc,sstk,sfir,oair,osfc,ostk,ofir,
!     *                              bair,bsfc,bstk,bfir. the first letter
!     *                              indicates the species ("B" for black
!     *                              carbon, "O" for organic carbon and
!     *                              "S" for sulfur), while for each of
!     *                              these, there are "AIR" for aircraft,
!     *                              "SFC" for surface, "STK" for stack
!     *                              and "FIR" for fire, each having
!     *                              different emission heights.
!     *                            - for the chemistry, the following
!     *                              fields have been added:
!     *                              ddd,ddb,ddo,dds,wdld,wdlb,wdlo,wdls,
!     *                              wddd,wddb,wddo,wdds,wdsd,wdsb,wdso,
!     *                              wdss,esd,esfs,eais,ests,efis,esfb,
!     *                              eaib,estb,efib,esfo,eaio,esto,efio
!     *                              and the following have been removed:
!     *                              asfs,ashp,aso3,awds,dafx,dcfx,dda,ddc,
!     *                              esbt,esff,eoff,ebff,eobb,ebbb,eswf,
!     *                              sfd,sfs,wdda,wddc,wdla,wdlc,wdsa,wdsc,
!     *                              cdph,clph,csph.
!     *                            - fairpak/fairrow and fairpal/fairrol
!     *                              added for aircraft emissions
!     *                            - o3cpak/o3crow and o3cpal/o3crol
!     *                              added for chemical ozone interpolation.
!     *                            - DON'T PACK CLASS INPUT FIELDS
!     *                              (fcan,alic,alvc,cmas,lnzo,root)
!     *                              since are invariant.
!     *                            - update directives turned into
!     *                              cpp directives.
!     * feb 19/2009 - m.lazare.    previous version pack7h for gcm15h:
!     *                            - add new fields for emissions: eoff,
!     *                              ebff,eobb,ebbb.
!     *                            - add new fields for diagnostics of
!     *                              conservation: qtpt,xtpt.
!     *                            - add new field for chemistry: sfrc.
!     *                            - add new diagnostic cloud fields
!     *                              (using optical depth cutoff):
!     *                              cldo (both pak and pal).
!     *                            - reorganize emission forcing fields
!     *                              dependant whether are under
!     *                              historical emissions (%df histemi)
!     *                              or not.
!     *                            - add fields for explosive volcanoes:
!     *                              vtau and trop under control of
!     *                              "%DF EXPLVOL". each have both pak
!     *                              and pal.
!     * apr 21/2008 - l.solheim/   previous version pack7g for gcm15g:
!     *               m.lazare/    -  add new radiative forcing arrays
!     *               k.vonsalzen/    (under control of "%DF RADFORCE").
!     *               x.ma.        - new diagnostic fields: wdd4,wds4,edsl,
!     *                              esbf,esff,esvc,esve,eswf along with
!     *                              tdem->edso (under control of
!     *                              "XTRACHEM"), as well as almx,almc
!     *                              and instantaneous clwt,cidt.
!     *                            - new aerocom forcing fields:
!     *                              ebwa,eowa,eswa,eost (each with accumulated
!     *                              and target),escv,ehcv,esev,ehev,
!     *                              ebbt,eobt,ebft,eoft,esdt,esit,esst,
!     *                              esot,espt,esrt.
!     *                            - code related to s/l removed.
!     *                            - remove unused: qtpn,utpm,vtpm,tsem,ebc,
!     *                              eoc,eocf,eso2,evol,hvol.
!     * jan 11/2006 - j.cole/      previous version pack7f for gcm15f:
!     *               m.lazare.    - add isccp simulator fields from jason.
!     * nov 26/2006 - m.lazare.    new version for gcm15f:
!     *                            - two extra new fields ("DMC" and "SMC")
!     *                              under control of "%IF DEF,XTRACONV".
!     * may 07/2006 - m.lazare.    previous version for pack7e for gcm15e.
!========================================================================
!     * general physics arrays.
!
do l=1,ilev
  do i=il1,il2
    almcpak(ioff+i,l)  = almcrow(i,l)
    almxpak(ioff+i,l)  = almxrow(i,l)
    cicpak (ioff+i,l)  = cicrow (i,l)
    clcvpak(ioff+i,l)  = clcvrow(i,l)
    cldpak (ioff+i,l)  = cldrow (i,l)
    clwpak (ioff+i,l)  = clwrow (i,l)
    cvarpak(ioff+i,l)  = cvarrow(i,l)
    cvmcpak(ioff+i,l)  = cvmcrow(i,l)
    cvsgpak(ioff+i,l)  = cvsgrow(i,l)
    ftoxpak(ioff+i,l)  = ftoxrow(i,l)
    ftoypak(ioff+i,l)  = ftoyrow(i,l)
    hrlpak (ioff+i,l)  = hrlrow (i,l)
    hrspak (ioff+i,l)  = hrsrow (i,l)
    hrlcpak (ioff+i,l) = hrlcrow (i,l)
    hrscpak (ioff+i,l) = hrscrow (i,l)
    ometpak(ioff+i,l)  = ometrow(i,l)
    qwf0pal(ioff+i,l)  = qwf0rol(i,l)
    qwfmpal(ioff+i,l)  = qwfmrol(i,l)
    rhpak  (ioff+i,l)  = rhrow  (i,l)
    scdnpak(ioff+i,l)  = scdnrow(i,l)
    sclfpak(ioff+i,l)  = sclfrow(i,l)
    slwcpak(ioff+i,l)  = slwcrow(i,l)
    tacnpak(ioff+i,l)  = tacnrow(i,l)
    tdoxpak(ioff+i,l)  = tdoxrow(i,l)
    tdoypak(ioff+i,l)  = tdoyrow(i,l)
    zdetpak(ioff+i,l)  = zdetrow(i,l)
  end do
end do
!
do n=1,ntrac
  do l=1,ilev
    do i=il1,il2
      sfrcpal (ioff+i,l,n) = sfrcrol (i,l,n)
    end do
  end do
end do
do l=1,ilev
  do i=il1,il2
    altipak(ioff+i,l)   = altirow(i,l)
  end do
end do
!
do l=1,levoz
  do i=il1,il2
    ozpak  (ioff+i,l) = ozrow  (i,l)
    ozpal  (ioff+i,l) = ozrol  (i,l)
  end do
end do
!
do l=1,levozc
  do i=il1,il2
    o3cpak (ioff+i,l) = o3crow (i,l)
    o3cpal (ioff+i,l) = o3crol (i,l)
  end do
end do

!
csalpal(ioff+il1:ioff+il2,1:nbs)     =csalrol(il1:il2,1:nbs)
csalpat(ioff+il1:ioff+il2,1:im,1:nbs)=csalrot(il1:il2,1:im,1:nbs)
salbpal(ioff+il1:ioff+il2,1:nbs)     =salbrol(il1:il2,1:nbs)
salbpat(ioff+il1:ioff+il2,1:im,1:nbs)=salbrot(il1:il2,1:im,1:nbs)
csdpal(ioff+il1:ioff+il2) = csdrol(il1:il2)
csfpal(ioff+il1:ioff+il2) = csfrol(il1:il2)
!
do i=il1,il2
  alphpak(ioff+i) = alphrow(i)
  begipal(ioff+i) = begirol(i)
  begkpal(ioff+i) = begkrol(i)
  beglpal(ioff+i) = beglrol(i)
  begopal(ioff+i) = begorol(i)
  begpal (ioff+i) = begrol (i)
  bwgipal(ioff+i) = bwgirol(i)
  bwgkpal(ioff+i) = bwgkrol(i)
  bwglpal(ioff+i) = bwglrol(i)
  bwgopal(ioff+i) = bwgorol(i)
  bwgpal (ioff+i) = bwgrol (i)
  cbmfpal(ioff+i) = cbmfrol(i)
  chfxpak(ioff+i) = chfxrow(i)
  cictpak(ioff+i) = cictrow(i)
  clbpal (ioff+i) = clbrol (i)
  cldtpak(ioff+i) = cldtrow(i)
  cldopak(ioff+i) = cldorow(i)
  cldopal(ioff+i) = cldorol(i)
  cldapak(ioff+i) = cldarow(i)
  cldapal(ioff+i) = cldarol(i)
  clwtpak(ioff+i) = clwtrow(i)
  cqfxpak(ioff+i) = cqfxrow(i)
  csbpal (ioff+i) = csbrol (i)
  cszpal (ioff+i) = cszrol (i)
  deltpak(ioff+i) = deltrow(i)
  dmsopak(ioff+i) = dmsorow(i)
  dmsopal(ioff+i) = dmsorol(i)
  edmspak(ioff+i) = edmsrow(i)
  edmspal(ioff+i) = edmsrol(i)
  emispak(ioff+i) = emisrow(i)
  envpak (ioff+i) = envrow (i)
  fdlpak (ioff+i) = fdlrow (i)
  fdlcpak(ioff+i) = fdlcrow(i)
  flampal(ioff+i) = flamrol(i)
  flampak(ioff+i) = flamrow(i)
  flanpak(ioff+i) = flanrow(i)
  flanpal(ioff+i) = flanrol(i)
  flgcpak(ioff+i) = flgcrow(i)
  flgpak (ioff+i) = flgrow (i)
  fsampak(ioff+i) = fsamrow(i)
  fsampal(ioff+i) = fsamrol(i)
  fsanpak(ioff+i) = fsanrow(i)
  fsanpal(ioff+i) = fsanrol(i)
  fsdcpak(ioff+i) = fsdcrow(i)
  fsdpak (ioff+i) = fsdrow (i)
  fsdpal (ioff+i) = fsdrol (i)
  fsfpal (ioff+i) = fsfrol (i)
  fsgcpak(ioff+i) = fsgcrow(i)
  fsgpak (ioff+i) = fsgrow (i)
  fsgipal(ioff+i) = fsgirol(i)
  fsgopal(ioff+i) = fsgorol(i)
  fsipal (ioff+i) = fsirol (i)
  fslopak(ioff+i) = fslorow(i)
  fslopal(ioff+i) = fslorol(i)
  fsopak (ioff+i) = fsorow (i)
  fsrcpak(ioff+i) = fsrcrow(i)
  fsrpak (ioff+i) = fsrrow (i)
  fsscpak(ioff+i) = fsscrow(i)
  fsspak (ioff+i) = fssrow (i)
  fsvpak (ioff+i) = fsvrow (i)
  fsvpal (ioff+i) = fsvrol (i)
  gampak (ioff+i) = gamrow (i)
  gicnpak(ioff+i) = gicnrow(i)
  gtapak (ioff+i) = gtarow (i)
  gtpak  (ioff+i) = gtrow  (i)
  hflpak (ioff+i) = hflrow (i)
  hfspak (ioff+i) = hfsrow (i)
  hseapal(ioff+i) = hsearol(i)
  ofsgpal(ioff+i) = ofsgrol(i)
  olrpak (ioff+i) = olrrow (i)
  olrcpak(ioff+i) = olrcrow(i)
  parpal (ioff+i) = parrol (i)
  pblhpak(ioff+i) = pblhrow(i)
  pbltpak(ioff+i) = pbltrow(i)
  pchfpak(ioff+i) = pchfrow(i)
  pcpcpak(ioff+i) = pcpcrow(i)
  pcppak (ioff+i) = pcprow (i)
  pcpspak(ioff+i) = pcpsrow(i)
  plhfpak(ioff+i) = plhfrow(i)
  pmslpal(ioff+i) = pmslrol(i)
  psapak (ioff+i) = psarow (i)
  pshfpak(ioff+i) = pshfrow(i)
  psipak (ioff+i) = psirow (i)
  pwatpak(ioff+i) = pwatrow(i)
  pwatpam(ioff+i) = pwatrom(i)
  qfspak (ioff+i) = qfsrow (i)
  qfslpal(ioff+i) = qfslrol(i)
  qfsopal(ioff+i) = qfsorol(i)
  qfxpak (ioff+i) = qfxrow (i)
  qtpfpam(ioff+i) = qtpfrom(i)
  qtphpam(ioff+i) = qtphrom(i)
  qtptpam(ioff+i) = qtptrom(i)
  rainspal(ioff+i)= rainsrol(i)
  sicnpak(ioff+i) = sicnrow(i)
  sicpak (ioff+i) = sicrow (i)
  sigxpak(ioff+i) = sigxrow(i)
  slimpal(ioff+i) = slimrol(i)
  slimplw(ioff+i) = slimrlw(i)
  slimpsh(ioff+i) = slimrsh(i)
  slimplh(ioff+i) = slimrlh(i)
  snopak (ioff+i) = snorow (i)
  snowspal(ioff+i)= snowsrol(i)
  sqpak  (ioff+i) = sqrow  (i)
  srhpak (ioff+i) = srhrow (i)
  srhnpak(ioff+i) = srhnrow(i)
  srhxpak(ioff+i) = srhxrow(i)
  stmnpak(ioff+i) = stmnrow(i)
  stmxpak(ioff+i) = stmxrow(i)
  stpak  (ioff+i) = strow  (i)
  supak  (ioff+i) = surow  (i)
  svpak  (ioff+i) = svrow  (i)
  swapak (ioff+i) = swarow (i)
  swapal (ioff+i) = swarol (i)
  swxpak (ioff+i) = swxrow (i)
  swxupak(ioff+i) = swxurow(i)
  swxvpak(ioff+i) = swxvrow(i)
  tcvpak (ioff+i) = tcvrow (i)
  tfxpak (ioff+i) = tfxrow (i)
  ufspak (ioff+i) = ufsrow (i)
  ufspal (ioff+i) = ufsrol (i)
  ufsipal(ioff+i) = ufsirol(i)
  ufsopal(ioff+i) = ufsorol(i)
  vfspak (ioff+i) = vfsrow (i)
  vfspal (ioff+i) = vfsrol (i)
  vfsipal(ioff+i) = vfsirol(i)
  vfsopal(ioff+i) = vfsorol(i)
end do
snopat(ioff+il1:ioff+il2,:) = snorot(il1:il2,:)
gtpat (ioff+il1:ioff+il2,:) = gtrot (il1:il2,:)
gtpax (ioff+il1:ioff+il2,:) = gtrox (il1:il2,:)
emispat(ioff+il1:ioff+il2,:) = emisrot(il1:il2,:)
xlmpat(ioff+il1:ioff+il2,:,:)=xlmrot(il1:il2,:,:)
!
do l = 1, nbs
  do i = il1, il2
    fsdbpal (ioff+i,l)  = fsdbrol (i,l)
    fsfbpal (ioff+i,l)  = fsfbrol (i,l)
    csdbpal (ioff+i,l)  = csdbrol (i,l)
    csfbpal (ioff+i,l)  = csfbrol (i,l)
    fssbpal (ioff+i,l)  = fssbrol (i,l)
    fsscbpal (ioff+i,l) = fsscbrol (i,l)
    wrkapal (ioff+i,l)  = wrkarol (i,l)
    wrkbpal (ioff+i,l)  = wrkbrol (i,l)
  end do
end do
clbpat (ioff+il1:ioff+il2,:)=clbrot (il1:il2,:)
csbpat (ioff+il1:ioff+il2,:)=csbrot (il1:il2,:)
csdpat (ioff+il1:ioff+il2,:)=csdrot (il1:il2,:)
csfpat (ioff+il1:ioff+il2,:)=csfrot (il1:il2,:)
fdlpat (ioff+il1:ioff+il2,:)=fdlrot (il1:il2,:)
fdlcpat(ioff+il1:ioff+il2,:)=fdlcrot(il1:il2,:)
flgpat (ioff+il1:ioff+il2,:)=flgrot (il1:il2,:)
fsdpat (ioff+il1:ioff+il2,:)=fsdrot (il1:il2,:)
fsfpat (ioff+il1:ioff+il2,:)=fsfrot (il1:il2,:)
fsgpat (ioff+il1:ioff+il2,:)=fsgrot (il1:il2,:)
fsipat (ioff+il1:ioff+il2,:)=fsirot (il1:il2,:)
fsvpat (ioff+il1:ioff+il2,:)=fsvrot (il1:il2,:)
parpat (ioff+il1:ioff+il2,:)=parrot (il1:il2,:)
!
do l = 1, nbs
  do m = 1, im
    do i = il1, il2
      fsdbpat (ioff+i,m,l)  = fsdbrot (i,m,l)
      fsfbpat (ioff+i,m,l)  = fsfbrot (i,m,l)
      csdbpat (ioff+i,m,l)  = csdbrot (i,m,l)
      csfbpat (ioff+i,m,l)  = csfbrot (i,m,l)
      fssbpat (ioff+i,m,l)  = fssbrot (i,m,l)
      fsscbpat(ioff+i,m,l)  = fsscbrot(i,m,l)
    end do
  end do
end do
!
!     * land surface arrays.
!
do l=1,ignd
  do i=il1,il2
    dzgpak (ioff+i,l) = dzgrow (i,l)
    hfcgpak(ioff+i,l) = hfcgrow(i,l)
    hmfgpak(ioff+i,l) = hmfgrow(i,l)
    porgpak(ioff+i,l) = porgrow(i,l)
    fcappak(ioff+i,l) = fcaprow(i,l)
    qfvgpak(ioff+i,l) = qfvgrow(i,l)
    tgpak  (ioff+i,l) = tgrow  (i,l)
    wgfpak (ioff+i,l) = wgfrow (i,l)
    wglpak (ioff+i,l) = wglrow (i,l)
  end do
end do
gflxpak(ioff+il1:ioff+il2,:)  =gflxrow(il1:il2,:)
tgpat  (ioff+il1:ioff+il2,:,:)=tgrot  (il1:il2,:,:)
thicpat(ioff+il1:ioff+il2,:,:)=thicrot(il1:il2,:,:)
thlqpat(ioff+il1:ioff+il2,:,:)=thlqrot(il1:il2,:,:)
!
!
do i=il1,il2
  anpak  (ioff+i) = anrow  (i)
  drpak  (ioff+i) = drrow  (i)
  flggpak(ioff+i) = flggrow(i)
  flgnpak(ioff+i) = flgnrow(i)
  flgvpak(ioff+i) = flgvrow(i)
  fnlapak(ioff+i) = fnlarow(i)
  fnpak  (ioff+i) = fnrow  (i)
  fsggpak(ioff+i) = fsggrow(i)
  fsgnpak(ioff+i) = fsgnrow(i)
  fsgvpak(ioff+i) = fsgvrow(i)
  fvgpak (ioff+i) = fvgrow (i)
  fvnpak (ioff+i) = fvnrow (i)
  gsnopak(ioff+i) = gsnorow(i)
  hfcnpak(ioff+i) = hfcnrow(i)
  hfcvpak(ioff+i) = hfcvrow(i)
  hflgpak(ioff+i) = hflgrow(i)
  hflnpak(ioff+i) = hflnrow(i)
  hflvpak(ioff+i) = hflvrow(i)
  hfsgpak(ioff+i) = hfsgrow(i)
  hfsnpak(ioff+i) = hfsnrow(i)
  hfsvpak(ioff+i) = hfsvrow(i)
  hmfnpak(ioff+i) = hmfnrow(i)
  hmfvpak(ioff+i) = hmfvrow(i)
  mvpak  (ioff+i) = mvrow  (i)
  pcpnpak(ioff+i) = pcpnrow(i)
  pigpak (ioff+i) = pigrow (i)
  pinpak (ioff+i) = pinrow (i)
  pivfpak(ioff+i) = pivfrow(i)
  pivlpak(ioff+i) = pivlrow(i)
  qfgpak (ioff+i) = qfgrow (i)
  qfnpak (ioff+i) = qfnrow (i)
  qfvfpak(ioff+i) = qfvfrow(i)
  qfvlpak(ioff+i) = qfvlrow(i)
  rhonpak(ioff+i) = rhonrow(i)
  rofnpak(ioff+i) = rofnrow(i)
  rofopak(ioff+i) = roforow(i)
  rofopal(ioff+i) = roforol(i)
  rofpak (ioff+i) = rofrow (i)
  rofpal (ioff+i) = rofrol (i)
  rofvpak(ioff+i) = rofvrow(i)
  rovgpak(ioff+i) = rovgrow(i)
  skygpak(ioff+i) = skygrow(i)
  skynpak(ioff+i) = skynrow(i)
  smltpak(ioff+i) = smltrow(i)
  tbaspak(ioff+i) = tbasrow(i)
  tnpak  (ioff+i) = tnrow  (i)
  ttpak  (ioff+i) = ttrow  (i)
  tvpak  (ioff+i) = tvrow  (i)
  wtrgpak(ioff+i) = wtrgrow(i)
  wtrnpak(ioff+i) = wtrnrow(i)
  wtrvpak(ioff+i) = wtrvrow(i)
  wvfpak (ioff+i) = wvfrow (i)
  wvlpak (ioff+i) = wvlrow (i)
  znpak  (ioff+i) = znrow  (i)
  gapak  (ioff+i) = garow  (i)
  hblpak (ioff+i) = hblrow (i)
  ilmopak(ioff+i) = ilmorow(i)
  petpak (ioff+i) = petrow (i)
  rofbpak(ioff+i) = rofbrow(i)
  rofspak(ioff+i) = rofsrow(i)
  uepak  (ioff+i) = uerow  (i)
  wsnopak(ioff+i) = wsnorow(i)
  wtabpak(ioff+i) = wtabrow(i)
  zpndpak(ioff+i) = zpndrow(i)
  refpak (ioff+i) = refrow (i)
  bcsnpak(ioff+i) = bcsnrow(i)
  depbpak(ioff+i) = depbrow(i)
end do
!
!     * accumulated 3h variables
!

cldt3hpak(ioff+il1:ioff+il2)=cldt3hrow(il1:il2)
hfl3hpak (ioff+il1:ioff+il2)=hfl3hrow (il1:il2)
hfs3hpak (ioff+il1:ioff+il2)=hfs3hrow (il1:il2)
rof3hpal (ioff+il1:ioff+il2)=rof3hrol (il1:il2)
wgl3hpak (ioff+il1:ioff+il2)=wgl3hrow (il1:il2)
wgf3hpak (ioff+il1:ioff+il2)=wgf3hrow (il1:il2)
pcp3hpak (ioff+il1:ioff+il2)=pcp3hrow (il1:il2)
pcpc3hpak(ioff+il1:ioff+il2)=pcpc3hrow(il1:il2)
pcpl3hpak(ioff+il1:ioff+il2)=pcpl3hrow(il1:il2)
pcps3hpak(ioff+il1:ioff+il2)=pcps3hrow(il1:il2)
pcpn3hpak(ioff+il1:ioff+il2)=pcpn3hrow(il1:il2)
fdl3hpak (ioff+il1:ioff+il2)=fdl3hrow (il1:il2)
fdlc3hpak(ioff+il1:ioff+il2)=fdlc3hrow(il1:il2)
flg3hpak (ioff+il1:ioff+il2)=flg3hrow (il1:il2)
fss3hpak (ioff+il1:ioff+il2)=fss3hrow (il1:il2)
fssc3hpak(ioff+il1:ioff+il2)=fssc3hrow(il1:il2)
fsd3hpak (ioff+il1:ioff+il2)=fsd3hrow (il1:il2)
fsg3hpak (ioff+il1:ioff+il2)=fsg3hrow (il1:il2)
fsgc3hpak(ioff+il1:ioff+il2)=fsgc3hrow(il1:il2)
sq3hpak  (ioff+il1:ioff+il2)=sq3hrow  (il1:il2)
st3hpak  (ioff+il1:ioff+il2)=st3hrow  (il1:il2)
su3hpak  (ioff+il1:ioff+il2)=su3hrow  (il1:il2)
sv3hpak  (ioff+il1:ioff+il2)=sv3hrow  (il1:il2)

olr3hpak(ioff+il1:ioff+il2)  = olr3hrow(il1:il2)
fsr3hpak(ioff+il1:ioff+il2)  = fsr3hrow(il1:il2)
olrc3hpak(ioff+il1:ioff+il2) = olrc3hrow(il1:il2)
fsrc3hpak(ioff+il1:ioff+il2) = fsrc3hrow(il1:il2)
fso3hpak(ioff+il1:ioff+il2)  = fso3hrow(il1:il2)
pwat3hpak(ioff+il1:ioff+il2) = pwat3hrow(il1:il2)
clwt3hpak(ioff+il1:ioff+il2) = clwt3hrow(il1:il2)
cict3hpak(ioff+il1:ioff+il2) = cict3hrow(il1:il2)
pmsl3hpak(ioff+il1:ioff+il2) = pmsl3hrow(il1:il2)

!
!  * instantaneous variables for sampling
!
do i=il1,il2
  swpal(ioff+i)      = swrol(i)
  srhpal(ioff+i)     = srhrol(i)
  pcppal(ioff+i)     = pcprol(i)
  pcpnpal(ioff+i)    = pcpnrol(i)
  pcpcpal(ioff+i)    = pcpcrol(i)
  qfsipal(ioff+i)    = qfsirol(i)
  qfnpal(ioff+i)     = qfnrol(i)
  qfvfpal(ioff+i)    = qfvfrol(i)
  ufsinstpal(ioff+i) = ufsinstrol(i)
  vfsinstpal(ioff+i) = vfsinstrol(i)
  hflipal(ioff+i)    = hflirol(i)
  hfsipal(ioff+i)    = hfsirol(i)
  fdlpal(ioff+i)     = fdlrol(i)
  flgpal(ioff+i)     = flgrol(i)
  fsspal(ioff+i)     = fssrol(i)
  fsgpal(ioff+i)     = fsgrol(i)
  fsscpal(ioff+i)    = fsscrol(i)
  fsgcpal(ioff+i)    = fsgcrol(i)
  fdlcpal(ioff+i)    = fdlcrol(i)
  fsopal(ioff+i)     = fsorol(i)
  fsrpal(ioff+i)     = fsrrol(i)
  olrpal(ioff+i)     = olrrol(i)
  olrcpal(ioff+i)    = olrcrol(i)
  fsrcpal(ioff+i)    = fsrcrol(i)
  pwatipal(ioff+i)   = pwatirol(i)
  cldtpal(ioff+i)    = cldtrol(i)
  clwtpal(ioff+i)    = clwtrol(i)
  cictpal(ioff+i)    = cictrol(i)
  baltpal(ioff+i)    = baltrol(i)
  pmslipal(ioff+i)   = pmslirol(i)
  cdcbpal(ioff+i)    = cdcbrol(i)
  cscbpal(ioff+i)    = cscbrol(i)
  gtpal(ioff+i)      = gtrol(i)
  tcdpal(ioff+i)     = tcdrol(i)
  bcdpal(ioff+i)     = bcdrol(i)
  pspal(ioff+i)      = psrol(i)
  stpal(ioff+i)      = strol(i)
  supal(ioff+i)      = surol(i)
  svpal(ioff+i)      = svrol(i)
end do ! i

do l = 1, ilev
  do i = il1, il2
    dmcpal(ioff+i,l)  = dmcrol(i,l)
    tapal(ioff+i,l)   = tarol(i,l)
    uapal(ioff+i,l)   = uarol(i,l)
    vapal(ioff+i,l)   = varol(i,l)
    qapal(ioff+i,l)   = qarol(i,l)
    rhpal(ioff+i,l)   = rhrol(i,l)
    zgpal(ioff+i,l)   = zgrol(i,l)
    rkhpal(ioff+i,l)  = rkhrol(i,l)
    rkmpal(ioff+i,l)  = rkmrol(i,l)
    rkqpal(ioff+i,l)  = rkqrol(i,l)

    ttppal(ioff+i,l)  = ttprol(i,l)
    ttpppal(ioff+i,l) = ttpprol(i,l)
    ttpvpal(ioff+i,l) = ttpvrol(i,l)
    ttplpal(ioff+i,l) = ttplrol(i,l)
    ttpspal(ioff+i,l) = ttpsrol(i,l)
    ttlcpal(ioff+i,l) = ttlcrol(i,l)
    ttscpal(ioff+i,l) = ttscrol(i,l)
    ttpcpal(ioff+i,l) = ttpcrol(i,l)
    ttpmpal(ioff+i,l) = ttpmrol(i,l)

    qtppal(ioff+i,l)  = qtprol(i,l)
    qtpppal(ioff+i,l) = qtpprol(i,l)
    qtpvpal(ioff+i,l) = qtpvrol(i,l)
    qtpcpal(ioff+i,l) = qtpcrol(i,l)
    qtpmpal(ioff+i,l) = qtpmrol(i,l)
  end do ! i
end do ! l
!
anpat(ioff+il1:ioff+il2,:) = anrot(il1:il2,:)
drnpat(ioff+il1:ioff+il2,:) = drnrot(il1:il2,:)
fnpat(ioff+il1:ioff+il2,:) = fnrot(il1:il2,:)
mvpat(ioff+il1:ioff+il2,:) = mvrot(il1:il2,:)
qavpat(ioff+il1:ioff+il2,:) = qavrot(il1:il2,:)
rhonpat(ioff+il1:ioff+il2,:) = rhonrot(il1:il2,:)
tavpat(ioff+il1:ioff+il2,:) = tavrot(il1:il2,:)
tbaspat(ioff+il1:ioff+il2,:) = tbasrot(il1:il2,:)
tnpat(ioff+il1:ioff+il2,:) = tnrot(il1:il2,:)
tpndpat(ioff+il1:ioff+il2,:) = tpndrot(il1:il2,:)
ttpat(ioff+il1:ioff+il2,:) = ttrot(il1:il2,:)
tvpat(ioff+il1:ioff+il2,:) = tvrot(il1:il2,:)
wsnopat(ioff+il1:ioff+il2,:) = wsnorot(il1:il2,:)
wvfpat(ioff+il1:ioff+il2,:) = wvfrot(il1:il2,:)
wvlpat(ioff+il1:ioff+il2,:) = wvlrot(il1:il2,:)
zpndpat(ioff+il1:ioff+il2,:) = zpndrot(il1:il2,:)
refpat (ioff+il1:ioff+il2,:) = refrot (il1:il2,:)
bcsnpat(ioff+il1:ioff+il2,:) = bcsnrot(il1:il2,:)
!
farepat(ioff+il1:ioff+il2,:) = farerot(il1:il2,:)
tsfspat(ioff+il1:ioff+il2,:,:)=tsfsrot(il1:il2,:,:)
!
if (kount==kstart) then
  dlzwpat(ioff+il1:ioff+il2,:,:)=dlzwrot(il1:il2,:,:)
  porgpat(ioff+il1:ioff+il2,:,:)=porgrot(il1:il2,:,:)
  thfcpat(ioff+il1:ioff+il2,:,:)=thfcrot(il1:il2,:,:)
  thlwpat(ioff+il1:ioff+il2,:,:)=thlwrot(il1:il2,:,:)
  thrpat (ioff+il1:ioff+il2,:,:)=thrrot (il1:il2,:,:)
  thmpat (ioff+il1:ioff+il2,:,:)=thmrot (il1:il2,:,:)
  bipat  (ioff+il1:ioff+il2,:,:)=birot  (il1:il2,:,:)
  psispat(ioff+il1:ioff+il2,:,:)=psisrot(il1:il2,:,:)
  grkspat(ioff+il1:ioff+il2,:,:)=grksrot(il1:il2,:,:)
  thrapat(ioff+il1:ioff+il2,:,:)=thrarot(il1:il2,:,:)
  hcpspat(ioff+il1:ioff+il2,:,:)=hcpsrot(il1:il2,:,:)
  tcspat (ioff+il1:ioff+il2,:,:)=tcsrot (il1:il2,:,:)
  psiwpat(ioff+il1:ioff+il2,:,:)=psiwrot(il1:il2,:,:)
  zbtwpat(ioff+il1:ioff+il2,:,:)=zbtwrot(il1:il2,:,:)
  isndpat(ioff+il1:ioff+il2,:,:)=isndrot(il1:il2,:,:)
  !
  algdvpat(ioff+il1:ioff+il2,:) =algdvrot(il1:il2,:)
  algdnpat(ioff+il1:ioff+il2,:) =algdnrot(il1:il2,:)
  algwvpat(ioff+il1:ioff+il2,:) =algwvrot(il1:il2,:)
  algwnpat(ioff+il1:ioff+il2,:) =algwnrot(il1:il2,:)
  igdrpat(ioff+il1:ioff+il2,:)  =igdrrot(il1:il2,:)
end if
!
! * TKE fields.
!
do l=1,ilev
  do i=il1,il2
    tkempak (ioff+i,l) = tkemrow (i,l)
    xlmpak  (ioff+i,l) = xlmrow  (i,l)
    svarpak (ioff+i,l) = svarrow (i,l)
  enddo
enddo
#if defined (agcm_ctem)

      rmatcpat  (ioff+il1:ioff+il2,:,:,:) = rmatcrot  (il1:il2,:,:,:)
      rtctmpat  (ioff+il1:ioff+il2,:,:,:) = rtctmrot  (il1:il2,:,:,:)

      cfcanpat  (ioff+il1:ioff+il2,:,:) = cfcanrot  (il1:il2,:,:)
      zolncpat  (ioff+il1:ioff+il2,:,:) = zolncrot  (il1:il2,:,:)
      ailcpat   (ioff+il1:ioff+il2,:,:) = ailcrot   (il1:il2,:,:)
      cmascpat  (ioff+il1:ioff+il2,:,:) = cmascrot  (il1:il2,:,:)
      calvcpat  (ioff+il1:ioff+il2,:,:) = calvcrot  (il1:il2,:,:)
      calicpat  (ioff+il1:ioff+il2,:,:) = calicrot  (il1:il2,:,:)
      paicpat   (ioff+il1:ioff+il2,:,:) = paicrot   (il1:il2,:,:)
      slaicpat  (ioff+il1:ioff+il2,:,:) = slaicrot  (il1:il2,:,:)
      fcancpat  (ioff+il1:ioff+il2,:,:) = fcancrot  (il1:il2,:,:)
      todfcpat  (ioff+il1:ioff+il2,:,:) = todfcrot  (il1:il2,:,:)
      ailcgpat  (ioff+il1:ioff+il2,:,:) = ailcgrot  (il1:il2,:,:)
      slaipat   (ioff+il1:ioff+il2,:,:) = slairot   (il1:il2,:,:)
      fsnowptl  (ioff+il1:ioff+il2,:)   = fsnowrtl  (il1:il2,:)
      tcanoptl  (ioff+il1:ioff+il2,:)   = tcanortl  (il1:il2,:)
      tcansptl  (ioff+il1:ioff+il2,:)   = tcansrtl  (il1:il2,:)
      taptl     (ioff+il1:ioff+il2,:)   = tartl     (il1:il2,:)
      cfluxcspat(ioff+il1:ioff+il2,:)   = cfluxcsrot(il1:il2,:)
      cfluxcgpat(ioff+il1:ioff+il2,:)   = cfluxcgrot(il1:il2,:)

      co2cg1pat (ioff+il1:ioff+il2,:,:) = co2cg1rot (il1:il2,:,:)
      co2cg2pat (ioff+il1:ioff+il2,:,:) = co2cg2rot (il1:il2,:,:)
      co2cs1pat (ioff+il1:ioff+il2,:,:) = co2cs1rot (il1:il2,:,:)
      co2cs2pat (ioff+il1:ioff+il2,:,:) = co2cs2rot (il1:il2,:,:)
      ancgptl   (ioff+il1:ioff+il2,:,:) = ancgrtl   (il1:il2,:,:)
      ancsptl   (ioff+il1:ioff+il2,:,:) = ancsrtl   (il1:il2,:,:)
      rmlcgptl  (ioff+il1:ioff+il2,:,:) = rmlcgrtl  (il1:il2,:,:)
      rmlcsptl  (ioff+il1:ioff+il2,:,:) = rmlcsrtl  (il1:il2,:,:)

      tbarptl   (ioff+il1:ioff+il2,:,:) = tbarrtl   (il1:il2,:,:)
      tbarcptl  (ioff+il1:ioff+il2,:,:) = tbarcrtl  (il1:il2,:,:)
      tbarcsptl (ioff+il1:ioff+il2,:,:) = tbarcsrtl (il1:il2,:,:)
      tbargptl  (ioff+il1:ioff+il2,:,:) = tbargrtl  (il1:il2,:,:)
      tbargsptl (ioff+il1:ioff+il2,:,:) = tbargsrtl (il1:il2,:,:)
      thliqcptl (ioff+il1:ioff+il2,:,:) = thliqcrtl (il1:il2,:,:)
      thliqgptl (ioff+il1:ioff+il2,:,:) = thliqgrtl (il1:il2,:,:)
      thicecptl (ioff+il1:ioff+il2,:,:) = thicecrtl (il1:il2,:,:)
!
!     * time varying.
!
      soilcpat  (ioff+il1:ioff+il2,:,:)   = soilcrot  (il1:il2,:,:)
      litrcpat  (ioff+il1:ioff+il2,:,:)   = litrcrot  (il1:il2,:,:)
      rootcpat  (ioff+il1:ioff+il2,:,:)   = rootcrot  (il1:il2,:,:)
      stemcpat  (ioff+il1:ioff+il2,:,:)   = stemcrot  (il1:il2,:,:)
      gleafcpat (ioff+il1:ioff+il2,:,:)   = gleafcrot (il1:il2,:,:)
      bleafcpat (ioff+il1:ioff+il2,:,:)   = bleafcrot (il1:il2,:,:)
      fallhpat  (ioff+il1:ioff+il2,:,:)   = fallhrot  (il1:il2,:,:)
      posphpat  (ioff+il1:ioff+il2,:,:)   = posphrot  (il1:il2,:,:)
      leafspat  (ioff+il1:ioff+il2,:,:)   = leafsrot  (il1:il2,:,:)
      growtpat  (ioff+il1:ioff+il2,:,:)   = growtrot  (il1:il2,:,:)
      lastrpat  (ioff+il1:ioff+il2,:,:)   = lastrrot  (il1:il2,:,:)
      lastspat  (ioff+il1:ioff+il2,:,:)   = lastsrot  (il1:il2,:,:)
      thisylpat (ioff+il1:ioff+il2,:,:)   = thisylrot (il1:il2,:,:)
      stemhpat  (ioff+il1:ioff+il2,:,:)   = stemhrot  (il1:il2,:,:)
      roothpat  (ioff+il1:ioff+il2,:,:)   = roothrot  (il1:il2,:,:)
      tempcpat  (ioff+il1:ioff+il2,:,:)   = tempcrot  (il1:il2,:,:)
      ailcbpat  (ioff+il1:ioff+il2,:,:)   = ailcbrot  (il1:il2,:,:)
      bmasvpat  (ioff+il1:ioff+il2,:,:)   = bmasvrot  (il1:il2,:,:)
      veghpat   (ioff+il1:ioff+il2,:,:)   = veghrot   (il1:il2,:,:)
      rootdpat  (ioff+il1:ioff+il2,:,:)   = rootdrot  (il1:il2,:,:)
!
      prefpat   (ioff+il1:ioff+il2,:,:)   = prefrot   (il1:il2,:,:)
      newfpat   (ioff+il1:ioff+il2,:,:)   = newfrot   (il1:il2,:,:)
!
      cvegpat   (ioff+il1:ioff+il2,:)     = cvegrot   (il1:il2,:)
      cdebpat   (ioff+il1:ioff+il2,:)     = cdebrot   (il1:il2,:)
      chumpat   (ioff+il1:ioff+il2,:)     = chumrot   (il1:il2,:)
      fcolpat   (ioff+il1:ioff+il2,:)     = fcolrot   (il1:il2,:)
!
!     * ctem diagnostic output fields.
!
      cvegpak(ioff+il1:ioff+il2) = cvegrow(il1:il2)
      cdebpak(ioff+il1:ioff+il2) = cdebrow(il1:il2)
      chumpak(ioff+il1:ioff+il2) = chumrow(il1:il2)
      claipak(ioff+il1:ioff+il2) = clairow(il1:il2)
      cfnppak(ioff+il1:ioff+il2) = cfnprow(il1:il2)
      cfnepak(ioff+il1:ioff+il2) = cfnerow(il1:il2)
      cfrvpak(ioff+il1:ioff+il2) = cfrvrow(il1:il2)
      cfgppak(ioff+il1:ioff+il2) = cfgprow(il1:il2)
      cfnbpak(ioff+il1:ioff+il2) = cfnbrow(il1:il2)
      cffvpak(ioff+il1:ioff+il2) = cffvrow(il1:il2)
      cffdpak(ioff+il1:ioff+il2) = cffdrow(il1:il2)
      cflvpak(ioff+il1:ioff+il2) = cflvrow(il1:il2)
      cfldpak(ioff+il1:ioff+il2) = cfldrow(il1:il2)
      cflhpak(ioff+il1:ioff+il2) = cflhrow(il1:il2)
      cbrnpak(ioff+il1:ioff+il2) = cbrnrow(il1:il2)
      cfrhpak(ioff+il1:ioff+il2) = cfrhrow(il1:il2)
      cfhtpak(ioff+il1:ioff+il2) = cfhtrow(il1:il2)
      cflfpak(ioff+il1:ioff+il2) = cflfrow(il1:il2)
      cfrdpak(ioff+il1:ioff+il2) = cfrdrow(il1:il2)
      cfrgpak(ioff+il1:ioff+il2) = cfrgrow(il1:il2)
      cfrmpak(ioff+il1:ioff+il2) = cfrmrow(il1:il2)
      cvglpak(ioff+il1:ioff+il2) = cvglrow(il1:il2)
      cvgspak(ioff+il1:ioff+il2) = cvgsrow(il1:il2)
      cvgrpak(ioff+il1:ioff+il2) = cvgrrow(il1:il2)
      cfnlpak(ioff+il1:ioff+il2) = cfnlrow(il1:il2)
      cfnspak(ioff+il1:ioff+il2) = cfnsrow(il1:il2)
      cfnrpak(ioff+il1:ioff+il2) = cfnrrow(il1:il2)
      ch4hpak(ioff+il1:ioff+il2) = ch4hrow(il1:il2)
      ch4npak(ioff+il1:ioff+il2) = ch4nrow(il1:il2)
      wfrapak(ioff+il1:ioff+il2) = wfrarow(il1:il2)
      cw1dpak(ioff+il1:ioff+il2) = cw1drow(il1:il2)
      cw2dpak(ioff+il1:ioff+il2) = cw2drow(il1:il2)
      fcolpak(ioff+il1:ioff+il2) = fcolrow(il1:il2)
      curfpak(ioff+il1:ioff+il2,:) = curfrow(il1:il2,:)

!     additional ctem related cmip6 output
      brfrpak(ioff+il1:ioff+il2) = brfrrow(il1:il2)   ! baresoilfrac
      c3crpak(ioff+il1:ioff+il2) = c3crrow(il1:il2)   ! cropfracc3
      c4crpak(ioff+il1:ioff+il2) = c4crrow(il1:il2)   ! cropfracc4
      crpfpak(ioff+il1:ioff+il2) = crpfrow(il1:il2)   ! cropfrac
      c3grpak(ioff+il1:ioff+il2) = c3grrow(il1:il2)   ! grassfracc3
      c4grpak(ioff+il1:ioff+il2) = c4grrow(il1:il2)   ! grassfracc4
      grsfpak(ioff+il1:ioff+il2) = grsfrow(il1:il2)   ! grassfrac
      bdtfpak(ioff+il1:ioff+il2) = bdtfrow(il1:il2)   ! treefracbdldcd
      betfpak(ioff+il1:ioff+il2) = betfrow(il1:il2)   ! treefracbdlevg
      ndtfpak(ioff+il1:ioff+il2) = ndtfrow(il1:il2)   ! treefracndldcd
      netfpak(ioff+il1:ioff+il2) = netfrow(il1:il2)   ! treefracndlevg
      treepak(ioff+il1:ioff+il2) = treerow(il1:il2)   ! treefrac
      vegfpak(ioff+il1:ioff+il2) = vegfrow(il1:il2)   ! vegfrac
      c3pfpak(ioff+il1:ioff+il2) = c3pfrow(il1:il2)   ! c3pftfrac
      c4pfpak(ioff+il1:ioff+il2) = c4pfrow(il1:il2)   ! c4pftfrac

      clndpak(ioff+il1:ioff+il2) = clndrow(il1:il2)   ! cland

!
! instantaneous output (used for subdaily output)
!
      cfgppal(ioff+il1:ioff+il2) = cfgprol(il1:il2)
      cfrvpal(ioff+il1:ioff+il2) = cfrvrol(il1:il2)
      cfrhpal(ioff+il1:ioff+il2) = cfrhrol(il1:il2)
      cfrdpal(ioff+il1:ioff+il2) = cfrdrol(il1:il2)

#endif
#if defined explvol
!
!     * explosive volcano fields.
!
      vtaupak(ioff+il1:ioff+il2) = vtaurow(il1:il2)
      vtaupal(ioff+il1:ioff+il2) = vtaurol(il1:il2)
      troppak(ioff+il1:ioff+il2) = troprow(il1:il2)
      troppal(ioff+il1:ioff+il2) = troprol(il1:il2)

      do ib = 1, nbs
         do l = 1, levsa
	    sw_ext_sa_pak(ioffz+il1z,l,ib) = sw_ext_sa_row(il1,l,ib)
	    sw_ext_sa_pal(ioffz+il1z,l,ib) = sw_ext_sa_rol(il1,l,ib)
	    sw_ssa_sa_pak(ioffz+il1z,l,ib) = sw_ssa_sa_row(il1,l,ib)
	    sw_ssa_sa_pal(ioffz+il1z,l,ib) = sw_ssa_sa_rol(il1,l,ib)
	    sw_g_sa_pak(ioffz+il1z,l,ib)   = sw_g_sa_row(il1,l,ib)
	    sw_g_sa_pal(ioffz+il1z,l,ib)   = sw_g_sa_rol(il1,l,ib)
         end do ! l
      end do ! ib

      do ib = 1, nbl
         do l = 1, levsa
	    lw_ext_sa_pak(ioffz+il1z,l,ib) = lw_ext_sa_row(il1,l,ib)
	    lw_ext_sa_pal(ioffz+il1z,l,ib) = lw_ext_sa_rol(il1,l,ib)
	    lw_ssa_sa_pak(ioffz+il1z,l,ib) = lw_ssa_sa_row(il1,l,ib)
	    lw_ssa_sa_pal(ioffz+il1z,l,ib) = lw_ssa_sa_rol(il1,l,ib)
	    lw_g_sa_pak(ioffz+il1z,l,ib)   = lw_g_sa_row(il1,l,ib)
	    lw_g_sa_pal(ioffz+il1z,l,ib)   = lw_g_sa_rol(il1,l,ib)
         end do ! l
      end do ! ib

      do l = 1, levsa
	 w055_ext_sa_pak(ioffz+il1z,l) = w055_ext_sa_row(il1,l)
	 w110_ext_sa_pak(ioffz+il1z,l) = w110_ext_sa_row(il1,l)
	 w055_ext_sa_pal(ioffz+il1z,l) = w055_ext_sa_rol(il1,l)
	 w110_ext_sa_pal(ioffz+il1z,l) = w110_ext_sa_rol(il1,l)
	 pressure_sa_pak(ioffz+il1z,l) = pressure_sa_row(il1,l)
	 pressure_sa_pal(ioffz+il1z,l) = pressure_sa_rol(il1,l)
      end do ! l

      do i = il1, il2
         w055_vtau_sa_pak(ioff+i) = w055_vtau_sa_row(i)
         w055_vtau_sa_pal(ioff+i) = w055_vtau_sa_rol(i)
         w110_vtau_sa_pak(ioff+i) = w110_vtau_sa_row(i)
         w110_vtau_sa_pal(ioff+i) = w110_vtau_sa_rol(i)
      end do ! i

      do l = 1, ilev
         do i = il1, il2
            w055_ext_gcm_sa_pak(ioff+i,l) = w055_ext_gcm_sa_row(i,l)
            w055_ext_gcm_sa_pal(ioff+i,l) = w055_ext_gcm_sa_rol(i,l)
            w110_ext_gcm_sa_pak(ioff+i,l) = w110_ext_gcm_sa_row(i,l)
            w110_ext_gcm_sa_pal(ioff+i,l) = w110_ext_gcm_sa_rol(i,l)
         end do ! i
      end do ! l

#endif
!
!     * lakes fields.
!
do i=il1,il2
  ldmxpak(ioff+i) = ldmxrow(i)
  lrimpak(ioff+i) = lrimrow(i)
  lrinpak(ioff+i) = lrinrow(i)
  luimpak(ioff+i) = luimrow(i)
  luinpak(ioff+i) = luinrow(i)
  lzicpak(ioff+i) = lzicrow(i)
#if defined (cslm)
        delupak(ioff+i) = delurow(i)
        dtmppak(ioff+i) = dtmprow(i)
        expwpak(ioff+i) = expwrow(i)
        gredpak(ioff+i) = gredrow(i)
        rhompak(ioff+i) = rhomrow(i)
        t0lkpak(ioff+i) = t0lkrow(i)
        tkelpak(ioff+i) = tkelrow(i)
        flglpak(ioff+i) = flglrow(i)
        fnlpak (ioff+i) = fnlrow (i)
        fsglpak(ioff+i) = fsglrow(i)
        hfclpak(ioff+i) = hfclrow(i)
        hfllpak(ioff+i) = hfllrow(i)
        hfslpak(ioff+i) = hfslrow(i)
        hmflpak(ioff+i) = hmflrow(i)
        pilpak (ioff+i) = pilrow (i)
        qflpak (ioff+i) = qflrow (i)
!
        do l=1,nlklm
          tlakpak(ioff+i,l) = tlakrow(i,l)
        end do
#endif
#if defined (flake)
        lshppak(ioff+i) = lshprow(i)
        ltavpak(ioff+i) = ltavrow(i)
        lticpak(ioff+i) = lticrow(i)
        ltmxpak(ioff+i) = ltmxrow(i)
        ltsnpak(ioff+i) = ltsnrow(i)
        ltwbpak(ioff+i) = ltwbrow(i)
        lzsnpak(ioff+i) = lzsnrow(i)
#endif
end do
!
!     * emission fields.
!
do i=il1,il2
  eostpak(ioff+i) = eostrow(i)
  eostpal(ioff+i) = eostrol(i)
  !
  escvpak(ioff+i) = escvrow(i)
  ehcvpak(ioff+i) = ehcvrow(i)
  esevpak(ioff+i) = esevrow(i)
  ehevpak(ioff+i) = ehevrow(i)
end do
#if defined transient_aerosol_emissions
      do l=1,levwf
      do i=il1,il2
        fbbcpak(ioff+i,l) = fbbcrow(i,l)
        fbbcpal(ioff+i,l) = fbbcrol(i,l)
      end do
      end do
      do l=1,levair
      do i=il1,il2
        fairpak(ioff+i,l) = fairrow(i,l)
        fairpal(ioff+i,l) = fairrol(i,l)
      end do
      end do
#if defined emists
      do i=il1,il2
        sairpak(ioff+i) = sairrow(i)
        ssfcpak(ioff+i) = ssfcrow(i)
        sbiopak(ioff+i) = sbiorow(i)
        sshipak(ioff+i) = sshirow(i)
        sstkpak(ioff+i) = sstkrow(i)
        sfirpak(ioff+i) = sfirrow(i)
        sairpal(ioff+i) = sairrol(i)
        ssfcpal(ioff+i) = ssfcrol(i)
        sbiopal(ioff+i) = sbiorol(i)
        sshipal(ioff+i) = sshirol(i)
        sstkpal(ioff+i) = sstkrol(i)
        sfirpal(ioff+i) = sfirrol(i)
        oairpak(ioff+i) = oairrow(i)
        osfcpak(ioff+i) = osfcrow(i)
        obiopak(ioff+i) = obiorow(i)
        oshipak(ioff+i) = oshirow(i)
        ostkpak(ioff+i) = ostkrow(i)
        ofirpak(ioff+i) = ofirrow(i)
        oairpal(ioff+i) = oairrol(i)
        osfcpal(ioff+i) = osfcrol(i)
        obiopal(ioff+i) = obiorol(i)
        oshipal(ioff+i) = oshirol(i)
        ostkpal(ioff+i) = ostkrol(i)
        ofirpal(ioff+i) = ofirrol(i)
        bairpak(ioff+i) = bairrow(i)
        bsfcpak(ioff+i) = bsfcrow(i)
        bbiopak(ioff+i) = bbiorow(i)
        bshipak(ioff+i) = bshirow(i)
        bstkpak(ioff+i) = bstkrow(i)
        bfirpak(ioff+i) = bfirrow(i)
        bairpal(ioff+i) = bairrol(i)
        bsfcpal(ioff+i) = bsfcrol(i)
        bbiopal(ioff+i) = bbiorol(i)
        bshipal(ioff+i) = bshirol(i)
        bstkpal(ioff+i) = bstkrol(i)
        bfirpal(ioff+i) = bfirrol(i)
      end do
#endif
#endif
#if (defined(pla) && defined(pfrc))
      do i=il1,il2
        bcdpfpak(ioff+i) = bcdpfrow(i)
        bcdpfpal(ioff+i) = bcdpfrol(i)
      end do
      do l=1,ilev
      do i=il1,il2
        amldfpak(ioff+i,l) = amldfrow(i,l)
        amldfpal(ioff+i,l) = amldfrol(i,l)
        reamfpak(ioff+i,l) = reamfrow(i,l)
        reamfpal(ioff+i,l) = reamfrol(i,l)
        veamfpak(ioff+i,l) = veamfrow(i,l)
        veamfpal(ioff+i,l) = veamfrol(i,l)
        fr1fpak (ioff+i,l) = fr1frow (i,l)
        fr1fpal (ioff+i,l) = fr1frol (i,l)
        fr2fpak (ioff+i,l) = fr2frow (i,l)
        fr2fpal (ioff+i,l) = fr2frol (i,l)
        ssldfpak(ioff+i,l) = ssldfrow(i,l)
        ssldfpal(ioff+i,l) = ssldfrol(i,l)
        ressfpak(ioff+i,l) = ressfrow(i,l)
        ressfpal(ioff+i,l) = ressfrol(i,l)
        vessfpak(ioff+i,l) = vessfrow(i,l)
        vessfpal(ioff+i,l) = vessfrol(i,l)
        dsldfpak(ioff+i,l) = dsldfrow(i,l)
        dsldfpal(ioff+i,l) = dsldfrol(i,l)
        redsfpak(ioff+i,l) = redsfrow(i,l)
        redsfpal(ioff+i,l) = redsfrol(i,l)
        vedsfpak(ioff+i,l) = vedsfrow(i,l)
        vedsfpal(ioff+i,l) = vedsfrol(i,l)
        bcldfpak(ioff+i,l) = bcldfrow(i,l)
        bcldfpal(ioff+i,l) = bcldfrol(i,l)
        rebcfpak(ioff+i,l) = rebcfrow(i,l)
        rebcfpal(ioff+i,l) = rebcfrol(i,l)
        vebcfpak(ioff+i,l) = vebcfrow(i,l)
        vebcfpal(ioff+i,l) = vebcfrol(i,l)
        ocldfpak(ioff+i,l) = ocldfrow(i,l)
        ocldfpal(ioff+i,l) = ocldfrol(i,l)
        reocfpak(ioff+i,l) = reocfrow(i,l)
        reocfpal(ioff+i,l) = reocfrol(i,l)
        veocfpak(ioff+i,l) = veocfrow(i,l)
        veocfpal(ioff+i,l) = veocfrol(i,l)
        zcdnfpak(ioff+i,l) = zcdnfrow(i,l)
        zcdnfpal(ioff+i,l) = zcdnfrol(i,l)
        bcicfpak(ioff+i,l) = bcicfrow(i,l)
        bcicfpal(ioff+i,l) = bcicfrol(i,l)
      end do
      end do
#endif
!
!     * tracer arrays.
!
do n=1,ntrac
  do i=il1,il2
    xfspak (ioff+i,n) = xfsrow (i,n)
    xsfxpak(ioff+i,n) = xsfxrow(i,n)
    xsfxpal(ioff+i,n) = xsfxrol(i,n)
    xsrfpak(ioff+i,n) = xsrfrow(i,n)
    xsrfpal(ioff+i,n) = xsrfrol(i,n)

    xtpfpam(ioff+i,n) = xtpfrom(i,n)
    xtphpam(ioff+i,n) = xtphrom(i,n)
    xtvipas(ioff+i,n) = xtviros(i,n)
    xtvipak(ioff+i,n) = xtvirow(i,n)
    xtvipam(ioff+i,n) = xtvirom(i,n)
    xtptpam(ioff+i,n) = xtptrom(i,n)
  end do
  do l=1,ilev
    do i=il1,il2
      xwf0pal(ioff+i,l,n) = xwf0rol(i,l,n)
      xwfmpal(ioff+i,l,n) = xwfmrol(i,l,n)
    end do
  end do
end do
!
do l=1,levox
  do i=il1,il2
    h2o2pak(ioff+i,l) = h2o2row(i,l)
    h2o2pal(ioff+i,l) = h2o2rol(i,l)
    hno3pak(ioff+i,l) = hno3row(i,l)
    hno3pal(ioff+i,l) = hno3rol(i,l)
    nh3pak (ioff+i,l) = nh3row (i,l)
    nh3pal (ioff+i,l) = nh3rol (i,l)
    nh4pak (ioff+i,l) = nh4row (i,l)
    nh4pal (ioff+i,l) = nh4rol (i,l)
    no3pak (ioff+i,l) = no3row (i,l)
    no3pal (ioff+i,l) = no3rol (i,l)
    o3pak  (ioff+i,l) = o3row  (i,l)
    o3pal  (ioff+i,l) = o3rol  (i,l)
    ohpak  (ioff+i,l) = ohrow  (i,l)
    ohpal  (ioff+i,l) = ohrol  (i,l)
  end do
end do
do l=1,ilev
  do i=il1,il2
    tbndpak(ioff+i,l) = tbndrow(i,l)
    tbndpal(ioff+i,l) = tbndrol(i,l)
    ea55pak(ioff+i,l) = ea55row(i,l)
    ea55pal(ioff+i,l) = ea55rol(i,l)
    rkmpak (ioff+i,l) = rkmrow (i,l)
    rkhpak (ioff+i,l) = rkhrow (i,l)
    rkqpak (ioff+i,l) = rkqrow (i,l)
  end do
end do
!
do i=il1,il2
  spotpak(ioff+i) = spotrow(i)
  st01pak(ioff+i) = st01row(i)
  st02pak(ioff+i) = st02row(i)
  st03pak(ioff+i) = st03row(i)
  st04pak(ioff+i) = st04row(i)
  st06pak(ioff+i) = st06row(i)
  st13pak(ioff+i) = st13row(i)
  st14pak(ioff+i) = st14row(i)
  st15pak(ioff+i) = st15row(i)
  st16pak(ioff+i) = st16row(i)
  st17pak(ioff+i) = st17row(i)
  suz0pak(ioff+i) = suz0row(i)
  suz0pal(ioff+i) = suz0rol(i)
  pdsfpak(ioff+i) = pdsfrow(i)
  pdsfpal(ioff+i) = pdsfrol(i)
end do
!
!     * conditional arrays.
!
#if defined rad_flux_profs
      do l = 1, ilev+2
         do i = il1, il2
            fsaupak(ioff+i,l) = fsaurow(i,l)
            fsadpak(ioff+i,l) = fsadrow(i,l)
            flaupak(ioff+i,l) = flaurow(i,l)
            fladpak(ioff+i,l) = fladrow(i,l)
            fscupak(ioff+i,l) = fscurow(i,l)
            fscdpak(ioff+i,l) = fscdrow(i,l)
            flcupak(ioff+i,l) = flcurow(i,l)
            flcdpak(ioff+i,l) = flcdrow(i,l)
         end do ! i
      end do ! l
#endif
do l = 1, ilev+2
  do i = il1, il2
    fsaupal(ioff+i,l) = fsaurol(i,l)
    fsadpal(ioff+i,l) = fsadrol(i,l)
    flaupal(ioff+i,l) = flaurol(i,l)
    fladpal(ioff+i,l) = fladrol(i,l)
    fscupal(ioff+i,l) = fscurol(i,l)
    fscdpal(ioff+i,l) = fscdrol(i,l)
    flcupal(ioff+i,l) = flcurol(i,l)
    flcdpal(ioff+i,l) = flcdrol(i,l)
  end do ! i
end do ! l


#if defined radforce

      !--- upward and downward all-sky and clear-sky fluxes
      !--- fl* thermal, fs* solar

      fsaupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fsaurow_r (il1:il2,1:ilev+2,1:nrfp)
      fsadpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fsadrow_r (il1:il2,1:ilev+2,1:nrfp)
      flaupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          flaurow_r (il1:il2,1:ilev+2,1:nrfp)
      fladpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fladrow_r (il1:il2,1:ilev+2,1:nrfp)
      fscupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fscurow_r (il1:il2,1:ilev+2,1:nrfp)
      fscdpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fscdrow_r (il1:il2,1:ilev+2,1:nrfp)
      flcupak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          flcurow_r (il1:il2,1:ilev+2,1:nrfp)
      flcdpak_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          flcdrow_r (il1:il2,1:ilev+2,1:nrfp)

      fsaupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fsaurol_r (il1:il2,1:ilev+2,1:nrfp)
      fsadpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fsadrol_r (il1:il2,1:ilev+2,1:nrfp)
      flaupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          flaurol_r (il1:il2,1:ilev+2,1:nrfp)
      fladpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fladrol_r (il1:il2,1:ilev+2,1:nrfp)
      fscupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fscurol_r (il1:il2,1:ilev+2,1:nrfp)
      fscdpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          fscdrol_r (il1:il2,1:ilev+2,1:nrfp)
      flcupal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          flcurol_r (il1:il2,1:ilev+2,1:nrfp)
      flcdpal_r(ioff+il1:ioff+il2,1:ilev+2,1:nrfp) = &
          flcdrol_r (il1:il2,1:ilev+2,1:nrfp)

      !--- temperature perturbation (t_* - t_o) for each perturbation
      rdtpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           rdtrow_r (il1:il2,1:ilev,1:nrfp)
      rdtpal_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           rdtrol_r (il1:il2,1:ilev,1:nrfp)
      rdtpam_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           rdtrom_r (il1:il2,1:ilev,1:nrfp)

      !--- heating rates for each perturbation
      hrspak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           hrsrow_r (il1:il2,1:ilev,1:nrfp)
      hrlpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           hrlrow_r (il1:il2,1:ilev,1:nrfp)

      hrscpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           hrscrow_r (il1:il2,1:ilev,1:nrfp)
      hrlcpak_r (ioff+il1:ioff+il2,1:ilev,1:nrfp) = &
           hrlcrow_r (il1:il2,1:ilev,1:nrfp)

      !--- surface fluxes from secondary radiation calls
      csbpal_r(ioff+il1:ioff+il2,1:nrfp)  = csbrol_r(il1:il2,1:nrfp)
      fsgpal_r(ioff+il1:ioff+il2,1:nrfp)  = fsgrol_r(il1:il2,1:nrfp)
      fsspal_r(ioff+il1:ioff+il2,1:nrfp)  = fssrol_r(il1:il2,1:nrfp)
      fsscpal_r(ioff+il1:ioff+il2,1:nrfp) = fsscrol_r(il1:il2,1:nrfp)
      clbpal_r(ioff+il1:ioff+il2,1:nrfp)  = clbrol_r(il1:il2,1:nrfp)
      flgpal_r(ioff+il1:ioff+il2,1:nrfp)  = flgrol_r(il1:il2,1:nrfp)
      fdlpal_r(ioff+il1:ioff+il2,1:nrfp)  = fdlrol_r(il1:il2,1:nrfp)
      fdlcpal_r(ioff+il1:ioff+il2,1:nrfp) = fdlcrol_r(il1:il2,1:nrfp)
      fsrpal_r(ioff+il1:ioff+il2,1:nrfp)  = fsrrol_r(il1:il2,1:nrfp)
      fsrcpal_r(ioff+il1:ioff+il2,1:nrfp) = fsrcrol_r(il1:il2,1:nrfp)
      fsopal_r(ioff+il1:ioff+il2,1:nrfp)  = fsorol_r(il1:il2,1:nrfp)
      olrpal_r(ioff+il1:ioff+il2,1:nrfp)  = olrrol_r(il1:il2,1:nrfp)
      olrcpal_r(ioff+il1:ioff+il2,1:nrfp) = olrcrol_r(il1:il2,1:nrfp)
      fslopal_r(ioff+il1:ioff+il2,1:nrfp) = fslorol_r(il1:il2,1:nrfp)

      do m = 1, im
       csbpat_r(ioff+il1:ioff+il2,m,1:nrfp) = csbrot_r(il1:il2,m,1:nrfp)
       clbpat_r(ioff+il1:ioff+il2,m,1:nrfp) = clbrot_r(il1:il2,m,1:nrfp)
       fsgpat_r(ioff+il1:ioff+il2,m,1:nrfp) = fsgrot_r(il1:il2,m,1:nrfp)
       flgpat_r(ioff+il1:ioff+il2,m,1:nrfp) = flgrot_r(il1:il2,m,1:nrfp)
      end do ! m

      !--- vertical level index of tropopause height
      kthpal(ioff+il1:ioff+il2) = kthrol(il1:il2)
#endif
#if defined qconsav
      do i=il1,il2
          qaddpak(ioff+i,1) = qaddrow(i,1)
          qaddpak(ioff+i,2) = qaddrow(i,2)
          qtphpak(ioff+i,1) = qtphrow(i,1)
          qtphpak(ioff+i,2) = qtphrow(i,2)
      end do
#endif
!
do l=1,ilev
  do i=il1,il2
#if defined tprhs
          ttppak (ioff+i,l) = ttprow (i,l)
#endif
#if defined qprhs
          qtppak (ioff+i,l) = qtprow (i,l)
#endif
#if defined uprhs
          utppak (ioff+i,l) = utprow (i,l)
#endif
#if defined vprhs
          vtppak (ioff+i,l) = vtprow (i,l)
#endif
#if defined qconsav
          qtpfpak(ioff+i,l) = qtpfrow(i,l)
#endif
#if defined tprhsc
          ttpcpak(ioff+i,l) = ttpcrow(i,l)
          ttpkpak(ioff+i,l) = ttpkrow(i,l)
          ttpmpak(ioff+i,l) = ttpmrow(i,l)
          ttpnpak(ioff+i,l) = ttpnrow(i,l)
          ttpppak(ioff+i,l) = ttpprow(i,l)
          ttpvpak(ioff+i,l) = ttpvrow(i,l)
#endif
#if (defined (tprhsc) || defined (radforce))
          ttplpak(ioff+i,l) = ttplrow(i,l)
          ttpspak(ioff+i,l) = ttpsrow(i,l)
          ttscpak(ioff+i,l) = ttscrow(i,l)
          ttlcpak(ioff+i,l) = ttlcrow(i,l)
#endif
#if defined qprhsc
          qtpcpak(ioff+i,l) = qtpcrow(i,l)
          qtpmpak(ioff+i,l) = qtpmrow(i,l)
          qtpppak(ioff+i,l) = qtpprow(i,l)
          qtpvpak(ioff+i,l) = qtpvrow(i,l)
#endif
#if defined uprhsc
          utpcpak(ioff+i,l) = utpcrow(i,l)
          utpgpak(ioff+i,l) = utpgrow(i,l)
          utpnpak(ioff+i,l) = utpnrow(i,l)
          utpspak(ioff+i,l) = utpsrow(i,l)
          utpvpak(ioff+i,l) = utpvrow(i,l)
#endif
#if defined vprhsc
          vtpcpak(ioff+i,l) = vtpcrow(i,l)
          vtpgpak(ioff+i,l) = vtpgrow(i,l)
          vtpnpak(ioff+i,l) = vtpnrow(i,l)
          vtpspak(ioff+i,l) = vtpsrow(i,l)
          vtpvpak(ioff+i,l) = vtpvrow(i,l)
#endif
  end do
end do
!
do n=1,ntrac
#if defined xconsav
        do i=il1,il2
            xaddpak(ioff+i,1,n) = xaddrow(i,1,n)
            xaddpak(ioff+i,2,n) = xaddrow(i,2,n)
            xtphpak(ioff+i,1,n) = xtphrow(i,1,n)
            xtphpak(ioff+i,2,n) = xtphrow(i,2,n)
        end do
#endif
  do l=1,ilev
    do i=il1,il2
#if defined xprhs
            xtppak (ioff+i,l,n) = xtprow (i,l,n)
#endif
#if defined xconsav
            xtpfpak(ioff+i,l,n) = xtpfrow(i,l,n)
#endif
#if defined xprhsc
            xtpcpak(ioff+i,l,n) = xtpcrow(i,l,n)
            xtpmpak(ioff+i,l,n) = xtpmrow(i,l,n)
            xtpppak(ioff+i,l,n) = xtpprow(i,l,n)
            xtpvpak(ioff+i,l,n) = xtpvrow(i,l,n)
#endif
#if defined x01
            x01pak (ioff+i,l,n) = x01row (i,l,n)
#endif
#if defined x02
            x02pak (ioff+i,l,n) = x02row (i,l,n)
#endif
#if defined x03
            x03pak (ioff+i,l,n) = x03row (i,l,n)
#endif
#if defined x04
            x04pak (ioff+i,l,n) = x04row (i,l,n)
#endif
#if defined x05
            x05pak (ioff+i,l,n) = x05row (i,l,n)
#endif
#if defined x06
            x06pak (ioff+i,l,n) = x06row (i,l,n)
#endif
#if defined x07
            x07pak (ioff+i,l,n) = x07row (i,l,n)
#endif
#if defined x08
            x08pak (ioff+i,l,n) = x08row (i,l,n)
#endif
#if defined x09
            x09pak (ioff+i,l,n) = x09row (i,l,n)
#endif
    end do
  end do
end do
#if defined xtraconv
      do i=il1,il2
        bcdpak (ioff+i) = bcdrow (i)
        bcspak (ioff+i) = bcsrow (i)
        capepak(ioff+i) = caperow(i)
        cdcbpak(ioff+i) = cdcbrow(i)
        cinhpak(ioff+i) = cinhrow(i)
        cscbpak(ioff+i) = cscbrow(i)
        tcdpak (ioff+i) = tcdrow (i)
        tcspak (ioff+i) = tcsrow (i)
      end do
!
      do l=1,ilev
      do i=il1,il2
          dmcpak (ioff+i,l) = dmcrow (i,l)
          smcpak (ioff+i,l) = smcrow (i,l)
          dmcdpak(ioff+i,l) = dmcdrow(i,l)
          dmcupak(ioff+i,l) = dmcurow(i,l)

      end do
      end do
#endif
#if defined xtrachem
      do i=il1,il2
        dd4pak (ioff+i) = dd4row (i)
        dox4pak(ioff+i) = dox4row(i)
        doxdpak(ioff+i) = doxdrow(i)
        esdpak (ioff+i) = esdrow (i)
        esfspak(ioff+i) = esfsrow(i)
        eaispak(ioff+i) = eaisrow(i)
        estspak(ioff+i) = estsrow(i)
        efispak(ioff+i) = efisrow(i)
        esfbpak(ioff+i) = esfbrow(i)
        eaibpak(ioff+i) = eaibrow(i)
        estbpak(ioff+i) = estbrow(i)
        efibpak(ioff+i) = efibrow(i)
        esfopak(ioff+i) = esforow(i)
        eaiopak(ioff+i) = eaiorow(i)
        estopak(ioff+i) = estorow(i)
        efiopak(ioff+i) = efiorow(i)
        edslpak(ioff+i) = edslrow(i)
        edsopak(ioff+i) = edsorow(i)
        esvcpak(ioff+i) = esvcrow(i)
        esvepak(ioff+i) = esverow(i)
        noxdpak(ioff+i) = noxdrow(i)
        wdd4pak(ioff+i) = wdd4row(i)
        wdl4pak(ioff+i) = wdl4row(i)
        wds4pak(ioff+i) = wds4row(i)
      end do
#ifndef pla
      do i=il1,il2
        dddpak (ioff+i) = dddrow (i)
        ddbpak (ioff+i) = ddbrow (i)
        ddopak (ioff+i) = ddorow (i)
        ddspak (ioff+i) = ddsrow (i)
        wdldpak(ioff+i) = wdldrow(i)
        wdlbpak(ioff+i) = wdlbrow(i)
        wdlopak(ioff+i) = wdlorow(i)
        wdlspak(ioff+i) = wdlsrow(i)
        wdddpak(ioff+i) = wdddrow(i)
        wddbpak(ioff+i) = wddbrow(i)
        wddopak(ioff+i) = wddorow(i)
        wddspak(ioff+i) = wddsrow(i)
        wdsdpak(ioff+i) = wdsdrow(i)
        wdsbpak(ioff+i) = wdsbrow(i)
        wdsopak(ioff+i) = wdsorow(i)
        wdsspak(ioff+i) = wdssrow(i)
        dd6pak (ioff+i) = dd6row (i)
        sdhppak(ioff+i) = sdhprow(i)
        sdo3pak(ioff+i) = sdo3row(i)
        slhppak(ioff+i) = slhprow(i)
        slo3pak(ioff+i) = slo3row(i)
        sshppak(ioff+i) = sshprow(i)
        sso3pak(ioff+i) = sso3row(i)
        wdd6pak(ioff+i) = wdd6row(i)
        wdl6pak(ioff+i) = wdl6row(i)
        wds6pak(ioff+i) = wds6row(i)
      end do
!
      do l=1,ilev
        do i=il1,il2
          phdpak (ioff+i,l) = phdrow (i,l)
          phlpak (ioff+i,l) = phlrow (i,l)
          phspak (ioff+i,l) = phsrow (i,l)
        end do
      end do
#endif
#endif
#if defined (xtradust)
      do i=il1,il2
        duwdpak(ioff+i) = duwdrow(i)
        dustpak(ioff+i) = dustrow(i)
        duthpak(ioff+i) = duthrow(i)
        usmkpak(ioff+i) = usmkrow(i)
        fallpak(ioff+i) = fallrow(i)
        fa10pak(ioff+i) = fa10row(i)
         fa2pak(ioff+i) =  fa2row(i)
         fa1pak(ioff+i) =  fa1row(i)
        gustpak(ioff+i) = gustrow(i)
        zspdpak(ioff+i) = zspdrow(i)
        vgfrpak(ioff+i) = vgfrrow(i)
        smfrpak(ioff+i) = smfrrow(i)
      end do
#endif
#if (defined(pla) && defined(pam))
      do l=1,ilev
      do i=il1,il2
        ssldpak(ioff+i,l)   = ssldrow(i,l)
        resspak(ioff+i,l)   = ressrow(i,l)
        vesspak(ioff+i,l)   = vessrow(i,l)
        dsldpak(ioff+i,l)   = dsldrow(i,l)
        redspak(ioff+i,l)   = redsrow(i,l)
        vedspak(ioff+i,l)   = vedsrow(i,l)
        bcldpak(ioff+i,l)   = bcldrow(i,l)
        rebcpak(ioff+i,l)   = rebcrow(i,l)
        vebcpak(ioff+i,l)   = vebcrow(i,l)
        ocldpak(ioff+i,l)   = ocldrow(i,l)
        reocpak(ioff+i,l)   = reocrow(i,l)
        veocpak(ioff+i,l)   = veocrow(i,l)
        amldpak(ioff+i,l)   = amldrow(i,l)
        reampak(ioff+i,l)   = reamrow(i,l)
        veampak(ioff+i,l)   = veamrow(i,l)
        fr1pak (ioff+i,l)   = fr1row (i,l)
        fr2pak (ioff+i,l)   = fr2row (i,l)
        zcdnpak(ioff+i,l)   = zcdnrow(i,l)
        bcicpak(ioff+i,l)   = bcicrow(i,l)
        oednpak(ioff+i,l,:) = oednrow(i,l,:)
        oercpak(ioff+i,l,:) = oercrow(i,l,:)
        oidnpak(ioff+i,l)   = oidnrow(i,l)
        oircpak(ioff+i,l)   = oircrow(i,l)
        svvbpak(ioff+i,l)   = svvbrow(i,l)
        psvvpak(ioff+i,l)   = psvvrow(i,l)
        svmbpak(ioff+i,l,:) = svmbrow(i,l,:)
        svcbpak(ioff+i,l,:) = svcbrow(i,l,:)
        pnvbpak(ioff+i,l,:)  =pnvbrow(i,l,:)
        pnmbpak(ioff+i,l,:,:)=pnmbrow(i,l,:,:)
        pncbpak(ioff+i,l,:,:)=pncbrow(i,l,:,:)
        psvbpak(ioff+i,l,:,:)  =psvbrow(i,l,:,:)
        psmbpak(ioff+i,l,:,:,:)=psmbrow(i,l,:,:,:)
        pscbpak(ioff+i,l,:,:,:)=pscbrow(i,l,:,:,:)
        qnvbpak(ioff+i,l,:)  =qnvbrow(i,l,:)
        qnmbpak(ioff+i,l,:,:)=qnmbrow(i,l,:,:)
        qncbpak(ioff+i,l,:,:)=qncbrow(i,l,:,:)
        qsvbpak(ioff+i,l,:,:)  =qsvbrow(i,l,:,:)
        qsmbpak(ioff+i,l,:,:,:)=qsmbrow(i,l,:,:,:)
        qscbpak(ioff+i,l,:,:,:)=qscbrow(i,l,:,:,:)
        qgvbpak(ioff+i,l)   = qgvbrow(i,l)
        qgmbpak(ioff+i,l,:) = qgmbrow(i,l,:)
        qgcbpak(ioff+i,l,:) = qgcbrow(i,l,:)
        qdvbpak(ioff+i,l)   = qdvbrow(i,l)
        qdmbpak(ioff+i,l,:) = qdmbrow(i,l,:)
        qdcbpak(ioff+i,l,:) = qdcbrow(i,l,:)
        onvbpak(ioff+i,l,:)  =onvbrow(i,l,:)
        onmbpak(ioff+i,l,:,:)=onmbrow(i,l,:,:)
        oncbpak(ioff+i,l,:,:)=oncbrow(i,l,:,:)
        osvbpak(ioff+i,l,:,:)  =osvbrow(i,l,:,:)
        osmbpak(ioff+i,l,:,:,:)=osmbrow(i,l,:,:,:)
        oscbpak(ioff+i,l,:,:,:)=oscbrow(i,l,:,:,:)
        ogvbpak(ioff+i,l)   = ogvbrow(i,l)
        ogmbpak(ioff+i,l,:) = ogmbrow(i,l,:)
        ogcbpak(ioff+i,l,:) = ogcbrow(i,l,:)
        devbpak(ioff+i,l,:) = devbrow(i,l,:)
        pdevpak(ioff+i,l,:) = pdevrow(i,l,:)
        dembpak(ioff+i,l,:,:)=dembrow(i,l,:,:)
        decbpak(ioff+i,l,:,:)=decbrow(i,l,:,:)
        divbpak(ioff+i,l)   = divbrow(i,l)
        pdivpak(ioff+i,l)   = pdivrow(i,l)
        dimbpak(ioff+i,l,:) = dimbrow(i,l,:)
        dicbpak(ioff+i,l,:) = dicbrow(i,l,:)
        revbpak(ioff+i,l,:) = revbrow(i,l,:)
        prevpak(ioff+i,l,:) = prevrow(i,l,:)
        rembpak(ioff+i,l,:,:)=rembrow(i,l,:,:)
        recbpak(ioff+i,l,:,:)=recbrow(i,l,:,:)
        rivbpak(ioff+i,l)   = rivbrow(i,l)
        privpak(ioff+i,l)   = privrow(i,l)
        rimbpak(ioff+i,l,:) = rimbrow(i,l,:)
        ricbpak(ioff+i,l,:) = ricbrow(i,l,:)
        sulipak(ioff+i,l)  = sulirow(i,l)
        rsuipak(ioff+i,l)  = rsuirow(i,l)
        vsuipak(ioff+i,l)  = vsuirow(i,l)
        f1supak(ioff+i,l)  = f1surow(i,l)
        f2supak(ioff+i,l)  = f2surow(i,l)
        bclipak(ioff+i,l)  = bclirow(i,l)
        rbcipak(ioff+i,l)  = rbcirow(i,l)
        vbcipak(ioff+i,l)  = vbcirow(i,l)
        f1bcpak(ioff+i,l)  = f1bcrow(i,l)
        f2bcpak(ioff+i,l)  = f2bcrow(i,l)
        oclipak(ioff+i,l)  = oclirow(i,l)
        rocipak(ioff+i,l)  = rocirow(i,l)
        vocipak(ioff+i,l)  = vocirow(i,l)
        f1ocpak(ioff+i,l)  = f1ocrow(i,l)
        f2ocpak(ioff+i,l)  = f2ocrow(i,l)
      end do
      end do
#if defined (xtrapla1)
      do l=1,ilev
      do i=il1,il2
        sncnpak(ioff+i,l) = sncnrow(i,l)
        ssunpak(ioff+i,l) = ssunrow(i,l)
        scndpak(ioff+i,l) = scndrow(i,l)
        sgsppak(ioff+i,l) = sgsprow(i,l)
        ccnpak (ioff+i,l) = ccnrow (i,l)
        cc02pak(ioff+i,l) = cc02row(i,l)
        cc04pak(ioff+i,l) = cc04row(i,l)
        cc08pak(ioff+i,l) = cc08row(i,l)
        cc16pak(ioff+i,l) = cc16row(i,l)
        ccnepak(ioff+i,l) = ccnerow(i,l)
        acaspak(ioff+i,l) = acasrow(i,l)
        acoapak(ioff+i,l) = acoarow(i,l)
        acbcpak(ioff+i,l) = acbcrow(i,l)
        acsspak(ioff+i,l) = acssrow(i,l)
        acmdpak(ioff+i,l) = acmdrow(i,l)
        ntpak  (ioff+i,l) = ntrow(i,l)
        n20pak (ioff+i,l) = n20row(i,l)
        n50pak (ioff+i,l) = n50row(i,l)
        n100pak(ioff+i,l) = n100row(i,l)
        n200pak(ioff+i,l) = n200row(i,l)
        wtpak  (ioff+i,l) = wtrow(i,l)
        w20pak (ioff+i,l) = w20row(i,l)
        w50pak (ioff+i,l) = w50row(i,l)
        w100pak(ioff+i,l) = w100row(i,l)
        w200pak(ioff+i,l) = w200row(i,l)
        rcripak(ioff+i,l) = rcrirow(i,l)
        supspak(ioff+i,l) = supsrow(i,l)
        henrpak(ioff+i,l) = henrrow(i,l)
        o3frpak(ioff+i,l) = o3frrow(i,l)
        h2o2frpak(ioff+i,l) = h2o2frrow(i,l)
        wparpak(ioff+i,l) = wparrow(i,l)
        pm25pak(ioff+i,l) = pm25row(i,l)
        pm10pak(ioff+i,l) = pm10row(i,l)
        dm25pak(ioff+i,l) = dm25row(i,l)
        dm10pak(ioff+i,l) = dm10row(i,l)
      end do
      end do
      do i=il1,il2
        vncnpak(ioff+i) = vncnrow(i)
        vasnpak(ioff+i) = vasnrow(i)
        vscdpak(ioff+i) = vscdrow(i)
        vgsppak(ioff+i) = vgsprow(i)
        voaepak(ioff+i) = voaerow(i)
        vbcepak(ioff+i) = vbcerow(i)
        vasepak(ioff+i) = vaserow(i)
        vmdepak(ioff+i) = vmderow(i)
        vssepak(ioff+i) = vsserow(i)
        voawpak(ioff+i) = voawrow(i)
        vbcwpak(ioff+i) = vbcwrow(i)
        vaswpak(ioff+i) = vaswrow(i)
        vmdwpak(ioff+i) = vmdwrow(i)
        vsswpak(ioff+i) = vsswrow(i)
        voadpak(ioff+i) = voadrow(i)
        vbcdpak(ioff+i) = vbcdrow(i)
        vasdpak(ioff+i) = vasdrow(i)
        vmddpak(ioff+i) = vmddrow(i)
        vssdpak(ioff+i) = vssdrow(i)
        voagpak(ioff+i) = voagrow(i)
        vbcgpak(ioff+i) = vbcgrow(i)
        vasgpak(ioff+i) = vasgrow(i)
        vmdgpak(ioff+i) = vmdgrow(i)
        vssgpak(ioff+i) = vssgrow(i)
        voacpak(ioff+i) = voacrow(i)
        vbccpak(ioff+i) = vbccrow(i)
        vascpak(ioff+i) = vascrow(i)
        vmdcpak(ioff+i) = vmdcrow(i)
        vsscpak(ioff+i) = vsscrow(i)
        vasipak(ioff+i) = vasirow(i)
        vas1pak(ioff+i) = vas1row(i)
        vas2pak(ioff+i) = vas2row(i)
        vas3pak(ioff+i) = vas3row(i)
        vccnpak(ioff+i) = vccnrow(i)
        vcnepak(ioff+i) = vcnerow(i)
      end do
#endif
#if defined (xtrapla2)
      do l=1,ilev
      do i=il1,il2
        cornpak(ioff+i,l) = cornrow(i,l)
        cormpak(ioff+i,l) = cormrow(i,l)
        rsn1pak(ioff+i,l) = rsn1row(i,l)
        rsm1pak(ioff+i,l) = rsm1row(i,l)
        rsn2pak(ioff+i,l) = rsn2row(i,l)
        rsm2pak(ioff+i,l) = rsm2row(i,l)
        rsn3pak(ioff+i,l) = rsn3row(i,l)
        rsm3pak(ioff+i,l) = rsm3row(i,l)
        do isf=1,isdnum
          sdnupak(ioff+i,l,isf) = sdnurow(i,l,isf)
          sdmapak(ioff+i,l,isf) = sdmarow(i,l,isf)
          sdacpak(ioff+i,l,isf) = sdacrow(i,l,isf)
          sdcopak(ioff+i,l,isf) = sdcorow(i,l,isf)
          sssnpak(ioff+i,l,isf) = sssnrow(i,l,isf)
          smdnpak(ioff+i,l,isf) = smdnrow(i,l,isf)
          sianpak(ioff+i,l,isf) = sianrow(i,l,isf)
          sssmpak(ioff+i,l,isf) = sssmrow(i,l,isf)
          smdmpak(ioff+i,l,isf) = smdmrow(i,l,isf)
          sewmpak(ioff+i,l,isf) = sewmrow(i,l,isf)
          ssumpak(ioff+i,l,isf) = ssumrow(i,l,isf)
          socmpak(ioff+i,l,isf) = socmrow(i,l,isf)
          sbcmpak(ioff+i,l,isf) = sbcmrow(i,l,isf)
          siwmpak(ioff+i,l,isf) = siwmrow(i,l,isf)
        end do
      end do
      end do
      do i=il1,il2
        do isf=1,isdiag
          sdvlpak(ioff+i,isf) = sdvlrow(i,isf)
        end do
        vrn1pak(ioff+i) = vrn1row(i)
        vrm1pak(ioff+i) = vrm1row(i)
        vrn2pak(ioff+i) = vrn2row(i)
        vrm2pak(ioff+i) = vrm2row(i)
        vrn3pak(ioff+i) = vrn3row(i)
        vrm3pak(ioff+i) = vrm3row(i)
      end do
      do i=il1,il2
        defapak(ioff+i) = defarow(i)
        defcpak(ioff+i) = defcrow(i)
        do isf=1,isdust
          dmacpak(ioff+i,isf) = dmacrow(i,isf)
          dmcopak(ioff+i,isf) = dmcorow(i,isf)
          dnacpak(ioff+i,isf) = dnacrow(i,isf)
          dncopak(ioff+i,isf) = dncorow(i,isf)
        end do
        do isf=1,isdiag
          defxpak(ioff+i,isf) = defxrow(i,isf)
          defnpak(ioff+i,isf) = defnrow(i,isf)
        end do
      end do
      do i=il1,il2
        tnsspak(ioff+i) = tnssrow(i)
        tnmdpak(ioff+i) = tnmdrow(i)
        tniapak(ioff+i) = tniarow(i)
        tmsspak(ioff+i) = tmssrow(i)
        tmmdpak(ioff+i) = tmmdrow(i)
        tmocpak(ioff+i) = tmocrow(i)
        tmbcpak(ioff+i) = tmbcrow(i)
        tmsppak(ioff+i) = tmsprow(i)
        do n=1,isdnum
          snsspak(ioff+i,n) = snssrow(i,n)
          snmdpak(ioff+i,n) = snmdrow(i,n)
          sniapak(ioff+i,n) = sniarow(i,n)
          smsspak(ioff+i,n) = smssrow(i,n)
          smmdpak(ioff+i,n) = smmdrow(i,n)
          smocpak(ioff+i,n) = smocrow(i,n)
          smbcpak(ioff+i,n) = smbcrow(i,n)
          smsppak(ioff+i,n) = smsprow(i,n)
          siwhpak(ioff+i,n) = siwhrow(i,n)
          sewhpak(ioff+i,n) = sewhrow(i,n)
        end do
      end do
#endif
#endif
#if defined xtrals
      do l=1,ilev
      do i=il1,il2
        aggpak (ioff+i,l) = aggrow (i,l)
        autpak (ioff+i,l) = autrow (i,l)
        cndpak (ioff+i,l) = cndrow (i,l)
        deppak (ioff+i,l) = deprow (i,l)
        evppak (ioff+i,l) = evprow (i,l)
        frhpak (ioff+i,l) = frhrow (i,l)
        frkpak (ioff+i,l) = frkrow (i,l)
        frspak (ioff+i,l) = frsrow (i,l)
        mltipak(ioff+i,l) = mltirow(i,l)
        mltspak(ioff+i,l) = mltsrow(i,l)
        raclpak(ioff+i,l) = raclrow(i,l)
        rainpak(ioff+i,l) = rainrow(i,l)
        sacipak(ioff+i,l) = sacirow(i,l)
        saclpak(ioff+i,l) = saclrow(i,l)
        snowpak(ioff+i,l) = snowrow(i,l)
        subpak (ioff+i,l) = subrow (i,l)
        sedipak(ioff+i,l) = sedirow(i,l)
        rliqpak(ioff+i,l) = rliqrow(i,l)
        ricepak(ioff+i,l) = ricerow(i,l)
        cliqpak(ioff+i,l) = cliqrow(i,l)
        cicepak(ioff+i,l) = cicerow(i,l)
        rlncpak(ioff+i,l) = rlncrow(i,l)

        rliqpal(ioff+i,l) = rliqrol(i,l)
        ricepal(ioff+i,l) = ricerol(i,l)
        cliqpal(ioff+i,l) = cliqrol(i,l)
        cicepal(ioff+i,l) = cicerol(i,l)
        rlncpal(ioff+i,l) = rlncrol(i,l)


      end do
      end do
!
      do i=il1,il2
        vaggpak(ioff+i)   = vaggrow(i)
        vautpak(ioff+i)   = vautrow(i)
        vcndpak(ioff+i)   = vcndrow(i)
        vdeppak(ioff+i)   = vdeprow(i)
        vevppak(ioff+i)   = vevprow(i)
        vfrhpak(ioff+i)   = vfrhrow(i)
        vfrkpak(ioff+i)   = vfrkrow(i)
        vfrspak(ioff+i)   = vfrsrow(i)
        vmlipak(ioff+i)   = vmlirow(i)
        vmlspak(ioff+i)   = vmlsrow(i)
        vrclpak(ioff+i)   = vrclrow(i)
        vscipak(ioff+i)   = vscirow(i)
        vsclpak(ioff+i)   = vsclrow(i)
        vsubpak(ioff+i)   = vsubrow(i)
        vsedipak(ioff+i)  = vsedirow(i)
        reliqpak(ioff+i)  = reliqrow(i)
        reicepak(ioff+i)  = reicerow(i)
        cldliqpak(ioff+i) = cldliqrow(i)
        cldicepak(ioff+i) = cldicerow(i)
        ctlncpak(ioff+i)  = ctlncrow(i)
        cllncpak(ioff+i)  = cllncrow(i)

        reliqpal(ioff+i)  = reliqrol(i)
        reicepal(ioff+i)  = reicerol(i)
        cldliqpal(ioff+i) = cldliqrol(i)
        cldicepal(ioff+i) = cldicerol(i)
        ctlncpal(ioff+i)  = ctlncrol(i)
        cllncpal(ioff+i)  = cllncrol(i)

      end do
#endif
#if defined (aodpth)
       do i=il1,il2
!
        exb1pak(ioff+i) = exb1row(i)
        exb2pak(ioff+i) = exb2row(i)
        exb3pak(ioff+i) = exb3row(i)
        exb4pak(ioff+i) = exb4row(i)
        exb5pak(ioff+i) = exb5row(i)
        exbtpak(ioff+i) = exbtrow(i)
        odb1pak(ioff+i) = odb1row(i)
        odb2pak(ioff+i) = odb2row(i)
        odb3pak(ioff+i) = odb3row(i)
        odb4pak(ioff+i) = odb4row(i)
        odb5pak(ioff+i) = odb5row(i)
        odbtpak(ioff+i) = odbtrow(i)
        odbvpak(ioff+i) = odbvrow(i)
        ofb1pak(ioff+i) = ofb1row(i)
        ofb2pak(ioff+i) = ofb2row(i)
        ofb3pak(ioff+i) = ofb3row(i)
        ofb4pak(ioff+i) = ofb4row(i)
        ofb5pak(ioff+i) = ofb5row(i)
        ofbtpak(ioff+i) = ofbtrow(i)
        abb1pak(ioff+i) = abb1row(i)
        abb2pak(ioff+i) = abb2row(i)
        abb3pak(ioff+i) = abb3row(i)
        abb4pak(ioff+i) = abb4row(i)
        abb5pak(ioff+i) = abb5row(i)
        abbtpak(ioff+i) = abbtrow(i)
!
        exb1pal(ioff+i) = exb1rol(i)
        exb2pal(ioff+i) = exb2rol(i)
        exb3pal(ioff+i) = exb3rol(i)
        exb4pal(ioff+i) = exb4rol(i)
        exb5pal(ioff+i) = exb5rol(i)
        exbtpal(ioff+i) = exbtrol(i)
        odb1pal(ioff+i) = odb1rol(i)
        odb2pal(ioff+i) = odb2rol(i)
        odb3pal(ioff+i) = odb3rol(i)
        odb4pal(ioff+i) = odb4rol(i)
        odb5pal(ioff+i) = odb5rol(i)
        odbtpal(ioff+i) = odbtrol(i)
        odbvpal(ioff+i) = odbvrol(i)
        ofb1pal(ioff+i) = ofb1rol(i)
        ofb2pal(ioff+i) = ofb2rol(i)
        ofb3pal(ioff+i) = ofb3rol(i)
        ofb4pal(ioff+i) = ofb4rol(i)
        ofb5pal(ioff+i) = ofb5rol(i)
        ofbtpal(ioff+i) = ofbtrol(i)
        abb1pal(ioff+i) = abb1rol(i)
        abb2pal(ioff+i) = abb2rol(i)
        abb3pal(ioff+i) = abb3rol(i)
        abb4pal(ioff+i) = abb4rol(i)
        abb5pal(ioff+i) = abb5rol(i)
        abbtpal(ioff+i) = abbtrol(i)
!
        exs1pak(ioff+i) = exs1row(i)
        exs2pak(ioff+i) = exs2row(i)
        exs3pak(ioff+i) = exs3row(i)
        exs4pak(ioff+i) = exs4row(i)
        exs5pak(ioff+i) = exs5row(i)
        exstpak(ioff+i) = exstrow(i)
        ods1pak(ioff+i) = ods1row(i)
        ods2pak(ioff+i) = ods2row(i)
        ods3pak(ioff+i) = ods3row(i)
        ods4pak(ioff+i) = ods4row(i)
        ods5pak(ioff+i) = ods5row(i)
        odstpak(ioff+i) = odstrow(i)
        odsvpak(ioff+i) = odsvrow(i)
        ofs1pak(ioff+i) = ofs1row(i)
        ofs2pak(ioff+i) = ofs2row(i)
        ofs3pak(ioff+i) = ofs3row(i)
        ofs4pak(ioff+i) = ofs4row(i)
        ofs5pak(ioff+i) = ofs5row(i)
        ofstpak(ioff+i) = ofstrow(i)
        abs1pak(ioff+i) = abs1row(i)
        abs2pak(ioff+i) = abs2row(i)
        abs3pak(ioff+i) = abs3row(i)
        abs4pak(ioff+i) = abs4row(i)
        abs5pak(ioff+i) = abs5row(i)
        abstpak(ioff+i) = abstrow(i)
!
        exs1pal(ioff+i) = exs1rol(i)
        exs2pal(ioff+i) = exs2rol(i)
        exs3pal(ioff+i) = exs3rol(i)
        exs4pal(ioff+i) = exs4rol(i)
        exs5pal(ioff+i) = exs5rol(i)
        exstpal(ioff+i) = exstrol(i)
        ods1pal(ioff+i) = ods1rol(i)
        ods2pal(ioff+i) = ods2rol(i)
        ods3pal(ioff+i) = ods3rol(i)
        ods4pal(ioff+i) = ods4rol(i)
        ods5pal(ioff+i) = ods5rol(i)
        odstpal(ioff+i) = odstrol(i)
        odsvpal(ioff+i) = odsvrol(i)
        ofs1pal(ioff+i) = ofs1rol(i)
        ofs2pal(ioff+i) = ofs2rol(i)
        ofs3pal(ioff+i) = ofs3rol(i)
        ofs4pal(ioff+i) = ofs4rol(i)
        ofs5pal(ioff+i) = ofs5rol(i)
        ofstpal(ioff+i) = ofstrol(i)
        abs1pal(ioff+i) = abs1rol(i)
        abs2pal(ioff+i) = abs2rol(i)
        abs3pal(ioff+i) = abs3rol(i)
        abs4pal(ioff+i) = abs4rol(i)
        abs5pal(ioff+i) = abs5rol(i)
        abstpal(ioff+i) = abstrol(i)
!
       end do
#endif
#if defined use_cosp
! cosp input
      do l=1,ilev
         do i=il1,il2
            rmixpak (ioff+i,l) = rmixrow (i,l)
            smixpak (ioff+i,l) = smixrow (i,l)
            rrefpak (ioff+i,l) = rrefrow (i,l)
            srefpak (ioff+i,l) = srefrow (i,l)
         end do ! i
      end do ! l

! cosp output

! put the accumulating arrays from cosp into the output arrays

      do i = il1, il2

! isccp fields
         if (lalbisccp) &
         o_albisccp(ioff+i) = albisccp(i)

         if (ltauisccp) &
         o_tauisccp(ioff+i) = tauisccp(i)

         if (lpctisccp) &
         o_pctisccp(ioff+i) = pctisccp(i)

         if (lcltisccp) &
         o_cltisccp(ioff+i) = cltisccp(i)


         if (lmeantbisccp) &
         o_meantbisccp(ioff+i) = meantbisccp(i)

         if (lmeantbclrisccp) &
         o_meantbclrisccp(ioff+i) = meantbclrisccp(i)

         if (lisccp_sim) &
         o_sunl(ioff+i) = sunl(i)

! calipso fields

         if (lclhcalipso) then
            o_clhcalipso(ioff+i)    = clhcalipso(i)
            ocnt_clhcalipso(ioff+i) = cnt_clhcalipso(i)
         end if

         if (lclmcalipso) then
            o_clmcalipso(ioff+i)    = clmcalipso(i)
            ocnt_clmcalipso(ioff+i) = cnt_clmcalipso(i)
         end if

         if (lcllcalipso) then
            o_cllcalipso(ioff+i)    = cllcalipso(i)
            ocnt_cllcalipso(ioff+i) = cnt_cllcalipso(i)
         end if

         if (lcltcalipso) then
            o_cltcalipso(ioff+i)    = cltcalipso(i)
            ocnt_cltcalipso(ioff+i) = cnt_cltcalipso(i)
         end if

         if (lclhcalipsoliq) then
            o_clhcalipsoliq(ioff+i)    = clhcalipsoliq(i)
            ocnt_clhcalipsoliq(ioff+i) = cnt_clhcalipsoliq(i)
         end if

         if (lclmcalipsoliq) then
            o_clmcalipsoliq(ioff+i)    = clmcalipsoliq(i)
            ocnt_clmcalipsoliq(ioff+i) = cnt_clmcalipsoliq(i)
         end if

         if (lcllcalipsoliq) then
            o_cllcalipsoliq(ioff+i)    = cllcalipsoliq(i)
            ocnt_cllcalipsoliq(ioff+i) = cnt_cllcalipsoliq(i)
         end if

         if (lcltcalipsoliq) then
            o_cltcalipsoliq(ioff+i)    = cltcalipsoliq(i)
            ocnt_cltcalipsoliq(ioff+i) = cnt_cltcalipsoliq(i)
         end if

         if (lclhcalipsoice) then
            o_clhcalipsoice(ioff+i)    = clhcalipsoice(i)
            ocnt_clhcalipsoice(ioff+i) = cnt_clhcalipsoice(i)
         end if

         if (lclmcalipsoice) then
            o_clmcalipsoice(ioff+i)    = clmcalipsoice(i)
            ocnt_clmcalipsoice(ioff+i) = cnt_clmcalipsoice(i)
         end if

         if (lcllcalipsoice) then
            o_cllcalipsoice(ioff+i)    = cllcalipsoice(i)
            ocnt_cllcalipsoice(ioff+i) = cnt_cllcalipsoice(i)
         end if

         if (lcltcalipsoice) then
            o_cltcalipsoice(ioff+i)    = cltcalipsoice(i)
            ocnt_cltcalipsoice(ioff+i) = cnt_cltcalipsoice(i)
         end if

         if (lclhcalipsoun) then
            o_clhcalipsoun(ioff+i)    = clhcalipsoun(i)
            ocnt_clhcalipsoun(ioff+i) = cnt_clhcalipsoun(i)
         end if

         if (lclmcalipsoun) then
            o_clmcalipsoun(ioff+i)    = clmcalipsoun(i)
            ocnt_clmcalipsoun(ioff+i) = cnt_clmcalipsoun(i)
         end if

         if (lcllcalipsoun) then
            o_cllcalipsoun(ioff+i)    = cllcalipsoun(i)
            ocnt_cllcalipsoun(ioff+i) = cnt_cllcalipsoun(i)
         end if

         if (lcltcalipsoun) then
            o_cltcalipsoun(ioff+i)    = cltcalipsoun(i)
            ocnt_cltcalipsoun(ioff+i) = cnt_cltcalipsoun(i)
         end if


! cloudsat+calipso fields
         if (lcltlidarradar) then
            o_cltlidarradar(ioff+i)    = cltlidarradar(i)
            ocnt_cltlidarradar(ioff+i) = cnt_cltlidarradar(i)
         end if
      end do


! several special fields > 1d

! isccp
      if (lclisccp) then
         do ip = 1, 7           ! nptop
            do it = 1, 7        ! ntaucld
               do i = il1, il2
                  o_clisccp(ioff+i,it,ip) = clisccp(i,it,ip)

               end do
            end do
         end do
      end if

! parasol
      if (lparasolrefl) then
         do ip = 1, parasol_nrefl
            do i = il1, il2
               o_parasol_refl(ioff+i,ip)    =  parasol_refl(i,ip)
               ocnt_parasol_refl(ioff+i,ip) =  cnt_parasol_refl(i,ip)
            end do
         end do
      end if

! 3d fields that might be interpolated to special levels or not
      if (use_vgrid) then       ! interpolate to specific heights
         do n = 1, nlr
            do i = il1, il2
               if (lclcalipso) then
                  o_clcalipso(ioff+i,n)    = clcalipso(i,n)
                  ocnt_clcalipso(ioff+i,n) = cnt_clcalipso(i,n)
               end if

               if (lclcalipsoliq) then
                  o_clcalipsoliq(ioff+i,n) = clcalipsoliq(i,n)
                  ocnt_clcalipsoliq(ioff+i,n) = cnt_clcalipsoliq(i,n)
               end if

               if (lclcalipsoice) then
                  o_clcalipsoice(ioff+i,n) = clcalipsoice(i,n)
                  ocnt_clcalipsoice(ioff+i,n) = cnt_clcalipsoice(i,n)
               end if

               if (lclcalipsoun) then
                  o_clcalipsoun(ioff+i,n) = clcalipsoun(i,n)
                  ocnt_clcalipsoun(ioff+i,n) = cnt_clcalipsoun(i,n)
               end if

               if (lclcalipso2) then
                  o_clcalipso2(ioff+i,n)    = clcalipso2(i,n)
                  ocnt_clcalipso2(ioff+i,n) = cnt_clcalipso2(i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  o_cosp_height_mask(ioff+i,n) = cosp_height_mask(i,n)
               end if
            end do
         end do
      else
         do n = 1, ilev
            do i = il1, il2
               if (lclcalipso) then
                   o_clcalipso(ioff+i,n) =  clcalipso(i,n)
                   ocnt_clcalipso(ioff+i,n) = cnt_clcalipso(i,n)
               end if

               if (lclcalipsoliq) then
                  o_clcalipsoliq(ioff+i,n) = clcalipsoliq(i,n)
                  ocnt_clcalipsoliq(ioff+i,n) = cnt_clcalipsoliq(i,n)
               end if

               if (lclcalipsoice) then
                  o_clcalipsoice(ioff+i,n) = clcalipsoice(i,n)
                  ocnt_clcalipsoice(ioff+i,n) = cnt_clcalipsoice(i,n)
               end if

               if (lclcalipsoun) then
                  o_clcalipsoun(ioff+i,n) = clcalipsoun(i,n)
                  ocnt_clcalipsoun(ioff+i,n) = cnt_clcalipsoun(i,n)
               end if

               if (lclcalipso2) then
                   o_clcalipso2(ioff+i,n) = clcalipso2(i,n)
                   ocnt_clcalipso2(ioff+i,n) = cnt_clcalipso2(i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  o_cosp_height_mask(ioff+i,n) = cosp_height_mask(i,n)
               end if
            end do
         end do
      end if

         do n = 1, lidar_ntemp
            do i = il1, il2
               if (lclcalipsotmp) then
                  o_clcalipsotmp(ioff+i,n) = clcalipsotmp(i,n)
                  ocnt_clcalipsotmp(ioff+i,n) = cnt_clcalipsotmp(i,n)
               end if

               if (lclcalipsotmpliq) then
                  o_clcalipsotmpliq(ioff+i,n) = clcalipsotmpliq(i,n)
                  ocnt_clcalipsotmpliq(ioff+i,n) = &
                                        cnt_clcalipsotmpliq(i,n)
               end if

               if (lclcalipsotmpice) then
                  o_clcalipsotmpice(ioff+i,n) = clcalipsotmpice(i,n)
                  ocnt_clcalipsotmpice(ioff+i,n) = &
                                        cnt_clcalipsotmpice(i,n)
               end if

               if (lclcalipsotmpun) then
                  o_clcalipsotmpun(ioff+i,n) = clcalipsotmpun(i,n)
                  ocnt_clcalipsotmpun(ioff+i,n) = &
                                         cnt_clcalipsotmpun(i,n)
               end if
            end do ! i
         end do ! n

! cfads (2d histograms)

! calipso

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, nlr
                  do i = il1, il2
                     o_cfad_lidarsr532(ioff+i,n,isr) = &
                     cfad_lidarsr532(i,n,isr)
                  end do
               end do
            end do
         end if
      else
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, ilev
                  do i = il1, il2
                     o_cfad_lidarsr532(ioff+i,n,isr) = &
                     cfad_lidarsr532(i,n,isr)
                  end do
               end do
            end do
         end if
      end if

! cloudsat

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, nlr
                  do i = il1, il2
                     o_cfad_dbze94(ioff+i,n,ize)= &
                     cfad_dbze94(i,n,ize)
                  end do
               end do
            end do
         end if
      else
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, ilev
                  do i = il1, il2
                     o_cfad_dbze94(ioff+i,n,ize) = &
                     cfad_dbze94(i,n,ize)
                  end do
               end do
            end do
         end if
      end if
      if (lceres_sim) then
         do ip = 1, nceres
            do i = il1, il2
               o_ceres_cf(ioff+i,ip)    = ceres_cf(i,ip)
               o_ceres_cnt(ioff+i,ip)   = ceres_cnt(i,ip)
               o_ceres_ctp(ioff+i,ip)   = ceres_ctp(i,ip)
               o_ceres_tau(ioff+i,ip)   = ceres_tau(i,ip)
               o_ceres_lntau(ioff+i,ip) = ceres_lntau(i,ip)
               o_ceres_lwp(ioff+i,ip)   = ceres_lwp(i,ip)
               o_ceres_iwp(ioff+i,ip)   = ceres_iwp(i,ip)
               o_ceres_cfl(ioff+i,ip)   = ceres_cfl(i,ip)
               o_ceres_cfi(ioff+i,ip)   = ceres_cfi(i,ip)
               o_ceres_cntl(ioff+i,ip)  = ceres_cntl(i,ip)
               o_ceres_cnti(ioff+i,ip)  = ceres_cnti(i,ip)
               o_ceres_rel(ioff+i,ip)   = ceres_rel(i,ip)
               o_ceres_cdnc(ioff+i,ip)  = ceres_cdnc(i,ip)
               o_ceres_clm(ioff+i,ip)   = ceres_clm(i,ip)
               o_ceres_cntlm(ioff+i,ip) = ceres_cntlm(i,ip)
            end do ! i
         end do ! ip
      end if
      if (lceres_sim .and. lswath) then
         do ip = 1, nceres
            do i = il1, il2
               os_ceres_cf(ioff+i,ip)    = s_ceres_cf(i,ip)
               os_ceres_cnt(ioff+i,ip)   = s_ceres_cnt(i,ip)
               os_ceres_ctp(ioff+i,ip)   = s_ceres_ctp(i,ip)
               os_ceres_tau(ioff+i,ip)   = s_ceres_tau(i,ip)
               os_ceres_lntau(ioff+i,ip) = s_ceres_lntau(i,ip)
               os_ceres_lwp(ioff+i,ip)   = s_ceres_lwp(i,ip)
               os_ceres_iwp(ioff+i,ip)   = s_ceres_iwp(i,ip)
               os_ceres_cfl(ioff+i,ip)   = s_ceres_cfl(i,ip)
               os_ceres_cfi(ioff+i,ip)   = s_ceres_cfi(i,ip)
               os_ceres_cntl(ioff+i,ip)  = s_ceres_cntl(i,ip)
               os_ceres_cnti(ioff+i,ip)  = s_ceres_cnti(i,ip)
               os_ceres_rel(ioff+i,ip)   = s_ceres_rel(i,ip)
               os_ceres_cdnc(ioff+i,ip)  = s_ceres_cdnc(i,ip)
               os_ceres_clm(ioff+i,ip)   = s_ceres_clm(i,ip)
               os_ceres_cntlm(ioff+i,ip) = s_ceres_cntlm(i,ip)
            end do ! i
         end do ! ip
      end if

! misr
      if (lclmisr) then
         do i = il1, il2
            o_misr_cldarea(ioff+i)   = misr_cldarea(i)
            o_misr_mean_ztop(ioff+i) = misr_mean_ztop(i)
         end do ! i

         do ip = 1,misr_n_cth
            do i = il1, il2
               o_dist_model_layertops(ioff+i,ip) = &
                                     dist_model_layertops(i,ip)
            end do ! i
         end do ! ip

         ibox = 1
         do ip = 1,misr_n_cth
            do it = 1, 7
               do i = il1, il2
                  o_fq_misr_tau_v_cth(ioff+i,ibox) = &
                                     fq_misr_tau_v_cth(i,ibox)
               end do ! i
               ibox = ibox + 1
            end do ! it
         end do ! ip
      end if ! lclmisr

! modis
      if (lcltmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_total_mean(ioff+i) = &
                              modis_cloud_fraction_total_mean(i)
         end do ! i
      end if

      if (lclwmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_water_mean(ioff+i) = &
                              modis_cloud_fraction_water_mean(i)
         end do ! i
      end if

      if (lclimodis) then
         do i = il1, il2
            o_modis_cloud_fraction_ice_mean(ioff+i) = &
                              modis_cloud_fraction_ice_mean(i)
         end do ! i
      end if

      if (lclhmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_high_mean(ioff+i) = &
                              modis_cloud_fraction_high_mean(i)
         end do ! i
      end if

      if (lclmmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_mid_mean(ioff+i) = &
                              modis_cloud_fraction_mid_mean(i)
         end do ! i
      end if

      if (lcllmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_low_mean(ioff+i) = &
                              modis_cloud_fraction_low_mean(i)
         end do ! i
      end if

      if (ltautmodis) then
         do i = il1, il2
            o_modis_optical_thickness_total_mean(ioff+i) = &
                            modis_optical_thickness_total_mean(i)
         end do ! i
      end if

      if (ltauwmodis) then
         do i = il1, il2
            o_modis_optical_thickness_water_mean(ioff+i) = &
                            modis_optical_thickness_water_mean(i)
         end do ! i
      end if

      if (ltauimodis) then
         do i = il1, il2
            o_modis_optical_thickness_ice_mean(ioff+i) = &
                            modis_optical_thickness_ice_mean(i)
         end do ! i
      end if

      if (ltautlogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_total_logmean(ioff+i) = &
                        modis_optical_thickness_total_logmean(i)
         end do ! i
      end if

      if (ltauwlogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_water_logmean(ioff+i) = &
                         modis_optical_thickness_water_logmean(i)
         end do ! i
      end if

      if (ltauilogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_ice_logmean(ioff+i) = &
                          modis_optical_thickness_ice_logmean(i)
         end do ! i
      end if

      if (lreffclwmodis) then
         do i = il1, il2
            o_modis_cloud_particle_size_water_mean(ioff+i) = &
                        modis_cloud_particle_size_water_mean(i)
         end do ! i
      end if

      if (lreffclimodis) then
         do i = il1, il2
            o_modis_cloud_particle_size_ice_mean(ioff+i) = &
                        modis_cloud_particle_size_ice_mean(i)
         end do ! i
      end if

      if (lpctmodis) then
         do i = il1, il2
            o_modis_cloud_top_pressure_total_mean(ioff+i) = &
                       modis_cloud_top_pressure_total_mean(i)
         end do ! i
      end if

      if (llwpmodis) then
         do i = il1, il2
            o_modis_liquid_water_path_mean(ioff+i) = &
                                 modis_liquid_water_path_mean(i)
         end do ! i
      end if

      if (liwpmodis) then
         do i = il1, il2
            o_modis_ice_water_path_mean(ioff+i) = &
                                 modis_ice_water_path_mean(i)
         end do ! i
      end if

      if (lclmodis) then
         ibox = 1
         do ip = 1, a_nummodispressurebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
          o_modis_optical_thickness_vs_cloud_top_pressure(ioff+i,ibox)= &
              modis_optical_thickness_vs_cloud_top_pressure(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrimodis) then
         ibox = 1
         do ip = 1, a_nummodisrefficebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                      o_modis_optical_thickness_vs_reffice(ioff+i,ibox)= &
                         modis_optical_thickness_vs_reffice(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrlmodis) then
         ibox = 1
         do ip = 1, a_nummodisreffliqbins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                      o_modis_optical_thickness_vs_reffliq(ioff+i,ibox)= &
                         modis_optical_thickness_vs_reffliq(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (llcdncmodis) then
         do i = il1, il2
            o_modis_liq_cdnc_mean(ioff+i) = &
                                 modis_liq_cdnc_mean(i)
            o_modis_liq_cdnc_gcm_mean(ioff+i) = &
                                 modis_liq_cdnc_gcm_mean(i)
            o_modis_cloud_fraction_warm_mean(ioff+i) = &
                      modis_cloud_fraction_warm_mean(i)
         end do ! i
      end if

#endif
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
