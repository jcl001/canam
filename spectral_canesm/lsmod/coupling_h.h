#include "cppdef_config.h"

!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

!     * nov 14/2003 - d.robitaille

!     define some variable types for mpi communication

integer*4 :: me !<
integer*4 :: mpiproc !<
integer*4 :: proc !<
integer*4 :: coupler !<
integer*4 :: atmos !<
integer*4 :: ocean !<
character*8 :: member(0:1000) !<

common /mpimodules/ coupler, atmos, ocean, member

integer*4 :: status(mpi_status_size) !<
integer*4 :: ierr !<

integer*4 :: ibuflen !<
parameter (ibuflen=8)


character*3 :: cmodel !<
integer*4 :: cpl_rank_min !<
integer*4 :: cpl_rank_max !<
integer*4 :: ocn_rank_min !<
integer*4 :: ocn_rank_max !<
integer*4 :: atm_rank_min !<
integer*4 :: atm_rank_max !<
integer*4 :: icolor !<
integer*4 :: ikey !<
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
