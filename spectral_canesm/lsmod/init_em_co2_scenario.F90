!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine init_em_co2_scenario(irefyre,irefmne,mynode)
  !***********************************************************************
  ! read surface anthropogenic co2 emission scenario value from a file named em_scenario
  ! based on deck init_vtau_scenario.
  !
  ! input
  !   integer*4 :: mynode  ...mpi node (used to restrict i/o to node 0)
  !
  ! output
  !   integer   :: irefyre ...reference year to be used in surface co2 emissions
  !                        scenario.
  !   integer   :: irefmne ...reference month to be used in surface co2 emissions
  !                        scenario.
  !
  ! jason cole 21 january 2018 - added vtau file type
  ! jason cole 14 december 2016 - initial version
  !***********************************************************************
  use em_defs

  implicit none

  !--- input
  integer*4, intent(in) :: mynode !< Variable description\f$[units]\f$

  !--- output
  integer, intent(inout) :: irefyre !< Variable description\f$[units]\f$
  integer, intent(inout) :: irefmne !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--- local
  integer :: iu !<
  integer :: newunit !<
  logical :: ok !<
  integer, parameter :: verbose=2 !<

  !-----------------------------------------------------------------------
  !--- read surface co2 emissions control parmsub parameters from a namelist
  inquire(file='EM_CONFIG',exist=ok)
  if (ok) then
    iu=newunit(0)
    open(iu,file='EM_CONFIG',form='formatted')
    read(iu,nml=em_config)
    close(iu)
    if (mynode==0) then
      write(6,*)' '
      write(6,em_config)
      write(6,*)' '
      call flush(6)
    end if
  else
    write(6,*)'Namelist file EM_CONFIG is missing.'
    call xit("INIT_EM_CO2_SCENARIO",-1)
  end if

  !--- determine irefyre from value in namelist.
  irefyre=0
  if (em_scn_mode<0) then
    irefyre=(abs(em_scn_mode))/100
    irefmne=-1
  else if (em_scn_mode>0) then
    irefyre=(abs(em_scn_mode))/100
    irefmne=abs(mod(em_scn_mode,100))
  end if

  if (mynode==0 .and. verbose>1) then
    write(6,*)' '
    write(6,*)' em_scn_mode,irefyre=', &
               em_scn_mode,irefyre
    write(6,*)' em_scn_mode,irefmne=', &
               em_scn_mode,irefmne
  end if

end subroutine init_em_co2_scenario
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
