!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine rgtocg2(pako,paki,lon,nlat,ilat)

  !     * june 30, 2003 - m.lazare.
  !
  !     * this routine takes a normal s-n grid field, "PAKI"
  !     * and converts it into ordered s-n pairs, "PAKO",
  !     * which are used within the model ** on each node **.

  !     * ** note ** the cyclic longitude is stripped off !
  !     * ** note ** the variable "MYNODE" passed through the
  !     *            common deck "MPINFO" is used to select the
  !     *            chunk for a given node.

  !     * this routine is typically called when reading-in external
  !     * fields from the "AN" file.
  !     * "RGTOCG" should ** not ** be used.
  !
  implicit none
  integer :: i
  integer, intent(in) :: ilat
  integer :: j
  integer :: jend
  integer :: jne
  integer :: jnp
  integer :: jse
  integer :: jsp
  integer :: jstart
  integer, intent(in) :: lon
  integer :: ni
  integer :: nl
  integer, intent(in) :: nlat
  integer :: nlath
  integer :: nlatq
  !
  real, intent(inout), dimension(lon * ilat) :: pako !< Variable description\f$[units]\f$
  real, intent(in), dimension(lon + 1,nlat) :: paki !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer*4 :: mynode
  logical :: lrcm
  !
  common /mpinfo/ mynode
  common /model/  lrcm
  !==================================================================
  if (.not.lrcm) then
    !
    !**************************************************************************
    !     * gcm with reorganized latitudes and possible mpi.
    !**************************************************************************
    !
    !      * "NI" represents the counter for each grid point written into "PAKO".
    !      * "NL" represents the counter for latitude in the model structure
    !      * (ie s-n quartets starting at poles,equator).
    !
    ni = 0
    nl = 0
    if (mod(nlat,4) /= 0)       call xit('RGTOCG2', - 1)
    nlath = nlat/2
    nlatq = nlat/4
    !
    !      * mpi hook.
    !
    jstart = mynode * ilat + 1
    jend  = mynode * ilat + ilat
    !
    do j = 1,nlatq
      jsp = j
      nl = nl + 1
      if (nl >= jstart .and. nl <= jend) then
        do i = 1,lon
          ni = ni + 1
          pako(ni) = paki (i,jsp)
        end do
      end if
      !
      jnp = nlat - j + 1
      nl = nl + 1
      if (nl >= jstart .and. nl <= jend) then
        do i = 1,lon
          ni = ni + 1
          pako(ni) = paki (i,jnp)
        end do
      end if
      !
      jse = nlath - j + 1
      nl = nl + 1
      if (nl >= jstart .and. nl <= jend) then
        do i = 1,lon
          ni = ni + 1
          pako(ni) = paki (i,jse)
        end do
      end if
      !
      jne = nlath + j
      nl = nl + 1
      if (nl >= jstart .and. nl <= jend) then
        do i = 1,lon
          ni = ni + 1
          pako(ni) = paki (i,jne)
        end do
      end if
    end do
    !
  else
    !
    !**************************************************************************
    !      * rcm with usual latitudes and no mpi.
    !**************************************************************************
    !
    ni = 0
    do j = 1,nlat
      do i = 1,lon
        ni = ni + 1
        pako(ni) = paki(i,j)
      end do
    end do
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

