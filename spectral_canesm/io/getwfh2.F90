!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getwfh2(fbbcpak,fbbcpal,nlon,nlat,levwf,incd,iday, &
                   mday,mday1,mon,mon1,kount,ijpak,nuan,lf,gg)
  !
  !     * may 10/2012 - m.lazare. new version for gcm16:
  !     *                         - mdayt,mdayt1 passed in (as
  !     *                           "MDAY" and "MDAY1", respectively),
  !     *                           from model driver, instead of
  !     *                           being calculated inside this routine.
  !     * knut von salzen - feb 07,2009. new routine for gcm15h to read in
  !     *                                wildfires: fbbc.
  !     *                                (used to be done before in
  !     *                                getchem2).
  !
  implicit none
  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: incd
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer :: l
  integer, intent(in) :: levwf  !< Variable description\f$[units]\f$
  integer :: lon
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  integer, intent(in) :: mday1 !< Variable description\f$[units]\f$
  integer, intent(in) :: mon
  integer, intent(in) :: mon1
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nuan
  integer :: nc4to8
  real :: w1
  real :: w2
  !
  !     * surface emissions/concentrations.
  !
  !     * multi-level species.
  !
  real, intent(inout), dimension(ijpak,levwf) :: fbbcpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,levwf) :: fbbcpal !< Variable description\f$[units]\f$
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(levwf) :: lf !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer, dimension(6) :: levf
  integer*4 :: mynode
  !
  common /mpinfo/ mynode
  !
  data  levf/ 100, 500, 1000, 2000, 3000, 6000/
  !----------------------------------------------------------------------
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      rewind nuan
      do   l = 1,levwf
        call getggbx(fbbcpak(1,l),nc4to8("FBBC"),nuan,nlon,nlat,mon1, &
                     levf(l),gg)
      end do ! loop 8
      !
      do l = 1,levwf
        do i = 1,ijpak
          fbbcpal(i,l) = fbbcpak(i,l)
        end do
      end do ! loop 16
    else
      !
      !         * not a new integration. no new fields required.
      !
    end if
  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !        * start-up time. get fields for previous and target mid-month days.
      !
      rewind nuan
      !
      do  l = 1,levwf
        call getggbx(fbbcpak(1,l),nc4to8("FBBC"),nuan,nlon,nlat,mon1, &
                     levf(l),gg)
      end do ! loop 20
      !
      rewind nuan
      !
      do  l = 1,levwf
        call getggbx(fbbcpal(1,l),nc4to8("FBBC"),nuan,nlon,nlat,mon, &
                     levf(l),gg)
      end do ! loop 30
      !
      lon = nlon - 1
      day1 = real(mday1)
      day2 = real(iday)
      day3 = real(mday)
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) write(6,6000) iday,mday1,mday,w1,w2
      !
      do l = 1,levwf
        do i = 1,ijpak
          fbbcpak(i,l) = w1 * fbbcpal(i,l) + w2 * fbbcpak(i,l)
        end do
      end do ! loop 150
    else
      !
      !         * this is in the middle of a run.
      !
      rewind nuan
      !
      do  l = 1,levwf
        call getggbx(fbbcpal(1,l),nc4to8("FBBC"),nuan,nlon,nlat,mon, &
                     levf(l),gg)
      end do ! loop 230
    end if
    !
  end if
  return
  !---------------------------------------------------------------------
6000 format(' INTERPOLATING FOR', i5, ' BETWEEN', i5, ' AND', &
        i5, ' WITH WEIGHTS = ', 2f7.3)
  !
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
